<?php
include 'inc/connect.php';
include 'inc/template.php';
?>
  <!-- -------------container-------------->    
         <div id="page-wrapper" style="padding:25px 25px;">
<div class="container-fluid">
    <h1>Holidays Sheet search</h1>
    <hr/>    
    <div class="row" style="margin-top:30px; margin-bottom: 10px;">
        <!--===========================search================-->
        <form action="holidayssearch.php" method="post" class="pull-right" style="max-width: 200px;">
    
                <div class="input-group">
                 <input type="text" name="add" class="form-control" placeholder="enter your key"/>
                <div class="input-group-btn">
                    <button type="submit" name="save" class="btn btn-primary glyphicon glyphicon-search"></button>
                  </div>
                
                 </div>
</form>
        <!--===========================search================-->
     
</div>
<!--===========  form=========-->

<!--===========form end=========-->



<!--==============table==============-->
<div class="row">
    
    
<center>
    <table class="table table-hover text-center" border="1">
            <tr>
                <th class="text-center">SL</th>
                <th class="text-center">Name</th>
                <th class="text-center">Date of Holiday</th>
                <th class="text-center">Payment</th>
                <th class="text-center">Delete</th>
                <th class="text-center">Update</th>
            </tr>


        <?php
        if(isset($_POST['save'])){
        $search=$_POST['add'];
        $holidayssheetv="SELECT
                    `employee`.`firstname`
                    , `employee`.`lastname`
                    , `holidays`.`holid`
                    , `holidays`.`empid`
                    , `holidays`.`datehols`
                    , `holidays`.`payment`
                FROM
                    `employee`.`employee`
                    INNER JOIN `employee`.`holidays` 
                        ON (`employee`.`empid` = `holidays`.`empid`)
                where `holidays`.`holid`='$search' or `holidays`.`datehols`='$search'";
        $query=  mysqli_query($connnect,$holidayssheetv);
        $i=1;
        while($data=mysqli_fetch_array($query)){
        ?>
            <tr>
                <td><?php echo $i++;?></td>
                <td><?php  echo $data['firstname']?> <?php  echo $data['lastname']?></td>
                <td><?php echo $data['datehols'];?></td>
                <td><?php echo $data['payment'];?></td>

                <td><button data-target="#delete_nc" data-toggle="modal" class="btn btn-danger glyphicon glyphicon-trash" id="<?php echo $data['holid']?>"></button></td>
                <td><button data-target="#update_nc" data-toggle="modal" class="btn btn-info glyphicon glyphicon-edit" id="<?php echo $data['holid']?>"></button></td>
           </tr>


        <?php }}?>
    </table>
</center>
</div>

<!--==============table end==============-->

        </div>
        <!-- -------------container-------------->    
        
        </div>
       <!-- -------------page wrapper-------------->  
       
       
       
       
       
       
              <!--===========================delete modal================-->
     <div id="delete_nc" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&holidayss;</button>
                    <h3 style="color:red;">Warning</h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="pre_id" value=""/>
                    <p>Are you Sure to Want to Delete your Data?</p>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger" id="delete">Yes</button>
                    <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">No</button>
                </div>
            </div>
            
        </div>
        
        
    </div>  
    

<!--===========================delete modal================-->


<!--================================update=================-->



<div id="update_nc" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&holidayss;</button>
                    <strong><h3>Update your holidays sheet</h3></strong>
                </div>
                <div class="modal-body">
                    <div id="up"></div>
                    
                </div>
                <div class="modal-footer">
<!--                    <td><input class="btn btn-primary" type="submit" name="save" value="Submit"/></td>
                    <td><button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button></td>-->
                </div>
            </div>
           
        </div>
        
        
    </div>
    

<!--================================update=================-->
       
       
       
       
       
       
       
       

<script>
    
    
//   ======================delete====================
$('.example').delegate('button','click',function(event){
    var id=event.target.id;
    $('#pre_id').val(id);
    
})
$('#delete').click(function(){
    var base_url="http://localhost/employee/";
    var id=$('#pre_id').val();
    $.ajax({
            url:base_url+'holidaysdelete.php',
            method:'post',
            data:{holid:id},
            success:function(){
            window.location.assign('holidayssearch.php');
                
    }      
            })
    
});
//   ======================delete====================

//===========================update======================


$('.example').delegate('button','click',function(event){
    var base_url="http://localhost/employee/";
        var id=event.target.id;
    $('#pre_id').val(id);
    $.ajax({
        url:base_url+'holidays_update_form.php',
            method:'post',
            data:{id:id},
            success:function(data){
              $("#up").html(data);
           //alert(data);
                
    }   
    })
    
})

$("#update_c").submit(function(event){
 event.preventDefault();
 var base_url="http://localhost/employee/";
 $.ajax({
     url:base_url+'holidaysupdate.php',
     method:'post',
     data: new FormData(this),
     processData:false,
     processType:false,
     success:function(){
      //  window.location.assign('index.php');
      
         
     }
     
 })
    
})





//==========================update============

$(document).ready(function(){
    var base_url="http://localhost/employee/";
    $("#distict").autocomplete({
        source:base_url+'holidays_auto_search.php',
        minLength:1
        
        
    });
    
});
    

</script>