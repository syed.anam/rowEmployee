<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Massage</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>


    </head>


    <body>
        <div class="container" style="padding: 50px">
            <a href="messages.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>


            <div class="row parea" style="padding: 30px 20px 0px 20px">
                <center>


                    <table class="table table-bordered table-responsive excelTable">
                        <tr>
                            <td colspan="2"><center><h3>Massage</h3></center></td>
                        </tr>
                        <?php
                        $Imid = $_GET['Imid'];
                        $messagesview = "SELECT * FROM messages WHERE Imid='$Imid'";
                        $query = mysqli_query($connnect, $messagesview);
                        while ($data = mysqli_fetch_array($query)) {
                            ?> 
                            <tr>
                                <th>Massges</th>
                                <td><?php echo $data['message']; ?></td>
                            </tr>
                            <tr>
                                <th>Posted Date</th>
                                <td><?php echo $data['dateposted']; ?></td>
                            </tr>

                            <tr>
                                <th>Posted By</th>
                                <td><?php echo $data['postedby']; ?></td>
                            </tr>

                            <tr>
                                <th>Number of Views</th>
                                <td><?php echo $data['numviews']; ?></td>
                            </tr>


                        <?php } ?> 
                    </table>               
                </center>
            </div>
        </div>
    </body>
</html>


