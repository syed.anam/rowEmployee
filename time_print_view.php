<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Employee management | Time sheet print</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>

    </head>


    <body>
        <!-- -------------container--------------> 

        <div id="page-wrapper" style="padding:25px 25px;">
            <a href="time.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>            
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <a href="time_pdf_view.php" class="btn btn-sm btn-default"><img src="icon/pdf.png" width="17" height="17"/></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>


            <div class="container-fluid parea">


                <!--===========  form=========-->
                <center>
                    <img src="icon/logoprint.png" class="img-responsive" style="max-width: 400px; max-height: 200px"/>

                    <table class="table table-hover text-center table-responsive excelTable" border="1">
                        <tr>
                            <td colspan="7"><center><h3>Time sheet Detail</h3></center></td>
                        </tr>
                        <tr>
                            <th class="text-center">SL</th>
                            <th class="text-center">Employee Name</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Check in</th>
                            <th class="text-center">Check out</th>
                            <th class="text-center">Row time</th>
                            <th class="text-center">Checked</th>
                        </tr>


                        <?php
                        $timesheetv = "SELECT
                                            `employee`.`firstname`
                                            , `employee`.`lastname`
                                            , `project`.`projecttitle`
                                            , `timesheet`.`timeid`
                                            , `timesheet`.`empid`
                                            , `timesheet`.`projectid`
                                            , `timesheet`.`checkin`
                                            , `timesheet`.`checkout`
                                            , `timesheet`.`rawtime`
                                            , `timesheet`.`roundtime`
                                            , `timesheet`.`workdesc`
                                            , `timesheet`.`ipcheckin`
                                            , `timesheet`.`ipcheckout`
                                            , `timesheet`.`checked`
                                            , `timesheet`.`t_date`
                                        FROM
                                            `employee`.`project`
                                            INNER JOIN `employee`.`timesheet` 
                                                ON (`project`.`projectid` = `timesheet`.`projectid`)
                                            INNER JOIN `employee`.`employee` 
                                                ON (`employee`.`empid` = `timesheet`.`empid`)";
                        $query = mysqli_query($connnect, $timesheetv);

                        $i = 1;

                        while ($data = mysqli_fetch_array($query)) {
                            ?>


                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $data['firstname'] ?> <?php echo $data['lastname'] ?></td>
                                <td><?php echo $data['t_date'] ?></td>
                                <td><?php echo $data['checkin'] ?></td>
                                <td><?php echo $data['checkout'] ?></td>
                                <td><?php echo $data['rawtime'] ?></td>
                                <td><?php echo $data['checked'] ?></td>
                            </tr>


                        <?php } ?>
                    </table>
                </center>
            </div>

            <!--==============table end==============-->

        </div>
        <!-- -------------container-------------->
    </body>
</html>
