<?php
include'inc/connect.php';
require_once 'dompdf/dompdf_config.inc.php';

$codigoHTML='
<center>
   <img src="icon/logoprint.png" style="max-width: 400px; max-height: 200px; padding-top:30px;"/>
   <div  style="padding:20px;">
    <h3>Project view</h3>


<table width="100%" border="0.3">
<tr>
<td class="text-center">SL</td>
<td class="text-center">Department ID</td>
<td class="text-center">Project title</td>
<td class="text-center">Project description</td>
<td class="text-center">Hours Worked</td>
<td class="text-center">Active</td>
</tr>';


$projectv="SELECT
    `department`.`deptname`
    , `project`.`projectid`
    , `project`.`deptid`
    , `project`.`projecttitle`
    , `project`.`projectdesc`
    , `project`.`hoursworked`
    , `project`.`active`
FROM
    `employee`.`department`
    INNER JOIN `employee`.`project` 
        ON (`department`.`deptid` = `project`.`deptid`)";
$query=  mysqli_query($connnect,$projectv);

$i=1;

while($data=  mysqli_fetch_array($query)){

 $codigoHTML.='
<tr>
<td>'.$i++.'</td>
<td>'.$data['deptname'].'</td>
<td>'.$data['projecttitle'].'</td>
<td>'.$data['projectdesc'].'</td>
<td>'.$data['hoursworked'].'</td>
<td>'.$data['active'].'</td>

</tr>';

}
$codigoHTML.='
</table>
</div>
</center>';

$codigoHTML= utf8_decode($codigoHTML);
$dompdf=new DOMPDF();
$dompdf->load_html($codigoHTML);
ini_set("memory_limit", "128M");
$dompdf->render();
$dompdf->stream("project_view.pdf");



?>
        