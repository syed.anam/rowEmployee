<?php
include 'inc/connect.php';
include 'inc/template.php';
?>

<style>
    .table1{background-color: #E4E6E8}
    /*       table tr span{ color: red; font-weight: bold;}*/
    small{color:red;}
    .buttonr{width: 5; height: 10; border-radius: 50%; background-color: #aaaaaa}
</style>

<!-- -------------container-------------->     
<div id="page-wrapper" style="padding:20px;">
    <div class="container-fluid">
        <h1>Department</h1>
        <hr/>    
        <!--===========  form=========-->
        <div class="row">



            <button type="button" class="btn btn-sm btn-success glyphicon glyphicon-plus" data-toggle="modal" data-target="#add_user"></button>
            <!--    <button class="btn btn-primary glyphicon glyphicon-time" type="button" onclick="$('#target').toggle(1000);">search</button>-->
            <a href="department_view_print.php" class="btn btn-sm btn-default glyphicon glyphicon-print"></a>

            <!--=========================search=======================-->
            <!--            <form class="pull-right" action="department_search.php" method="post" style="max-width: 200px;">
            
            
                            <div class="input-group"> 
                                <input type="text" name="add" id="distict" class="form-control" placeholder="enter your key"/>
                                <div class="input-group-btn">
                                    <button type="submit" name="save" class="btn btn-primary glyphicon glyphicon-search"></button>
                                </div>
            
                            </div>
                        </form>-->
            <!--====================================search===============-->


            <div class="modal fade" id="add_user" role="dialog">

                <div class="modal-dialog modal-lg">

                    <div class="modal-content">

                        <div class="modal-header text-success text-center" style="background-color:#01421A; color:white"><h2>Department</h2></div>
                        <div class="modal-body">
                            <form id="save" action="" method="post">
                                <table class="table table-hover table1">                                       
                                    <tr>
                                        <th>Employee Name</th>
                                        <td><select id="emid_name" name="emid" onchange="names('emid_name')" onkeyup="names('emid_name')" class="form-control">
                                                <option value="">select any</option>

                                                <?php
                                                $sq4 = "select * from employee";
                                                $query2 = mysqli_query($connnect, $sq4);
                                                while ($sdata = mysqli_fetch_array($query2)) {
                                                    ?>    


                                                    <option value="<?php echo $sdata['empid'] ?>"><?php echo $sdata['firstname']; ?> <?php echo $sdata['lastname']; ?></option>

                                                <?php } ?>   
                                            </select>
                                            <span id="emid_name_error" style="color:red;">*</span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Department Name</th>
                                        <td><input type="text" id="deptname_name" name="deptname" onchange="names('deptname_name')" onkeyup="names('deptname_name')" class="form-control" />
                                            <span id="deptname_name_error" style="color:red;">*</span>
                                        </td>
                                    </tr>


                                    <tr>
                                        <th>Location</th>
                                        <td><input type="text" id="location_name" name="location" onchange="names('location_name')" onkeyup="names('location_name')" class="form-control" />
                                            <span id="location_name_error" style="color:red;">*</span>
                                        </td>
                                    </tr>


                                    <tr>
                                        <th>Department Description</th>
                                        <td><input type="text" id="deptdesc_name" name="deptdesc" onchange="names('deptdesc_name')" onkeyup="names('deptdesc_name')" class="form-control" />
                                            <span id="deptdesc_name_error" style="color:red;">*</span>
                                        </td>
                                    </tr>


                                    <tr>
                                        <th>Manager work description</th>
                                        <td><textarea rows="2" cols="3" id="mandaworkdesc_name" name="mandaworkdesc" onchange="names('mandaworkdesc_name')" onkeyup="names('mandaworkdesc_name')" class="form-control"></textarea>
                                            <span id="mandaworkdesc_name_error" style="color:red;">*</span>
                                        </td>
                                    </tr>


                                    <tr>
                                        <th>Messaging</th>
                                        <td><textarea rows="2" cols="3" id="editor1"  name="messaging" class="ckeditor form-control"></textarea>
                                            <span>*</span>
                                        </td>
                                    </tr>

                                    <span style="color:red; font-weight: bold">*</span><small >Require must be fill</small>
                                </table>

                        </div>
                        <div class="modal-footer" style="background-color:#01421A; color:white">
                            <button type="submit" class="btn btn-sm btn-success" name="save" value="Submit">Submit</button>
                            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!--======================form end=============-->
    <!--===========================date search box=====================-->
    <!--      <div class="row">
              
                  
        <div class="container" id="target" style=" padding-left: 200px;display: none;margin-bottom: 50px">
            
            <form id="showhide" action="payroll_datebetween.php" method="post">
        
                <div style="max-width:200px; margin-left: 20px;  float: left">
                <label>Start date</label>
                <input type="text" class="form-control datepicker" id="post_at" name="startdate" placeholder="enter your startdate" />
                    
                </div>
    
                <div style="max-width:200px; margin-left: 20px; float: left">
                <label>End date</label>
                <input type="text" class="form-control datepicker" id="post_at_to_date" name="enddate" placeholder="enter your enddate">
                </div>
                
                <div style="max-width:200px; margin-top: 24px; margin-left: 20px; float: left">
                    <button type="submit" name="search" class="btn btn-primary glyphicon glyphicon-search"></button>
                </div>
    </form>
           
        </div>
        
            
              
          </div>-->

    <!--===========================date search box=====================-->


    <!--==============table==============-->
    <div class="row" style="padding:15px;">
        <center>

            <!--this heading_of_table class in the sb-admin.css file=======-->
            <table class="table table-bordered text-center example" id="data_table_id">
                <thead>
                    <tr class="heading_of_table">
                        <th class="text-center">SL</th>
                        <th class="text-center">Department Name</th>
                        <th class="text-center">Department Description</th>
                        <?php
                        $access_user = $_SESSION['access_l'];
                        if ($access_user == 1) {
                            ?>    
                            <th class="text-center">Delete</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">View</th>
                        <?php } elseif ($access_user == 2) { ?>
                            <th class="text-center">View</th>                
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    include 'inc/connect.php';
                    $psql = "SELECT * FROM department";
                    $pquery = mysqli_query($connnect, $psql);
                    $i = 1;
                    while ($data = mysqli_fetch_array($pquery)) {
                        ?>    



                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data['deptname']; ?></td>
                            <td><?php echo $data['deptdesc']; ?></td>
                            <?php
                            $access_user = $_SESSION['access_l'];
                            if ($access_user == 1) {
                                ?> 
                                <td><button data-target="#delete_nc" data-toggle="modal" class="btn btn-sm btn-danger glyphicon glyphicon-trash" id="<?php echo $data['deptid'] ?>"></button></td>
                                <td><button data-target="#update_nc" data-toggle="modal" class="btn btn-sm btn-info glyphicon glyphicon-edit" id="<?php echo $data['deptid'] ?>"></button></td>
                                <td><a href="department_emp_print.php?deptid=<?php echo $data['deptid']; ?>" class="btn btn-sm btn-success glyphicon glyphicon-eject"></a></td>
                            <?php } elseif ($access_user == 2) { ?>
                                <td><a href="department_emp_print.php?deptid=<?php echo $data['deptid']; ?>" class="btn btn-sm btn-success glyphicon glyphicon-eject"></a></td>
                            <?php } ?>                
                        </tr>

                    <?php } ?> 
                </tbody>
            </table>
        </center>
    </div>

    <!--==============table end==============-->




    <!-- -------------container-------------->    

</div>
<!-- -------------page wrapper-------------->  



<!--===========================delete modal================-->
<div id="delete_nc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 style="color:red;">Warning</h3>
            </div>
            <div class="modal-body">
                <input type="hidden" id="pre_id" value=""/>
                <p>Are you Sure to Want to Delete your Data?</p>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-danger" id="delete">Yes</button>
                <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>


</div>  


<!--===========================delete modal================-->


<!--================================update=================-->



<div id="update_nc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <strong><h3>Update your Department</h3></strong>
            </div>
            <div class="modal-body">
                <div id="up"></div>

            </div>
            <div class="modal-footer">
<!--                    <td><input class="btn btn-primary" type="submit" name="save" value="Submit"/></td>
                <td><button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button></td>-->
            </div>
        </div>

    </div>


</div>


<!--================================update=================-->






<script>
    $(document).ready(function () {
        var base_url = "http://localhost/employee/";


        $('#save').submit(function (e) {
            e.preventDefault();

            var emid = $.trim($('#emid_name').val());
            //var payroll_date=$.trim($('#pdate_name').val());
            //var startday=$.trim($('#startday_name').val());
            var deptname = $.trim($('#deptname_name').val());
            var location = $.trim($('#location_name').val());
            var deptdesc = $.trim($('#deptdesc_name').val());
            //var mandaworkdesc=$.trim($('#mandaworkdesc_name').val());
            //var messaging=$.trim($('#messaging_name').val());


            if (emid === '' || deptname === '' || location === '' || deptdesc === '') {
//                if(emid==='' || deptname==='' || location==='' || deptdesc==='' || mandaworkdesc=''){
                if (emid == '') {
                    $('#emid_name_error').html('Please Enter Employee Name');
                    $('#emid_name').css("border-color", "red");
                }


                if (deptname == '') {
                    $('#deptname_name_error').html('Please Enter Your Department name');
                    $('#deptname_name').css("border-color", "red");
                }


                if (location == '') {
                    $('#location_name_error').html('Please Enter Your Department location');
                    $('#location_name').css("border-color", "red");
                }


                if (deptdesc == '') {
                    $('#deptdesc_name_error').html('Please Enter Your Department description');
                    $('#deptdesc_name').css("border-color", "red");
                }

//           if(mandaworkdesc==''){
//                $('#mandaworkdesc_name_error').html('Please Enter Your Manwork description');
//                $('#mandaworkdesc_name').css("border-color","red");
//            }
//            



            } else {
                $.ajax({
                    url: base_url + 'departmentinsert.php',
                    method: 'post',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function () {
                        window.location.assign('department.php');
                    }
                })
            }
        });
    });





    function names(id) {
        var val = $.trim($('#' + id).val());
        if (val === '') {
            $('#' + id + '_error').html('');
            $('#' + id).css('border-color', 'red');
        } else {
            $('#' + id + '_error').html('');
            $('#' + id).css('border-color', 'green');
        }
    }





//   ======================delete====================
    $('.example').delegate('button', 'click', function (event) {
        var id = event.target.id;
        $('#pre_id').val(id);

    })
    $('#delete').click(function () {
        var base_url = "http://localhost/employee/";
        var id = $('#pre_id').val();
        $.ajax({
            url: base_url + 'departmentdelete.php',
            method: 'post',
            data: {deptid: id},
            success: function () {
                window.location.assign('department.php');

            }
        })

    });
//   ======================delete====================

//===========================update======================


    $('.example').delegate('button', 'click', function (event) {
        var base_url = "http://localhost/employee/";
        var id = event.target.id;
        $('#pre_id').val(id);
        $.ajax({
            url: base_url + 'department_update_form.php',
            method: 'post',
            data: {id: id},
            success: function (data) {
                $("#up").html(data);
                //alert(data);

            }
        })

    })

    $("#update_c").submit(function (event) {
        event.preventDefault();
        var base_url = "http://localhost/employee/";
        $.ajax({
            url: base_url + 'departmentupdate.php',
            method: 'post',
            data: new FormData(this),
            processData: false,
            processType: false,
            success: function () {
                //  window.location.assign('index.php');


            }

        })

    })





//==========================update============




</script>