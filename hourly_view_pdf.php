<?php

include'inc/connect.php';
require_once 'dompdf/dompdf_config.inc.php';

$codigoHTML = '

   <center>
            <img src="icon/logo1.png"/>
    <h3>Hourly sheet detail</h3>

<table width="100%" border="1" style="padding:30px;">
    <tr>
        <td style="background-color:#EFEFEF">SL</th>
        <td style="background-color:#EFEFEF">Employee Name</th>
        <td style="background-color:#EFEFEF">Hourly Rate</th>
        <td style="background-color:#EFEFEF">Note</th>
    </tr>';




$hourlypdf = "SELECT
                `employee`.`firstname`
                    , `employee`.`lastname`
                    , `hourly`.`hourid`
                    , `hourly`.`empid`
                    , `hourly`.`hourlyrate`
                    , `hourly`.`note`
                FROM
                    `employee`.`employee`
                    INNER JOIN `employee`.`hourly` 
                        ON (`employee`.`empid` = `hourly`.`empid`)";
$query = mysqli_query($connnect, $hourlypdf);

$i = 1;

while ($data = mysqli_fetch_array($query)) {
    $codigoHTML.='
        <tr>
            <td>' . $i++ . '</td>
            <td>' . $data['firstname'] . ' ' . $data['lastname'] . '</td>
            <td>' . $data['hourlyrate'] . '</td>
            <td>' . $data['note'] . '</td>
        </tr>';
}
$codigoHTML.='
</table>
</center>';


$codigoHTML = utf8_decode($codigoHTML);
$dompdf = new DOMPDF();
$dompdf->load_html($codigoHTML);
ini_set("memory_limit", "128M");
$dompdf->render();
$dompdf->stream("hourly_pdf_view.pdf");
?>
