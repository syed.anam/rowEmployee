<?php
include 'inc/connect.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Employee management | Time sheet view</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>

    </head>


    <body>
        <div class="container" style="padding: 50px">
            <a href="time.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
<!--            <a href="timesheet_emp_view_pdf.php?timeid=<?php //$time_ses_id = $_SESSION['sess_time_id'];   ?>" class="btn btn-sm btn-default"><img src="icon/pdf.png" width="20px" height="20px"/></a>-->
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>


            <div class="row parea" style="padding: 30px 20px 0px 20px">



                <center>

                  
                    <table class="table table-bordered table-responsive excelTable">
                        <tr>
                            <td colspan="2"><center><h3>Time Sheet Information</h3></center></td>
                        </tr>
                        <?php
                        $timeid = $_GET['timeid'];
                        $timesheetview = "SELECT
                                                `employee`.`firstname`
                                                , `employee`.`lastname`
                                                , `project`.`projecttitle`
                                                , `timesheet`.`timeid`
                                                , `timesheet`.`empid`
                                                , `timesheet`.`projectid`
                                                , `timesheet`.`checkin`
                                                , `timesheet`.`checkout`
                                                , `timesheet`.`rawtime`
                                                , `timesheet`.`roundtime`
                                                , `timesheet`.`workdesc`
                                                , `timesheet`.`ipcheckin`
                                                , `timesheet`.`ipcheckout`
                                                , `timesheet`.`checked`
                                                , `timesheet`.`t_date`
                                            FROM
                                                `employee`.`project`
                                                INNER JOIN `employee`.`timesheet` 
                                                    ON (`project`.`projectid` = `timesheet`.`projectid`)
                                                INNER JOIN `employee`.`employee` 
                                                    ON (`employee`.`empid` = `timesheet`.`empid`)
                                                    where `timesheet`.`timeid`='$timeid'";
                        $query = mysqli_query($connnect, $timesheetview);
                        while ($data = mysqli_fetch_array($query)) {
                            ?>

                            <tr>
                                <th>Employee Name</th>
                                <td><?php echo $data['firstname'] ?> <?php echo $data['lastname'] ?></td>
                            </tr>
                            <tr>
                                <th>Project Name</th>
                                <td><?php echo ': ' . $data['projecttitle']; ?></td>
                            </tr>
                            <tr>
                                <th>Check In</th>
                                <td><?php echo ': ' . $data['checkin']; ?></td>
                            </tr>
                            <tr>
                                <th>Check Out</th>
                                <td><?php echo ': ' . $data['checkout']; ?></td>
                            </tr>
                            <tr>
                                <th>Raw Time</th>
                                <td><?php echo ': ' . $data['rawtime']; ?></td>
                            </tr>
                            <tr>
                                <th>Round Time</th>
                                <td><?php echo ': ' . $data['roundtime']; ?></td>
                            </tr>
                            <tr>
                                <th>Work Description</th>
                                <td><?php echo ': ' . $data['workdesc']; ?></td>
                            </tr>
                            <tr>
                                <th>Checked</th>
                                <td><?php echo ': ' . $data['checked']; ?></td>
                            </tr>


                        <?php } ?> 
                    </table>



                </center>

            </div>
        </div>

    </body>
</html>