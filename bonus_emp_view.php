<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Bonus employee view</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>


    </head>


    <body>
        <div class="container" style="padding: 50px">
            <a href="bonus.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>


            <div class="row parea" style="padding: 30px 20px 0px 20px">
                <center>


                    <table class="table table-bordered table-responsive excelTable">
                        <tr>
                            <td colspan="2"><center><h3>Bonus Information</h3></center></td>
                        </tr>
                        <?php
                        $bonusid = $_GET['bonusid'];
                        $bonusview = "SELECT
                                            `employee`.`firstname`
                                            , `employee`.`lastname`
                                            , `bonus`.`bonusid`
                                            , `bonus`.`empid`
                                            , `bonus`.`datebonus`
                                            , `bonus`.`bonuspayment`
                                            , `bonus`.`note`
                                        FROM
                                            `employee`.`employee`
                                            INNER JOIN `employee`.`bonus` 
                                                ON (`employee`.`empid` = `bonus`.`empid`)
                                                where `bonus`.`bonusid`='$bonusid'";
                        $query = mysqli_query($connnect, $bonusview);
                        while ($data = mysqli_fetch_array($query)) {
                            ?> 
                            <tr>
                                <th>Employee Name </th>
                                <td><?php echo $data['firstname'] ?> <?php echo $data['lastname'] ?></td>
                            </tr>
                            <tr>
                                <th>Date of Bonus</th>
                                <td><?php echo $data['datebonus']; ?></td>
                            </tr>
                            <tr>
                                <th>Bonus Payment :</th>
                                <td><?php echo $data['bonuspayment']; ?></td>
                            </tr>

                            <tr>
                                <th>Note :</th>
                                <td><?php echo $data['note']; ?></td>
                            </tr>


                        <?php } ?> 
                    </table>               
                </center>
            </div>
        </div>
    </body>
</html>


