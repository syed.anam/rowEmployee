<?php
include 'inc/connect.php';
include 'inc/template.php';
?>
  <!-- -------------container-------------->    
         <div id="page-wrapper" style="padding:25px 25px;">
<div class="container-fluid">
    <h1>Time Sheet search</h1>
    <hr/>    
    <div class="row" style="margin-top:30px; margin-bottom: 10px;">
        <!--===========================search================-->
        <form action="timesearch.php" method="post" class="pull-right" style="max-width: 200px;">
    
                <div class="input-group">
                 <input type="text" name="add" class="form-control" placeholder="enter your key"/>
                <div class="input-group-btn">
                    <button type="submit" name="save" class="btn btn-primary glyphicon glyphicon-search"></button>
                  </div>
                
                 </div>
</form>
        <!--===========================search================-->
     
</div>
<!--===========  form=========-->

<!--===========form end=========-->



<!--==============table==============-->
<div class="row">
    
    
<center>
    <table class="table table-hover text-center" border="1">
            <tr>
                <th class="text-center">SL</th>
                <th class="text-center">Name</th>
                <th class="text-center">Date</th>
                <th class="text-center">Check in</th>
                <th class="text-center">Check out</th>
                <th class="text-center">Rounded Time</th>
                <th class="text-center">Delete</th>
                <th class="text-center">Update</th>
            </tr>


        <?php
        if(isset($_POST['save'])){
        $search=$_POST['add'];
        $timesheetv="SELECT
            `employee`.`firstname`
            , `employee`.`lastname`
            , `project`.`projecttitle`
            , `timesheet`.`timeid`
            , `timesheet`.`empid`
            , `timesheet`.`projectid`
            , `timesheet`.`checkin`
            , `timesheet`.`checkout`
            , `timesheet`.`rawtime`
            , `timesheet`.`roundtime`
            , `timesheet`.`workdesc`
            , `timesheet`.`ipcheckin`
            , `timesheet`.`ipcheckout`
            , `timesheet`.`checked`
            , `timesheet`.`t_date`
        FROM
            `employee`.`project`
            INNER JOIN `employee`.`timesheet` 
                ON (`project`.`projectid` = `timesheet`.`projectid`)
            INNER JOIN `employee`.`employee` 
                ON (`employee`.`empid` = `timesheet`.`empid`)
                where `timesheet`.`timeid`='$search' or `timesheet`.`t_date`='$search'";
        $query=  mysqli_query($connnect,$timesheetv);
        $i=1;
        while($data=mysqli_fetch_array($query)){
        ?>
            <tr>
                <td><?php echo $i++;?></td>
                <td><?php  echo $data['firstname']?> <?php  echo $data['lastname']?></td>
                <td><?php echo $data['t_date'];?></td>
                <td><?php echo $data['checkin'];?></td>
                <td><?php echo $data['checkout'];?></td>
                <td><?php echo $data['roundtime'];?></td>
                <td><button data-target="#delete_nc" data-toggle="modal" class="btn btn-danger glyphicon glyphicon-trash" id="<?php echo $data['timeid']?>"></button></td>
                <td><button data-target="#update_nc" data-toggle="modal" class="btn btn-info glyphicon glyphicon-edit" id="<?php echo $data['timeid']?>"></button></td>
           </tr>


        <?php }}?>
    </table>
</center>
</div>

<!--==============table end==============-->

        </div>
        <!-- -------------container-------------->    
        
        </div>
       <!-- -------------page wrapper-------------->  
       
       
       
       
       
       
              <!--===========================delete modal================-->
     <div id="delete_nc" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 style="color:red;">Warning</h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="pre_id" value=""/>
                    <p>Are you Sure to Want to Delete your Data?</p>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger" id="delete">Yes</button>
                    <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">No</button>
                </div>
            </div>
            
        </div>
        
        
    </div>  
    

<!--===========================delete modal================-->


<!--================================update=================-->



<div id="update_nc" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <strong><h3>Update your Payroll</h3></strong>
                </div>
                <div class="modal-body">
                    <div id="up"></div>
                    
                </div>
                <div class="modal-footer">
<!--                    <td><input class="btn btn-primary" type="submit" name="save" value="Submit"/></td>
                    <td><button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button></td>-->
                </div>
            </div>
           
        </div>
        
        
    </div>
    

<!--================================update=================-->
       
       
       
       
       
       
       
       

<script>
    
    
//   ======================delete====================
$('.example').delegate('button','click',function(event){
    var id=event.target.id;
    $('#pre_id').val(id);
    
})
$('#delete').click(function(){
    var base_url="http://localhost/employee/";
    var id=$('#pre_id').val();
    $.ajax({
            url:base_url+'timedelete.php',
            method:'post',
            data:{timeid:id},
            success:function(){
            window.location.assign('timesearch.php');
                
    }      
            })
    
});
//   ======================delete====================

//===========================update======================


$('.example').delegate('button','click',function(event){
    var base_url="http://localhost/employee/";
        var id=event.target.id;
    $('#pre_id').val(id);
    $.ajax({
        url:base_url+'time_update_form.php',
            method:'post',
            data:{id:id},
            success:function(data){
              $("#up").html(data);
           //alert(data);
                
    }   
    })
    
})

$("#update_c").submit(function(event){
 event.preventDefault();
 var base_url="http://localhost/employee/";
 $.ajax({
     url:base_url+'timeupdate.php',
     method:'post',
     data: new FormData(this),
     processData:false,
     processType:false,
     success:function(){
      //  window.location.assign('index.php');
      
         
     }
     
 })
    
})





//==========================update============

$(document).ready(function(){
    var base_url="http://localhost/employee/";
    $("#distict").autocomplete({
        source:base_url+'time_auto_search.php',
        minLength:1
        
        
    });
    
});
    

</script>