<?php
include'inc/connect.php';
require_once 'dompdf/dompdf_config.inc.php';

$codigoHTML='

   <center>
<img src="icon/logoprint.png"/>
    <h3>Holiday sheet detail</h3>

<table width="100%" border="0.3" style="padding:30px;">
    <tr>
        <td style="background-color:#EFEFEF">SL</th>
        <td style="background-color:#EFEFEF">Employee Name</th>
        <td style="background-color:#EFEFEF">Date of holidays</th> 
        <td style="background-color:#EFEFEF">Payment</th>

    </tr>';




$holidayspdf="SELECT
                `employee`.`firstname`
                    , `employee`.`lastname`
                    , `holidays`.`holid`
                    , `holidays`.`empid`
                    , `holidays`.`datehols`
                    , `holidays`.`payment`
                    
                FROM
                    `employee`.`employee`
                    INNER JOIN `employee`.`holidays` 
                        ON (`employee`.`empid` = `holidays`.`empid`)";
$query=  mysqli_query($connnect,$holidayspdf);

$i=1;

while($data=mysqli_fetch_array($query)){   
 $codigoHTML.='
        <tr>
            <td>'.$i++.'</td>
            <td>'.$data['firstname'].' '.$data['lastname'].'</td>
            <td>'.$data['datehols'].'</td>
            <td>'.$data['payment'].'</td>
        </tr>';


 }
 $codigoHTML.='
</table>
</center>';


$codigoHTML= utf8_decode($codigoHTML);
$dompdf=new DOMPDF();
$dompdf->load_html($codigoHTML);
ini_set("memory_limit", "128M");
$dompdf->render();
$dompdf->stream("holidays_pdf_view.pdf");

?>
