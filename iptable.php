<?php
include 'inc/template.php';
include 'inc/connect.php';
?>

<?php
if (isset($_POST['save'])) {
    $type = $_POST['type'];
    $emname = $_POST['emnam'];
    $ipadd = $_POST['ipad'];
    $note = $_POST['note'];
    $sql = "insert into iptable(ip_table_type,empid,ipaddress,note)values('$type','$emname','$ipadd','$note')";
    $query = mysqli_query($connnect, $sql);
//    if($query){
//        echo 'your data is successfuly inserted';
//    }  else {
//        echo 'your data is not insert';    
//    }
}
?>


<!-- -------------container-------------->     
<div id="page-wrapper">
    <div class="container-fluid">   
        <!---------------start form--------------------->
        <div class="row" style="margin-top:30px;">

            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#add_user">Add</button>
            <a href="#" onclick="window.print()" class="btn btn-info glyphicon glyphicon-print"></a>
            <div class="modal fade" id="add_user" role="dialog">

                <div class="modal-dialog modal-lg">

                    <div class="modal-content">

                        <div class="modal-header text-success text-center" style="background-color:#01421A; color:white"><h2>IP Table</h2></div>
                        <div class="modal-body">

                            <form action="" method="post">
                                <table class="table table-hover table1">
                                <!--        <tr>
                                            <th>Ip Id</th>
                                            <td><input type="text" name="" class="form-control" maxlength="10"/><span>*</span></td>
                                        </tr> -->
                                    <tr>
                                        <th>Type</th>
                                        <td><input type="text" name="type" class="form-control" maxlength="10"/><span>*</span></td>
                                    </tr> 
                                    <tr>
                                        <th>Employee name</th>
                                        <td><input type="text" name="emnam" class="form-control" maxlength="10"/><span>*</span></td>
                                    </tr>
                                    <tr>
                                        <th>Ip Address</th>
                                        <td><textarea rows="2" cols="3" name="ipad" class="form-control"></textarea><span>*</span></td>
                                    </tr>
                                    <tr>
                                        <th>Note</th>
                                        <td><textarea rows="2" cols="3" name="note" class="form-control"></textarea>
                                            <span>*</span></td>
                                    </tr>

                                    <span style="color:red; font-weight: bold">*</span><small >Require must be fill</small>
                                </table> 

                        </div>
                        <div class="modal-footer" style="background-color:#01421A;">
                            <input type="submit" class="btn btn-warning" name="save" value="Submit">
                            <input type="button" class="btn btn-danger" data-dismiss="modal" value="Cancel">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!------------------------End form------------------------>

    <!-----------------------------start table---------------->
    <div class="row">
        <center>
            <h3>Ip table Details</h3>
            <!--this heading_of_table class in the sb-admin.css file=======-->
            <table class="table table-bordered text-center example" id="data_table_id">
                <thead>
                    <tr class="heading_of_table">
                        <th class="text-center">SL</th>
                        <th class="text-center">Type</th>
                        <th class="text-center">Employee Name</th>
                        <th class="text-center">Ip Address</th>
                        <th class="text-center">Note</th>
<?php
$access_user = $_SESSION['access_l'];
if ($access_user == 1) {
    ?>    
                            <th class="text-center">Delete</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">View</th>
<?php } elseif ($access_user == 2) { ?>
                            <th class="text-center">View</th>                
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
<?php
//            $ipid=$_GET['ipid'];
$typese = "select * from iptable";
$query = mysqli_query($connnect, $typese);
$i = 1;
while ($data = mysqli_fetch_array($query)) {
    ?>
                        <tr>
                            <td><?php echo $i++ ?></td> 
                            <td><?php echo $data['ip_table_type'] ?></td>
                            <td><?php echo $data['empid'] ?></td>
                            <td><?php echo $data['ipaddress'] ?></td>
                            <td><?php echo $data['note'] ?></td>
    <?php
    $access_user = $_SESSION['access_l'];
    if ($access_user == 1) {
        ?> 
                                <td><a href="iptable_del.php?lid=<?php echo $data['ipid'] ?>" class="btn btn-danger glyphicon glyphicon-trash"></a></td>
                                <td><a href="iptable_update.php?lid=<?php echo $data['ipid'] ?>" class="btn btn-warning glyphicon glyphicon-edit"></a></td>
                                <td><a href="iptable_view.php?lid=<?php echo $data['ipid'] ?>" class="btn btn-success glyphicon glyphicon-eject"></a></td>
    <?php } elseif ($access_user == 2) { ?>
                                <td><a href="iptable_view.php?lid=<?php echo $data['ipid'] ?>" class="btn btn-success glyphicon glyphicon-eject"></a></td>
                            <?php } ?>
                        </tr>
                            <?php } ?>

                </tbody>
            </table>
        </center>
    </div>
    <!----------------------------End table------------------->
</div>
<!-- -------------container-------------->    

<?php include 'inc/footer.php';?>