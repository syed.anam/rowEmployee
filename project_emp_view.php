<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Employee management | Project View</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>

    </head>


    <body>
        <div class="container" style="padding: 50px">
            <a href="project.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <a href="#" class="btn btn-sm btn-default"><img src="icon/pdf.png" width="17" height="17"/></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>

            <div class="row parea" style="padding: 30px 20px 0px 20px">
                <center>
                    <table class="table table-bordered table-responsive excelTable">
                        <tr>
                            <td colspan="2"><center><h3>Project Information</h3></center></td>
                        </tr>
                        <?php
                        $projectid = $_GET['projectid'];
                        $projectview = "SELECT
                                        `department`.`deptname`
                                        , `project`.`projectid`
                                        , `project`.`deptid`
                                        , `project`.`projecttitle`
                                        , `project`.`projectdesc`
                                        , `project`.`hoursworked`
                                        , `project`.`active`
                                    FROM
                                        `employee`.`department`
                                        INNER JOIN `employee`.`project` 
                                            ON (`department`.`deptid` = `project`.`deptid`) where `project`.`projectid`='$projectid'";
                        $query = mysqli_query($connnect, $projectview);
                        while ($data = mysqli_fetch_array($query)) {
                            ?>

                            <tr>
                                <th>Department Name</th>
                                <td><?php echo $data['deptname']; ?></td>
                            </tr>
                            <tr>
                                <th>Project Title</th>
                                <td><?php echo $data['projecttitle']; ?></td>
                            </tr>
                            <tr>
                                <th>Project Description</th>
                                <td><?php echo $data['projectdesc']; ?></td>
                            </tr>
                            <tr>
                                <th>Hours Worked</th>
                                <td><?php echo $data['hoursworked']; ?></td>
                            </tr>
                            <tr>
                                <th>Active</th>
                                <td><?php echo $data['active']; ?></td>
                            </tr>


                        <?php } ?> 
                    </table>



                </center>
            </div>
        </div>

    </body>
</html>