<?php
include 'inc/connect.php';
include 'inc/template.php';
?>

<style>
    .table1{background-color: #E4E6E8}
    /*       table tr span{ color: red; font-weight: bold;}*/
    small{color:red;}
    .buttonr{width: 5; height: 10; border-radius: 50%; background-color: #aaaaaa}
</style>

<!-- -------------container-------------->     
<div id="page-wrapper" style="padding:20px;">
    <div class="container-fluid">
        <h1>Employee</h1>
        <hr/>    
        <!--===========  form=========-->
        <div class="row">

            <button type="button" class="btn btn-sm btn-success glyphicon glyphicon-plus" data-toggle="modal" data-target="#add_user"></button>
            <a href="employeeview.php" class="btn btn-sm btn-default glyphicon glyphicon-print"></a>

            <!--=========================search=======================-->
            <!--            <form class="pull-right" action="employeesearch.php" method="post" style="max-width: 200px;">
            
            
                            <div class="input-group"> 
                                <input type="text" name="add" id="distict" class="form-control" placeholder="enter your key"/>
                                <div class="input-group-btn">
                                    <button type="submit" name="save" class="btn btn-primary glyphicon glyphicon-search"></button>
                                </div>
            
                            </div>
                        </form>-->
            <!--====================================search===============-->


            <div class="modal fade" id="add_user" role="dialog">

                <div class="modal-dialog modal-lg">

                    <div class="modal-content">

                        <div class="modal-header text-success text-center" style="background-color:#01421A; color:white"><h2>Bonus</h2></div>
                        <div class="modal-body">
                            <form id="save" action="" method="post">
                                <table class="table table-hover table1">                                       
                                    <tr>
                                        <th>Depertment Name</th>
                                        <td><select name="depid" id="depid" onchange="names('depid')" onkeyup="names('depid')" class="form-control depid" >
                                                <option value="">[Select From Dropdown]</option>
                                                <?php
                                                $sqlll = "select * from department";
                                                $queryyyy = mysqli_query($connnect, $sqlll);
                                                while ($sdata = mysqli_fetch_array($queryyyy)) {
                                                    ?>    
                                                    <option value="<?php echo $sdata['deptid'] ?>"><?php echo $sdata['deptname']; ?></option>
                                                <?php } ?>   
                                            </select>
                                            <span id="depid_error" style="color:red;"></span>
                                        </td>
                                        <th>Job Name</th>
                                        <td>                            
                                            <select name="jobid" id="jobid" onchange="names('jobid')" onkeyup="names('jobid')" class="form-control jobid" >
                                                <option value="">[Select From Dropdown]</option>
                                                <?php
                                                $sql3 = "select * from jobtitle";
                                                $query3 = mysqli_query($connnect, $sql3);
                                                while ($jdata = mysqli_fetch_array($query3)) {
                                                    ?>    
                                                    <option value="<?php echo $jdata['jobid'] ?>"><?php echo $jdata['jobtitle']; ?></option>
                                                <?php } ?>                               
                                            </select>
                                            <span id="jobid_error" style="color:red;"></span>
                                        </td>  
                                    </tr>    
                                    <tr>
                                        <th>Type Id</th>
                                        <td>
                                            <select name="typeid" id="typeid" onchange="names('typeid')" onkeyup="names('typeid')" class="form-control typeid" >
                                                <option value="">[Select From Dropdown]</option>
                                                <?php
                                                $sql4 = "select * from employeetype";
                                                $query4 = mysqli_query($connnect, $sql4);
                                                while ($jdata = mysqli_fetch_array($query4)) {
                                                    ?>    
                                                    <option value="<?php echo $jdata['typeid'] ?>"><?php echo $jdata['typename']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span id="typeid_error" style="color:red;"></span>
                                        </td>  

                                        <th>Category Name</th>
                                        <td>
                                            <select name="catid" id="catid" onchange="names('catid')" onkeyup="names('catid')" class="form-control catid" >
                                                <option value="">[Select From Dropdown]</option>
                                                <?php
                                                $sql5 = "select * from empcategory";
                                                $query5 = mysqli_query($connnect, $sql5);
                                                while ($cdata = mysqli_fetch_array($query5)) {
                                                    ?>    
                                                    <option value="<?php echo $cdata['catid'] ?>"><?php echo $cdata['catname']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span id="catid_error" style="color:red;"></span>
                                        </td>
                                    </tr>


                                    <tr>                                
                                        <th>First Name</th>
                                        <td><input type="text" name="firstname" id="firstname" onchange="names('firstname')" onkeyup="names('firstname')" class="form-control firstname" maxlength="20"/><span>*</span>
                                            <span id="firstname_error" style="color:red;"></span>
                                        </td>
                                        <th>Last Name</th>
                                        <td><input type="text" name="lastname" id="lastname" onchange="names('lastname')" onkeyup="names('lastname')" class="form-control lastname"  maxlength="20"/><span>*</span>
                                            <span id="lastname_error" style="color:red;"></span>
                                        </td>
                                    </tr>



                                    <tr>
                                        <th>Date of Birth</th>
                                        <td><input type="text" name="dob" id="dob" onchange="names('dob')" onkeyup="names('dob')" class="form-control datepicker dob"/><span>*</span>
                                            <span id="dob_error" style="color:red;"></span>
                                        </td>
                                        <th>Gender</th>
                                        <td>
                                            <select name="gender" id="gender" onchange="names('gender')" onkeyup="names('gender')" class="form-control gender">
                                                <option value="">[Please select]</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                                <option value="Common">Common</option>
                                            </select>
                                            <span id="gender_error" style="color:red;"></span>
                                        </td>
                                    </tr>


                                    <tr>
                                        <th>Marital status</th>
                                        <td>
                                            <select class="form-control" name="marital" id="marital" onchange="names('marital')" onkeyup="names('marital')">                                                  
                                                <option value="">[Please select]</option>
                                                <option value="Single">Single</option>
                                                <option value="Maried">Maried</option>
                                            </select>
                                            <span id="marital_error" style="color:red;"></span>
                                        </td>
                                        <th>Race</th>
                                        <td>
                                            <input type="text" name="race" class="form-control" id="race" onchange="names('race')" onkeyup="names('race')"/>
                                            <span id="marital_error" style="color:red;"></span>
                                            <span id="race_error" style="color:red;"></span>
                                        </td>
                                    </tr>



                                    <tr>
                                        <th>Mailing Address</th>
                                        <td><textarea rows="2" cols="2" name="maddress" maxlength="150" id="maddress" onchange="names('maddress')" onkeyup="names('maddress')" class="form-control maddress"></textarea><span>*</span>
                                            <span id="maddress_error" style="color:red;"></span>
                                        </td>
                                        <th>Permanent Address</th>
                                        <td><textarea rows="2" cols="2" name="paddress"  maxlength="150" id="paddress" onchange="names('paddress')" onkeyup="names('paddress')" class="form-control paddress"></textarea><span>*</span>
                                            <span id="paddress_error" style="color:red;"></span>
                                        </td>    
                                    </tr>


                                    <tr>
                                        <th>City</th>
                                        <td>
                                            <input type="text" name="city" class="form-control" id="city" onchange="names('city')" onkeyup="names('city')"/><span>*</span>
                                            <span id="city_error" style="color:red;"></span>
                                        </td>
                                        <th>Division</th>
                                        <td>
                                            <select class="form-control" name="division" id="division" onchange="names('division')" onkeyup="names('division')">
                                                <option value="">[Please select]</option>
                                                <option value="Dhaka">Dhaka</option>
                                                <option value="Rajshahi">Rajshahi</option>
                                                <option value="Sylhet">Sylhet</option>
                                                <option value="Barishal">Barishal</option>
                                                <option value="Rangpur">Rangpur</option>
                                                <option value="Khulna">Khulna</option>
                                                <option value="Chittagong">Chittagong</option>
                                                <option value="Moymonshing">Moymonshing</option>
                                            </select>
                                            <span id="division_error" style="color:red;"></span>

                                        </td>
                                    </tr>




                                    <tr>
                                        <th>Zipcode</th>
                                        <td>
                                            <input type="text" name="zipcode" class="form-control" maxlength="50" id="zipcode" onchange="names('zipcode')" onkeyup="names('zipcode')"/>
                                            <span id="zipcode_error" style="color:red;"></span>
                                        </td>
                                        <th>Country</th>
                                        <td>
                                            <select class="form-control" name="country" id="country" onchange="names('country')" onkeyup="names('country')">
                                                <option value="">[Please select]</option>
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="India">India</option>
                                                <option value="Pakistan">Pakistan</option>
                                                <option value="Japan">Japan</option>
                                                <option value="China">China</option>
                                                <option value="U.S.A">U.S.A</option>
                                                <option value="U.K">U.K</option>
                                                <option value="Canada">Canada</option>
                                            </select>
                                            <span id="country_error" style="color:red;"></span>
                                        </td>
                                    </tr>




                                    <tr>
                                        <th>Email</th>
                                        <td><input type="text" name="email" id="email" onchange="names('email')" onkeyup="names('email')" class="form-control email"/><span>*</span>
                                            <span id="email_error" style="color:red;"></span>

                                        <th>Cell Phone</th>
                                        <td><input type="text" name="cellphone" id="cellphone" onchange="names('cellphone')" onkeyup="names('cellphone')" class="form-control cellphone"/><span>*</span>
                                            <span id="cellphone_error" style="color:red;"></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Login</th>
                                        <td><input type="text" name="login" id="login" onchange="names('login')" onkeyup="names('login')" class="form-control login"/><span>*</span>
                                            <span id="login_error" style="color:red;"></span>
                                        </td>
                                        <th>Password</th>
                                        <td><input type="password" name="pass" id="password" onchange="names('password')" onkeyup="names('password')" class="form-control password"/><span>*</span>
                                            <span id="password_error" style="color:red;"></span>
                                        </td>
                                    </tr>








                                    <tr>
                                        <th>Admin</th>
                                        <td><input type="text" name="admin" class="form-control"/><span>*</span></td>

                                        <th>Image upload</th>
                                        <td><input type="file" name="imgFile" class="form-control" required/><span>*</span></td>
                                    </tr>
                                    <span style="color:red; font-weight: bold">*</span><small >Require must be fill</small>

                                </table> 

                        </div>
                        <div class="modal-footer" style="background-color:#01421A; color:white">
                            <button type="submit" class="btn btn-sm btn-success" name="save" value="Submit">Submit</button>
                            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!--======================form end=============-->


    <!--==============table==============-->
    <div class="row" style="padding:15px;">
        <center>
            <!--this heading_of_table class in the sb-admin.css file=======-->
            <table class="table table-bordered text-center example" id="data_table_id">
                <thead>
                    <tr class="heading_of_table">
                        <th class="text-center">SL</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Department name</th>
                        <th class="text-center">Email</th>
                        <?php
                        $access_user = $_SESSION['access_l'];
                        if ($access_user == 1) {
                            ?>                  

                            <th class="text-center">Delete</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">View</th>
                        <?php } elseif ($access_user == 2) { ?>

                            <th class="text-center">View</th>
                        <?php } ?>                

                    </tr>
                </thead>
                <tbody>
                    <?php
                    include 'inc/connect.php';
                    $psql = "SELECT
    `department`.`deptname`
    , `department`.`deptid`
    , `employee`.`empid`
    , `employee`.`jobid`
    , `employee`.`deptid`
    , `employee`.`typeid`
    , `employee`.`catid`
    , `employee`.`firstname`
    , `employee`.`lastname`
    , `employee`.`dob`
    , `employee`.`gender`
    , `employee`.`race`
    , `employee`.`marital`
    , `employee`.`address1`
    , `employee`.`address2`
    , `employee`.`city`
    , `employee`.`zipcode`
    , `employee`.`country`
    , `employee`.`email`
    , `employee`.`homephone`
    , `employee`.`login`
    , `employee`.`emp_password`
    , `employee`.`picture`
FROM
    `employee`.`department`
    INNER JOIN `employee`.`employee` 
        ON (`department`.`deptid` = `employee`.`deptid`) ORDER BY empid DESC";
                    //$psql="select * from employee";
                    $pquery = mysqli_query($connnect, $psql);
                    $i = 1;
                    while ($data = mysqli_fetch_array($pquery)) {
                        ?>    



                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data['firstname'] ?> <?php echo $data['lastname'] ?></td>
                            <td><?php echo $data['deptid']; ?></td>
                            <td><?php echo $data['email']; ?></td>

                            <?php
                            $access_user = $_SESSION['access_l'];
                            if ($access_user == 1) {
                                ?>   

                                <td><button data-target="#delete_nc" data-toggle="modal" class="btn btn-sm btn-danger glyphicon glyphicon-trash" id="<?php echo $data['empid'] ?>"></button></td>
                                <td><button data-target="#update_nc" data-toggle="modal" class="btn btn-sm btn-info glyphicon glyphicon-edit" id="<?php echo $data['empid'] ?>"></button></td>
                                <td><a href="employee_emp_view.php?empid=<?php echo $data['empid']; ?>" class="btn btn-sm btn-success glyphicon glyphicon-eject"></a></td>
                            <?php } elseif ($access_user == 2) { ?>
                                <td><a href="employee_emp_view.php?empid=<?php echo $data['empid']; ?>" class="btn btn-sm btn-success glyphicon glyphicon-eject"></a></td>

                            <?php } ?> 
                        </tr>

                    <?php } ?>
                </tbody>
            </table>
        </center>
    </div>

    <!--==============table end==============-->




    <!-- -------------container-------------->    

</div>
<!-- -------------page wrapper-------------->  



<!--===========================delete modal================-->
<div id="delete_nc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 style="color:red;">Warning</h3>
            </div>
            <div class="modal-body">
                <input type="hidden" id="pre_id" value=""/>
                <p>Are you Sure to Want to Delete your Data?</p>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-danger" id="delete">Yes</button>
                <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>


</div>  


<!--===========================delete modal================-->


<!--================================update=================-->



<div id="update_nc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <strong><h3>Update your employee</h3></strong>
            </div>
            <div class="modal-body">
                <div id="up"></div>

            </div>
            <div class="modal-footer">
<!--                    <td><input class="btn btn-primary" type="submit" name="save" value="Submit"/></td>
                <td><button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button></td>-->
            </div>
        </div>

    </div>


</div>


<!--================================update=================-->


<script>
    $(document).ready(function () {
        var base_url = "http://localhost/employee/";


        $('#save').submit(function (e) {
            e.preventDefault();

            var depid = $.trim($('#depid').val());
            var jobid = $.trim($('#jobid').val());
            var typeid = $.trim($('#typeid').val());
            var catid = $.trim($('#catid').val());
            var firstname = $.trim($('#firstname').val());
            var lastname = $.trim($('#lastname').val());
            var dob = $.trim($('#dob').val());
            var gender = $.trim($('#gender').val());
            var race = $.trim($('#race').val());
            var marital = $.trim($('#marital').val());
            var maddress = $.trim($('#maddress').val());
            var paddress = $.trim($('#paddress').val());
            var city = $.trim($('#city').val());
            var division = $.trim($('#division').val());
            var zipcode = $.trim($('#zipcode').val());
            var country = $.trim($('#country').val());
            var email = $.trim($('#email').val());
            var cellphone = $.trim($('#cellphone').val());
            var login = $.trim($('#login').val());
            var password = $.trim($('#password').val());





            if (depid === '' || jobid === '' || typeid === '' || catid === '' || firstname === '' || lastname === '' || dob === '' || gender === '' || maddress === '' || paddress === '' || city === '' || division === '' || zipcode === '' || country == '' || email === '' || cellphone==='' || login==='' || password==='') {
                if (depid === '') {
                    $('#depid_error').html('Please Enter Department Name');
                    $('#depid').css("border-color", "red");
                }

                if (jobid === '') {
                    $('#jobid_error').html('Please Enter Your Job Title');
                    $('#jobid').css("border-color", "red");
                }

                if (typeid === '') {
                    $('#typeid_error').html('Please Enter Your Type name');
                    $('#typeid').css("border-color", "red");
                }

                if (catid === '') {
                    $('#catid_error').html('Please Enter Catagory Name');
                    $('#catid').css("border-color", "red");
                }
                if (firstname === '') {
                    $('#firstname_error').html('Please Enter First Name');
                    $('#firstname').css("border-color", "red");
                }
                if (lastname === '') {
                    $('#lastname_error').html('Please Enter Last Name');
                    $('#lastname').css("border-color", "red");
                }


                if (dob === '') {
                    $('#dob_error').html('Please select date of birth');
                    $('#dob').css("border-color", "red");
                }
                if (gender === '') {
                    $('#gender_error').html('Please select gender');
                    $('#gender').css("border-color", "red");
                }
                if (race === '') {
                    $('#race_error').html('Please enter your race');
                    $('#race').css("border-color", "red");
                }
                if (marital === '') {
                    $('#marital_error').html('Please select marital status');
                    $('#marital').css("border-color", "red");
                }


                if (maddress === '') {
                    $('#maddress_error').html('Please select present address');
                    $('#maddress').css("border-color", "red");
                }
                if (paddress === '') {
                    $('#paddress_error').html('Please select Parmanent Address');
                    $('#paddress').css("border-color", "red");
                }


                if (city === '') {
                    $('#city_error').html('Please enter your city');
                    $('#city').css("border-color", "red");
                }
                if (division === '') {
                    $('#division_error').html('Please select your division');
                    $('#division').css("border-color", "red");
                }
                if (zipcode === '') {
                    $('#zipcode_error').html('Please Enter your zipcode');
                    $('#zipcode').css("border-color", "red");
                }
                if (country === '') {
                    $('#country_error').html('Please select your country');
                    $('#country').css("border-color", "red");
                }
                if (email === '') {
                    $('#email_error').html('Please select your email');
                    $('#email').css("border-color", "red");
                }
                if (cellphone === '') {
                    $('#cellphone_error').html('Please select your cellphone');
                    $('#cellphone').css("border-color", "red");
                }
                if (login === '') {
                    $('#login_error').html('Please select your login');
                    $('#login').css("border-color", "red");
                }
                if (password === '') {
                    $('#password_error').html('Please select your password');
                    $('#password').css("border-color", "red");
                }





            } else {
                $.ajax({
                    url: base_url + 'employee_insert.php',
                    method: 'post',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function () {
                        window.location.assign('employee.php');
                    }
                })
            }
        });
    });


    function names(id) {
        var val = $.trim($('#' + id).val());
        if (val === '') {
            $('#' + id + '_error').html('');
            $('#' + id).css('border-color', 'red');
        } else {
            $('#' + id + '_error').html('');
            $('#' + id).css('border-color', 'green');
        }
    }
//==============================department id filtering================
    //===============Empid and dep id change============


    $(".department").change(function ()
    {
        var id = $(this).val();
        var dataString = 'depID=' + id;

        $.ajax
                ({
                    type: "POST",
                    url: "time_dept_ID.php",
                    data: dataString,
                    cache: false,
                    success: function (html)
                    {
                        $(".emid_name").html(html);
                    }
                });
    });


//   ======================delete====================
    $('.example').delegate('button', 'click', function (event) {
        var id = event.target.id;
        $('#pre_id').val(id);

    })
    $('#delete').click(function () {
        var base_url = "http://localhost/employee/";
        var id = $('#pre_id').val();
        $.ajax({
            url: base_url + 'employeedelete.php',
            method: 'post',
            data: {id: id},
            success: function () {
                window.location.assign('employee.php');

            }
        })

    });
//   ======================delete====================

//===========================update======================


    $('.example').delegate('button', 'click', function (event) {
        var base_url = "http://localhost/employee/";
        var id = event.target.id;
        $('#pre_id').val(id);
        $.ajax({
            url: base_url + 'employee_update_form.php',
            method: 'post',
            data: {id: id},
            success: function (data) {
                $("#up").html(data);
                //alert(data);

            }
        })

    })

    $("#update_c").submit(function (event) {
        event.preventDefault();
        var base_url = "http://localhost/employee/";
        $.ajax({
            url: base_url + 'employeeupdate.php',
            method: 'post',
            data: new FormData(this),
            processData: false,
            processType: false,
            success: function () {
                //  window.location.assign('index.php');


            }

        })

    })





//==========================update============



</script>

<?php include 'inc/footer.php'; ?>