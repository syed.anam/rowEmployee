<?php
include 'inc/connect.php';
require_once 'dompdf/dompdf_config.inc.php';

$codigoHTML='
<center>
    <img src="icon/logoprint.png"/>
    <div style="padding: 20px">
    <h3>Department view</h3>



<table width="100%" border="0.3" style="padding:20px;">
<tr>
<td class="text-center" style="background-color:#EFEFEF">SL</td>
<td class="text-center" style="background-color:#EFEFEF">Employee Name</td>
<td class="text-center" style="background-color:#EFEFEF">Department Name</td>
<td class="text-center" style="background-color:#EFEFEF">Location</td>
<td class="text-center" style="background-color:#EFEFEF">Department description</td>
<td class="text-center" style="background-color:#EFEFEF">Manager work description</td>
</tr>';


$deparmentv="SELECT
    `employee`.`firstname`
    , `employee`.`lastname`
    , `department`.`deptid`
    , `department`.`empid`
    , `department`.`deptname`
    , `department`.`location`
    , `department`.`deptdesc`
    , `department`.`mandaworkdesc`
    , `department`.`messaging`
FROM
    `employee`.`employee`
    INNER JOIN `employee`.`department` 
        ON (`employee`.`empid` = `department`.`empid`)";
$query=  mysqli_query($connnect,$deparmentv);

$i=1;
while($data=mysqli_fetch_array($query)){

$codigoHTML.='
<tr>
<td>'.$i++.'</td>
<td>'.$data['firstname'].' '.$data['lastname'].'</td>
<td>'.$data['deptname'].'</td>
<td>'.$data['location'].'</td>
<td>'.$data['deptdesc'].'</td>
<td>'.$data['mandaworkdesc'].'</td>
</tr>';

}
$codigoHTML.='
</table>
    </div>
</center>';


$codigoHTML= utf8_decode($codigoHTML);
$dompdf=new DOMPDF();
$dompdf->load_html($codigoHTML);
ini_set("memory_limit", "128M");
$dompdf->render();
$dompdf->stream("department_view.pdf");
 

?>
            

