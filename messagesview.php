<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Massage</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>


    </head>


    <body>
        <!-- -------------container--------------> 

        <div id="page-wrapper" style="padding:25px 25px;">
            <a href="messages.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>            
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <a href="messages_view_pdf.php" class="btn btn-sm btn-default"><img src="icon/pdf.png" width="17px" height="17px"/></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>


            <div class="container-fluid parea">


                <!--===========  table=========-->
                <center>
                    <img src="icon/logoprint.png" class="img-responsive" style="max-width: 400px; max-height: 200px"/>
                    <!--    <h3>Bonus sheet detail</h3>-->
                    <hr/>


                    <table class="table table-hover text-center table-responsive excelTable" border="1">
                        <tr>
                            <th colspan="5" class="text-center">
                                <h3>Massages Detail</h3>
                            </th>
                        </tr>
                        <tr>
                            <th class="text-center">SL</th>
                            <th class="text-center">Massage</th>
                            <th class="text-center">Posted By</th> 
                            <th class="text-center">Posted Date</th>
                            <th class="text-center">Number of view</th>
                        </tr>


                        <?php
                        $messagesv = "SELECT * FROM messages";
                        $query3 = mysqli_query($connnect, $messagesv);

                        $i = 1;

                        while ($data = mysqli_fetch_array($query3)) {
                            ?>


                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $data['message'] ?></td>
                                <td><?php echo $data['postedby'] ?></td>
                                <td><?php echo $data['dateposted'] ?></td>
                                <td><?php echo $data['numviews'] ?></td>

                            </tr>


<?php } ?>
                    </table>
                </center>
            </div>

            <!--==============table end==============-->

        </div>
        <!-- -------------container-------------->
    </body>
</html>
