<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Employee management | Department Event view</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>
    </head>


    <body>
        <!-- -------------container--------------> 

        <div id="page-wrapper" style="padding:25px 25px;">
            <a href="deptevents.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>            
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <a href="deptevents_view_pdf.php" class="btn btn-sm btn-default"><img src="icon/pdf.png" width="17px" height="17px"/></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>

            <div class="container-fluid parea">


                <!--===========  table=========-->
                <center>
                    <img src="icon/logoprint.png" class="img-responsive" style="max-width: 400px; max-height: 200px"/>
                    
                  


                    <table class="table table-hover text-center table-responsive excelTable" border="1">
                        <tr>
                            <td colspan="7"><center><h3>Department Event sheet detail</h3></center></td>
                        </tr>
                        <tr>
                            <th class="text-center">SL</th>
                            <th class="text-center">Department Name</th>
                            <th class="text-center">Event Date</th>
                            <th class="text-center">Event Time</th>
                            <th class="text-center">Event Details</th>
                            <th class="text-center">Posted By</th>
                            <th class="text-center">Expiry Date</th>
                        </tr>


                        <?php
                        $depteventsv = "SELECT
                    `department`.`deptname`
                    , `deptevents`.`eventid`
                    , `deptevents`.`deptid`
                    , `deptevents`.`eventdate`
                    , `deptevents`.`eventime`
                    , `deptevents`.`evenbody`
                    , `deptevents`.`postedby`
                    , `deptevents`.`dateposted`
                    , `deptevents`.`expirydate`
                    , `deptevents`.`active`
                FROM
                    `employee`.`department`
                    INNER JOIN `employee`.`deptevents` 
                        ON (`department`.`deptid` = `deptevents`.`deptid`)";
                        $query3 = mysqli_query($connnect, $depteventsv);

                        $i = 1;

                        while ($data = mysqli_fetch_array($query3)) {
                            ?>


                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $data['deptname'] ?></td>
                                <td><?php echo $data['eventdate']; ?></td>
                                <td><?php echo $data['eventime']; ?></td>
                                <td><?php echo $data['evenbody']; ?></td>
                                <td><?php echo $data['postedby']; ?></td>
                                <td><?php echo $data['expirydate']; ?></td>
                            </tr>


                        <?php } ?>
                    </table>
                </center>
            </div>

            <!--==============table end==============-->

        </div>

        <!-- -------------container-------------->
    </body>
</html>
