<?php
session_start();
if (!@$_SESSION['ID']) {
    echo '<meta http-equiv="refresh" content="0;url=log.php">';
}
?>

<!DOCTYPE html>
<html>
    <head>  
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Employee management</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <link href="css/jquery.dataTables.min.css" rel="stylesheet">
        <script src="ckeditor/ckeditor.js" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
    <!--    <script src="js/jquery-min.js"></script>-->
        <script src="js/jquery-1.12.3.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/jquery-ui.js"></script>





        <script type="text/javascript" language="javascript" >

            $(function () {
                $(".datepicker").datepicker();
            });



            $(document).ready(function () {
                $('#data_table_id').DataTable();
            });

        </script>
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.php"><b>Employee management</b></a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
                        <ul class="dropdown-menu message-dropdown">
                            <li>
                                <a href="#"><i class="fa fa-picture-o pull-right"></i>Banner change</a>
                            </li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i></a>
                        <ul class="dropdown-menu alert-dropdown">
                            <li>
                                <a href="">Notice</a>
                            </li>


                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                            </li>

                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i><b> Log Out</b></a>
                            </li>
                        </ul>
                    </li>
                </ul>


                <!-- Sidebar Menu Items -->
                <?php
                $access_user = $_SESSION['access_l'];
                if ($access_user == 1) {
                    include('super_template.php');
                } elseif ($access_user == 2) {
                    include('admin_template.php');
                }
                ?>
                <!-- /.navbar-collapse -->
            </nav>


            <!-- -------------container-------------->    