<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Employee management | Department Event</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>


    </head>


    <body>
        <div class="container" style="padding: 50px">
            <a href="deptevents.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>


            <div class="row parea" style="padding: 30px 20px 0px 20px">
                <center>


                    <table class="table table-bordered table-responsive excelTable">
                        <tr>
                            <td colspan="2"><center><h3>Department Event Information</h3></center></td>
                        </tr>
                        <?php
                        $eventid = $_GET['eventid'];
                        $depteventsview = "SELECT
                                                `department`.`deptname`
                                                , `deptevents`.`eventid`
                                                , `deptevents`.`deptid`
                                                , `deptevents`.`eventdate`
                                                , `deptevents`.`eventime`
                                                , `deptevents`.`evenbody`
                                                , `deptevents`.`postedby`
                                                , `deptevents`.`dateposted`
                                                , `deptevents`.`expirydate`
                                                , `deptevents`.`active`
                                            FROM
                                                `employee`.`department`
                                                INNER JOIN `employee`.`deptevents` 
                                                    ON (`department`.`deptid` = `deptevents`.`deptid`)
                                    where `deptevents`.`eventid`='$eventid'";
                        $query = mysqli_query($connnect, $depteventsview);
                        while ($data = mysqli_fetch_array($query)) {
                            ?> 
                            <tr>
                                <th>Department Name </th>
                                <td><?php echo $data['deptname'] ?></td>
                            </tr>
                            <tr>
                                <th>Event Date</th>
                                <td><?php echo $data['eventdate']; ?></td>
                            </tr>
                            <tr>
                                <th>Event Time</th>
                                <td><?php echo $data['eventime']; ?></td>
                            </tr>

                            <tr>
                                <th>Event Detail</th>
                                <td><?php echo $data['evenbody']; ?></td>
                            </tr>        

                            <tr>
                                <th>Posted By</th>
                                <td><?php echo $data['postedby']; ?></td>
                            </tr>         

                            <tr>
                                <th>Date of posted</th>
                                <td><?php echo $data['dateposted']; ?></td>
                            </tr>

                            <tr>
                                <th>Expiry Date</th>
                                <td><?php echo $data['expirydate']; ?></td>
                            </tr>          


                        <?php } ?> 
                    </table>               
                </center>
            </div>
        </div>
    </body>
</html>


