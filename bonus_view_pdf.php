<?php
include'inc/connect.php';
require_once 'dompdf/dompdf_config.inc.php';

$codigoHTML='

   <center>
<img src="icon/logoprint.png"/>
    <h3>Bonus sheet detail</h3>

<table width="100%" border="0.3" style="padding:30px;">
    <tr>
        <td style="background-color:#EFEFEF">SL</th>
        <td style="background-color:#EFEFEF">Employee Name</th>
        <td style="background-color:#EFEFEF">Date of bonus</th> 
        <td style="background-color:#EFEFEF">Payment</th>
        <td style="background-color:#EFEFEF">Note</th>
    </tr>';




$bonuspdf="SELECT
                `employee`.`firstname`
                    , `employee`.`lastname`
                    , `bonus`.`bonusid`
                    , `bonus`.`empid`
                    , `bonus`.`datebonus`
                    , `bonus`.`bonuspayment`
                    , `bonus`.`note`
                FROM
                    `employee`.`employee`
                    INNER JOIN `employee`.`bonus` 
                        ON (`employee`.`empid` = `bonus`.`empid`)";
$query=  mysqli_query($connnect,$bonuspdf);

$i=1;

while($data=mysqli_fetch_array($query)){   
 $codigoHTML.='
        <tr>
            <td>'.$i++.'</td>
            <td>'.$data['firstname'].' '.$data['lastname'].'</td>
            <td>'.$data['datebonus'].'</td>
            <td>'.$data['bonuspayment'].'</td>
            <td>'.$data['note'].'</td>
        </tr>';


 }
 $codigoHTML.='
</table>
</center>';


$codigoHTML= utf8_decode($codigoHTML);
$dompdf=new DOMPDF();
$dompdf->load_html($codigoHTML);
ini_set("memory_limit", "128M");
$dompdf->render();
$dompdf->stream("bonus_pdf_view.pdf");

?>
