<?php
include'inc/connect.php';
require_once 'dompdf/dompdf_config.inc.php';

$codigoHTML='

   <center>
<img src="icon/logoprint.png" style="max-width: 400px; max-height: 200px"/>
    <h3>Time sheet detail</h3>

<table width="100%" border="0.5" style="padding:30px;">
<tr>
<th class="text-center" style="background-color:#E9EBEE">SL</th>
<th class="text-center" style="background-color:#E9EBEE">Name</th>
<th class="text-center" style="background-color:#E9EBEE">Date</th>
<th class="text-center" style="background-color:#E9EBEE">Check in</th>
<th class="text-center" style="background-color:#E9EBEE">Check out</th>
<th class="text-center" style="background-color:#E9EBEE">Row time</th>
<th class="text-center" style="background-color:#E9EBEE">Checked</th>
</tr>';




$timesheetv="SELECT
    `employee`.`firstname`
    , `employee`.`lastname`
    , `project`.`projecttitle`
    , `timesheet`.`timeid`
    , `timesheet`.`empid`
    , `timesheet`.`projectid`
    , `timesheet`.`checkin`
    , `timesheet`.`checkout`
    , `timesheet`.`rawtime`
    , `timesheet`.`roundtime`
    , `timesheet`.`workdesc`
    , `timesheet`.`ipcheckin`
    , `timesheet`.`ipcheckout`
    , `timesheet`.`checked`
    , `timesheet`.`t_date`
FROM
    `employee`.`project`
    INNER JOIN `employee`.`timesheet` 
        ON (`project`.`projectid` = `timesheet`.`projectid`)
    INNER JOIN `employee`.`employee` 
        ON (`employee`.`empid` = `timesheet`.`empid`)";
$query=  mysqli_query($connnect,$timesheetv);

$i=1;

while($data=mysqli_fetch_array($query)){   
 $codigoHTML.='
<tr>
<td>'.$i++.'</td>
<td>'.$data['firstname'].' '.$data['lastname'].'</td>
<td>'.$data['t_date'].'</td>
<td>'.$data['checkin'].'</td>
<td>'.$data['checkout'].'</td>
<td>'.$data['rawtime'].'</td>
<td>'.$data['checked'].'</td>
</tr>';


 }
 $codigoHTML.='
</table>
</center>';


$codigoHTML= utf8_decode($codigoHTML);
$dompdf=new DOMPDF();
$dompdf->load_html($codigoHTML);
ini_set("memory_limit", "128M");
$dompdf->render();
$dompdf->stream("time_view.pdf");

?>
