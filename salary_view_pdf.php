<?php
include'inc/connect.php';
require_once 'dompdf/dompdf_config.inc.php';

$codigoHTML='

   <center>
<img src="icon/logoprint.png"/>
    <h3>Salary sheet detail</h3>

<table width="100%" border="0.3" style="padding:20px;">
    <tr>
        <td style="background-color:#EFEFEF">SL</th>
        <td style="background-color:#EFEFEF">Employee Name</th>
        <td style="background-color:#EFEFEF">Base Year</th> 
        <td style="background-color:#EFEFEF">note</th>

    </tr>';




$salarypdf="SELECT
                `employee`.`firstname`
                    , `employee`.`lastname`
                    , `salary`.`salaryid`
                    , `salary`.`empid`
                    , `salary`.`baseyear`
                    , `salary`.`note`
                    
                FROM
                    `employee`.`employee`
                    INNER JOIN `employee`.`salary` 
                        ON (`employee`.`empid` = `salary`.`empid`)";
$query=  mysqli_query($connnect,$salarypdf);

$i=1;

while($data=mysqli_fetch_array($query)){   
 $codigoHTML.='
        <tr>
            <td>'.$i++.'</td>
            <td>'.$data['firstname'].' '.$data['lastname'].'</td>
            <td>'.$data['baseyear'].'</td>
            <td>'.$data['note'].'</td>
        </tr>';


 }
 $codigoHTML.='
</table>
</center>';


$codigoHTML= utf8_decode($codigoHTML);
$dompdf=new DOMPDF();
$dompdf->load_html($codigoHTML);
ini_set("memory_limit", "128M");
$dompdf->render();
$dompdf->stream("salary_pdf_view.pdf");

?>
