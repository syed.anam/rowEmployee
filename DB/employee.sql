-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 14, 2017 at 11:27 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employee`
--

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE `bonus` (
  `bonusid` int(11) NOT NULL,
  `empid` int(11) DEFAULT NULL,
  `datebonus` date DEFAULT NULL,
  `bonuspayment` decimal(10,2) DEFAULT NULL,
  `note` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bonus`
--

INSERT INTO `bonus` (`bonusid`, `empid`, `datebonus`, `bonuspayment`, `note`) VALUES
(6, 15, '0000-00-00', '0.00', ''),
(7, 0, '0000-00-00', '0.00', ''),
(8, 14, '2013-04-02', '123456.00', 'sdf'),
(9, 2, '2016-10-12', '2000.00', 'sdfsdf'),
(10, 2, '2016-10-05', '45674564.00', '5asasd'),
(11, 2, '2016-10-12', '2000.00', 'sdfsdf'),
(12, 2, '2016-10-05', '45674564.00', '5asasd'),
(13, 2, '2016-10-12', '2000.00', 'sdfsdf'),
(14, 2, '2016-10-05', '45674564.00', '5asasd'),
(15, 2, '2016-10-12', '2000.00', 'sdfsdf'),
(16, 2, '2016-10-05', '45674564.00', '5asasd'),
(17, 2, '2016-10-12', '2000.00', 'sdfsdf'),
(18, 2, '2016-10-05', '45674564.00', '5asasd'),
(19, 2, '2016-10-12', '2000.00', 'sdfsdf'),
(20, 2, '2016-10-05', '45674564.00', '5asasd'),
(21, 2, '2016-10-12', '2000.00', 'sdfsdf'),
(22, 2, '2016-10-05', '45674564.00', '5asasd'),
(23, 2, '2016-10-12', '2000.00', 'sdfsdf'),
(24, 2, '2016-10-05', '45674564.00', '5asasd'),
(25, 14, '0000-00-00', '0.00', 'sdfsdf'),
(26, 14, '0000-00-00', '0.00', 'dfsdfsdf'),
(27, 16, '0000-00-00', '0.00', 'sdfsdf'),
(28, 14, '0000-00-00', '0.00', 'sdfsdf'),
(29, 14, '0000-00-00', '0.00', 'sdfsdf'),
(30, 14, '0000-00-00', '0.00', 'sdfsdf'),
(31, 10, '0000-00-00', '0.00', 'sdfsdf'),
(32, 14, '0000-00-00', '0.00', 'sdfsdf'),
(33, 10, '0000-00-00', '0.00', 'sdfsdf'),
(34, 0, '0000-00-00', '0.00', 'dfg'),
(35, 0, '0000-00-00', '0.00', 'dfg'),
(36, 0, '0000-00-00', '0.00', 'kamina');

-- --------------------------------------------------------

--
-- Table structure for table `deductions`
--

CREATE TABLE `deductions` (
  `deduid` int(10) NOT NULL,
  `empid` int(10) DEFAULT NULL,
  `deductype` varchar(100) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deductions`
--

INSERT INTO `deductions` (`deduid`, `empid`, `deductype`, `amount`, `note`) VALUES
(1, 2014, '', '252001.00', ''),
(2, 0, '', '0.00', ''),
(4, 16, 'sdfsdf', '12.00', 'dfgdgdfgdfg'),
(5, 14, 'sdfsd', '20.00', 'sdfsdfsdf');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `deptid` int(11) NOT NULL,
  `empid` int(11) DEFAULT NULL,
  `deptname` varchar(30) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `deptdesc` varchar(100) DEFAULT NULL,
  `mandaworkdesc` varchar(100) DEFAULT NULL,
  `messaging` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`deptid`, `empid`, `deptname`, `location`, `deptdesc`, `mandaworkdesc`, `messaging`) VALUES
(13, 0, 'coool', 'asdfsadf', 'sadf', 'sadf', ''),
(15, 0, 'management', NULL, NULL, NULL, NULL),
(16, 0, 'Account', NULL, NULL, NULL, NULL),
(18, 14, 'sdf', 'sdf', 'sdf', 'sdfs', '');

-- --------------------------------------------------------

--
-- Table structure for table `deptevents`
--

CREATE TABLE `deptevents` (
  `eventid` int(11) NOT NULL,
  `deptid` int(11) DEFAULT NULL,
  `eventdate` date DEFAULT NULL,
  `eventime` time DEFAULT NULL,
  `evenbody` text,
  `postedby` varchar(100) DEFAULT NULL,
  `dateposted` date DEFAULT NULL,
  `expirydate` date DEFAULT NULL,
  `active` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deptevents`
--

INSERT INTO `deptevents` (`eventid`, `deptid`, `eventdate`, `eventime`, `evenbody`, `postedby`, `dateposted`, `expirydate`, `active`) VALUES
(8, 13, '2016-03-06', '05:20:50', 'dfsdfsfd', 'sdfsdfs', '2016-09-16', '2016-03-09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `empcategory`
--

CREATE TABLE `empcategory` (
  `catid` int(11) NOT NULL,
  `catname` tinytext,
  `catdesc` varchar(200) DEFAULT NULL,
  `miscnote` tinytext,
  `empid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empcategory`
--

INSERT INTO `empcategory` (`catid`, `catname`, `catdesc`, `miscnote`, `empid`) VALUES
(2, 'sinior officer ex', '', 'good', 10),
(8, 'Account Collector', 'abcd', 'abcde', 11);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `empid` int(11) NOT NULL,
  `deptid` int(11) DEFAULT NULL,
  `jobid` int(11) DEFAULT NULL,
  `typeid` int(11) DEFAULT NULL,
  `catid` int(11) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `race` varchar(20) DEFAULT NULL,
  `marital` varchar(10) DEFAULT NULL,
  `address1` text,
  `address2` text,
  `city` text,
  `state` varchar(30) DEFAULT NULL,
  `zipcode` text,
  `country` varchar(25) DEFAULT NULL,
  `email` text,
  `homephone` text,
  `login` varchar(25) DEFAULT NULL,
  `emp_password` varchar(50) DEFAULT NULL,
  `admin` varchar(25) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  `picture` text,
  `access_level` int(1) DEFAULT NULL COMMENT 'superadmin=1,admin=2,user=3'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`empid`, `deptid`, `jobid`, `typeid`, `catid`, `firstname`, `lastname`, `dob`, `gender`, `race`, `marital`, `address1`, `address2`, `city`, `state`, `zipcode`, `country`, `email`, `homephone`, `login`, `emp_password`, `admin`, `active`, `picture`, `access_level`) VALUES
(14, 13, 564656, 556848, 564654, 'shapan ', 'hossain', '0000-00-00', '', '3232', '', '', '', '', '', '', '', '', '', 'shapan', '123456', '', '', 'employee_img/ANAM PHOTO NEW.jpg', 1),
(15, 14, 0, 0, 0, 'Abir', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 'hossain', '123456', '', '', 'employee_img/', NULL),
(16, 14, 0, 0, 0, 'sdf', 'sdfsd', '0000-00-00', 'Female', 'sdf', 'Single', 'sdfsdf', 'sdf', 'sdfs', 'Barishal', 'dfsdf', 'Pakistan', 'sdf', 'sdfsdf', 'hossain', 'sdfs', 'sdfsdf', 's', 'employee_img/', NULL),
(17, 0, 0, 0, 0, 'google', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'employee_img/', NULL),
(18, 0, 0, 0, 0, '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 'shapan', 'sadf', '', '', 'employee_img/', NULL),
(19, 15, 0, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'employee_img/Untitled.png', NULL),
(21, 15, 0, 6, 2, 'sdfsdf', 'sdfsd', '2016-10-06', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'employee_img/', NULL),
(22, 15, 0, 6, 2, 'asdf', 'sadf', '2016-10-07', 'Female', 'sdf', 'Single', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'employee_img/Untitled.png', NULL),
(23, 15, 0, 6, 2, 'sdfsdf', 'sdfsdf', '2016-10-12', 'Female', 'sdfsfd', 'Single', 'sdfsdf', 'sdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'employee_img/', NULL),
(24, 15, 0, 12, 8, 'sdfs', 'sdf', '2016-10-05', 'Female', 'sdf', 'Single', 'sdf', 'sdf', 'sdf', 'Khulna', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'employee_img/', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employeetype`
--

CREATE TABLE `employeetype` (
  `typeid` int(10) NOT NULL,
  `typename` tinytext,
  `typedesc` varchar(200) DEFAULT NULL,
  `miscnote` tinytext,
  `empid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employeetype`
--

INSERT INTO `employeetype` (`typeid`, `typename`, `typedesc`, `miscnote`, `empid`) VALUES
(6, 'Part time', 'ABC', 'ABC', 10),
(12, 'Full time', 'sdfsf', 'sdfsdfsdf', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `emppicture`
--

CREATE TABLE `emppicture` (
  `pidd` int(10) NOT NULL,
  `empid` int(11) NOT NULL DEFAULT '0',
  `emp_pic_type` varchar(100) DEFAULT NULL,
  `filename` tinytext,
  `filesize` varchar(50) DEFAULT NULL,
  `filetype` varchar(50) DEFAULT NULL,
  `picture` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emppicture`
--

INSERT INTO `emppicture` (`pidd`, `empid`, `emp_pic_type`, `filename`, `filesize`, `filetype`, `picture`) VALUES
(4, 0, 'sdsa', 'asffsaf', 'safdaf', 'fdfadfsadf', ''),
(8, 11, 'pass', 'anam', '123', 'jpg', 'employee_img/ANAM PHOTO NEW.jpg'),
(10, 10, 'sdasd', 'dsadsa', 'asfas', 'dasdas', 'employee_img/13043433_1120441974674502_4921825714017922095_n.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `fpage`
--

CREATE TABLE `fpage` (
  `page_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `page_title` varchar(150) NOT NULL,
  `page_sub_title` varchar(200) NOT NULL,
  `page_date` date NOT NULL,
  `page_time` varchar(50) NOT NULL,
  `page_des` text NOT NULL,
  `page_picture` text NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fpage`
--

INSERT INTO `fpage` (`page_id`, `cat_id`, `page_title`, `page_sub_title`, `page_date`, `page_time`, `page_des`, `page_picture`, `status`) VALUES
(1, 1, 'top logo', 'top logo', '0000-00-00', '2016-10-11', '6:05 PM', 'fpage_image/', 0),
(2, 2, 'menu ', 'menu ', '0000-00-00', '2016-10-11', '6:08 PM', 'fpage_image/', 0),
(3, 1, 'top logo', 'top logo', '2016-10-11', '6:19 PM', 'top logo', 'fpage_images/logo1.jpg', 0),
(4, 5, 'favicon', 'favicon', '2016-10-11', '8:03 PM', 'favicon', 'fpage_images/favicon.ico', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fpage_category`
--

CREATE TABLE `fpage_category` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(50) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fpage_category`
--

INSERT INTO `fpage_category` (`cat_id`, `cat_name`, `status`) VALUES
(1, 'logo', 0),
(2, 'menu', 0),
(5, 'favicon', 0);

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `holid` int(11) NOT NULL,
  `empid` int(11) DEFAULT NULL,
  `datehols` date DEFAULT NULL,
  `payment` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`holid`, `empid`, `datehols`, `payment`) VALUES
(4, NULL, '2016-08-15', '1000.00'),
(5, NULL, '2016-12-16', '100000.00'),
(6, NULL, '2016-02-21', '8000.00'),
(9, 16, '2016-03-03', '1230.00');

-- --------------------------------------------------------

--
-- Table structure for table `hourly`
--

CREATE TABLE `hourly` (
  `hourid` int(10) NOT NULL,
  `empid` int(10) DEFAULT NULL,
  `hourlyrate` decimal(10,2) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hourly`
--

INSERT INTO `hourly` (`hourid`, `empid`, `hourlyrate`, `note`) VALUES
(2, 2014, '2016.00', ''),
(6, 14, '10.00', ''),
(8, 15, '0.00', ''),
(9, 17, '0.00', ''),
(12, 10, '0.00', ''),
(13, 10, '0.00', ''),
(14, 0, '25.00', 'sdf');

-- --------------------------------------------------------

--
-- Table structure for table `iptable`
--

CREATE TABLE `iptable` (
  `ipid` int(10) NOT NULL,
  `ip_table_type` varchar(50) DEFAULT NULL,
  `empid` int(11) NOT NULL DEFAULT '0',
  `ipaddress` tinytext,
  `note` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iptable`
--

INSERT INTO `iptable` (`ipid`, `ip_table_type`, `empid`, `ipaddress`, `note`) VALUES
(2, 'as', 0, 'abcdef', 'abcdefghi'),
(3, 'anam', 0, '1234546', 'abcde');

-- --------------------------------------------------------

--
-- Table structure for table `jobtitle`
--

CREATE TABLE `jobtitle` (
  `jobid` int(10) NOT NULL,
  `jobtitle` varchar(50) DEFAULT NULL,
  `jobdesc` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobtitle`
--

INSERT INTO `jobtitle` (`jobid`, `jobtitle`, `jobdesc`) VALUES
(9, 'Manager', 'e'),
(10, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `locks`
--

CREATE TABLE `locks` (
  `lockid` int(10) NOT NULL,
  `empid` int(10) NOT NULL,
  `datelock` date DEFAULT NULL,
  `reasonlock` tinytext,
  `active` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locks`
--

INSERT INTO `locks` (`lockid`, `empid`, `datelock`, `reasonlock`, `active`) VALUES
(1, 0, '0000-00-00', '', ''),
(2, 0, '2016-04-21', '', ''),
(3, 0, '0000-00-00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `Imid` int(10) NOT NULL,
  `message` text,
  `postedby` tinytext,
  `dateposted` date DEFAULT NULL,
  `numviews` varchar(100) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`Imid`, `message`, `postedby`, `dateposted`, `numviews`, `active`) VALUES
(1, '', 'sdfg', '0000-00-16', '50', '4'),
(5, 'sdsfsdfsd', 'fsdfsdfsdf', '2016-10-14', NULL, NULL),
(7, 'our newwlejrlwjer', 'werwerwerwer', '2016-10-14', NULL, NULL),
(8, 'our newwlejrlwjer', 'werwerwerwer', '2016-10-14', NULL, NULL),
(9, 'our newwlejrlwjer', 'werwerwerwer', '2016-10-14', NULL, NULL),
(10, 'our newwlejrlwjer', 'werwerwerwer', '2016-10-14', NULL, NULL),
(11, 'our newwlejrlwjer', 'werwerwerwer', '2016-10-14', NULL, NULL),
(12, 'our newwlejrlwjer', 'werwerwerwer', '2016-10-14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `payrollid` int(11) NOT NULL,
  `empid` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `startday` date DEFAULT NULL,
  `endday` date DEFAULT NULL,
  `hoursworked` time DEFAULT NULL,
  `grosspay` decimal(10,2) DEFAULT NULL,
  `deductions` decimal(10,2) DEFAULT NULL,
  `netpay` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payroll`
--

INSERT INTO `payroll` (`payrollid`, `empid`, `date`, `startday`, `endday`, `hoursworked`, `grosspay`, `deductions`, `netpay`) VALUES
(26, 14, '2016-09-26', '2016-10-13', '2016-10-13', '01:45:32', '4096797.59', '20.00', '4096777.59'),
(27, 14, '2016-09-26', '2016-10-13', '2016-10-13', '01:45:32', '17.59', '20.00', '-2.41'),
(28, 14, '2016-10-13', '0000-00-00', '2016-10-04', '09:20:00', '93.33', '20.00', '73.33'),
(29, 14, '2016-10-13', '2016-09-23', '2016-10-04', '09:20:00', '93.33', '20.00', '73.33'),
(30, 10, '2016-10-14', '2016-09-19', '2016-09-24', '01:24:00', '0.00', '0.00', '0.00'),
(31, 14, '2016-10-18', '0000-00-00', '0000-00-00', '00:00:00', '0.00', '0.00', '0.00'),
(32, 14, '2016-10-18', '0000-00-00', '0000-00-00', '00:00:00', '0.00', '0.00', '0.00'),
(33, 14, '2016-10-18', '2016-09-23', '2016-10-14', '09:20:00', '93.33', '20.00', '73.33'),
(34, 14, '2016-10-20', '0000-00-00', '0000-00-00', '00:00:00', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `projectid` int(11) NOT NULL,
  `deptid` int(11) DEFAULT NULL,
  `projecttitle` varchar(100) DEFAULT NULL,
  `projectdesc` text,
  `hoursworked` time DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`projectid`, `deptid`, `projecttitle`, `projectdesc`, `hoursworked`, `active`) VALUES
(3, 13, 'korim', '', '00:00:00', 's'),
(5, 15, 'asdasda', 'sdasd', '00:00:00', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `salaryid` int(10) NOT NULL,
  `empid` int(10) DEFAULT NULL,
  `baseyear` date DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`salaryid`, `empid`, `baseyear`, `note`) VALUES
(3, 14, '2016-00-00', 'dfgdgdfgdfg'),
(4, 0, '0000-00-00', 'sdf'),
(5, 0, '0000-00-00', 'sdf'),
(6, 0, '0000-00-00', 'sdf'),
(7, 0, '0000-00-00', 'sdf'),
(8, 0, '0000-00-00', 'sdf'),
(9, 0, '0000-00-00', 'sdf'),
(10, 0, '0000-00-00', 'sdf'),
(11, 0, '0000-00-00', 'sdf');

-- --------------------------------------------------------

--
-- Table structure for table `sickday`
--

CREATE TABLE `sickday` (
  `sickid` int(11) NOT NULL,
  `empid` int(11) DEFAULT NULL,
  `datesick` date DEFAULT NULL,
  `payment` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sickday`
--

INSERT INTO `sickday` (`sickid`, `empid`, `datesick`, `payment`) VALUES
(8, 0, '0000-00-00', '0.00'),
(10, 15, '2016-09-19', '0.00'),
(11, 16, '2016-09-19', '0.00'),
(12, 15, '2016-09-19', NULL),
(13, 14, '2016-09-24', NULL),
(14, 14, '2016-10-04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `timesheet`
--

CREATE TABLE `timesheet` (
  `timeid` int(11) NOT NULL,
  `empid` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `checkin` varchar(10) DEFAULT NULL,
  `checkout` varchar(10) DEFAULT NULL,
  `rawtime` time DEFAULT NULL,
  `roundtime` time DEFAULT NULL,
  `workdesc` text,
  `ipcheckin` time DEFAULT NULL,
  `ipcheckout` time DEFAULT NULL,
  `checked` varchar(5) DEFAULT 'NO',
  `t_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timesheet`
--

INSERT INTO `timesheet` (`timeid`, `empid`, `projectid`, `checkin`, `checkout`, `rawtime`, `roundtime`, `workdesc`, `ipcheckin`, `ipcheckout`, `checked`, `t_date`) VALUES
(61, 0, 3, '06:27:00', NULL, NULL, '02:02:55', 'sdfsdf', NULL, NULL, 'sdfsd', '2016-07-20'),
(62, 0, 3, '06:37:00', NULL, NULL, NULL, 'sdfsdf', NULL, NULL, 'sdfsd', '2016-07-20'),
(63, 0, 3, '06:38:00', NULL, NULL, NULL, 'dsfdsf', NULL, NULL, 'sdfsd', '2016-07-20'),
(78, 10, 3, NULL, NULL, NULL, NULL, 'sdf', NULL, NULL, 'A', '2016-09-19'),
(81, 16, 3, '', '04:35 PM', NULL, NULL, 'dfgdfgd', NULL, NULL, 'A', '2016-09-19'),
(84, 10, 3, '04:35 AM', NULL, NULL, NULL, 'sdfsdf', NULL, NULL, 'P', '2016-09-19'),
(85, 10, 3, '04:38 AM', NULL, NULL, NULL, 'sdf', NULL, NULL, 'P', '2016-09-19'),
(86, 14, 3, '03:52 AM', '05:09 PM', NULL, '01:17:00', 'e', NULL, NULL, 'P', '2016-09-24'),
(88, 14, 3, NULL, NULL, NULL, NULL, 'sdsf', NULL, NULL, 'A', '2016-09-24'),
(89, 16, 3, '05:14 AM', NULL, NULL, NULL, 'q', NULL, NULL, 'P', '2016-09-24'),
(90, 15, 3, '05:15 AM', NULL, NULL, NULL, 's', NULL, NULL, 'P', '2016-09-24'),
(91, 14, 3, '05:18 PM', NULL, NULL, NULL, 'dddddd', NULL, NULL, 'P', '2016-09-24'),
(92, 15, 5, '05:19 PM', NULL, NULL, NULL, 'w', NULL, NULL, 'P', '2016-09-24'),
(93, 15, 3, '05:21 PM', NULL, NULL, NULL, 's1', NULL, NULL, 'P', '2016-09-24'),
(94, 14, 3, '05:21 AM', NULL, NULL, NULL, 'sdf', NULL, NULL, 'P', '2016-09-23'),
(95, 14, 3, '05:22 AM', NULL, NULL, NULL, 'sdf', NULL, NULL, 'P', '2016-09-23'),
(96, 10, 3, '05:22 PM', '05:43 PM', NULL, '00:21:00', 'sdf', NULL, NULL, 'P', '2016-09-24'),
(97, 14, 3, '05:43 PM', '05:46 PM', NULL, '00:03:00', 'sd', NULL, NULL, 'P', '2016-09-24'),
(98, 14, 3, '06:34 PM', NULL, NULL, NULL, '', NULL, NULL, 'P', '2016-10-03'),
(99, 14, 3, NULL, NULL, NULL, NULL, '', NULL, NULL, 'A', '2016-10-04'),
(100, 14, 3, '07:15 PM', NULL, NULL, NULL, '', NULL, NULL, 'P', '2016-10-04'),
(101, 14, 3, '02:00 AM', NULL, NULL, NULL, '', NULL, NULL, 'P', '2016-10-14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`bonusid`);

--
-- Indexes for table `deductions`
--
ALTER TABLE `deductions`
  ADD PRIMARY KEY (`deduid`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`deptid`);

--
-- Indexes for table `deptevents`
--
ALTER TABLE `deptevents`
  ADD PRIMARY KEY (`eventid`);

--
-- Indexes for table `empcategory`
--
ALTER TABLE `empcategory`
  ADD PRIMARY KEY (`catid`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`empid`);

--
-- Indexes for table `employeetype`
--
ALTER TABLE `employeetype`
  ADD PRIMARY KEY (`typeid`);

--
-- Indexes for table `emppicture`
--
ALTER TABLE `emppicture`
  ADD PRIMARY KEY (`pidd`,`empid`);

--
-- Indexes for table `fpage`
--
ALTER TABLE `fpage`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `fpage_category`
--
ALTER TABLE `fpage_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`holid`);

--
-- Indexes for table `hourly`
--
ALTER TABLE `hourly`
  ADD PRIMARY KEY (`hourid`);

--
-- Indexes for table `iptable`
--
ALTER TABLE `iptable`
  ADD PRIMARY KEY (`ipid`,`empid`);

--
-- Indexes for table `jobtitle`
--
ALTER TABLE `jobtitle`
  ADD PRIMARY KEY (`jobid`);

--
-- Indexes for table `locks`
--
ALTER TABLE `locks`
  ADD PRIMARY KEY (`lockid`,`empid`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`Imid`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`payrollid`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`projectid`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`salaryid`);

--
-- Indexes for table `sickday`
--
ALTER TABLE `sickday`
  ADD PRIMARY KEY (`sickid`);

--
-- Indexes for table `timesheet`
--
ALTER TABLE `timesheet`
  ADD PRIMARY KEY (`timeid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `bonusid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `deductions`
--
ALTER TABLE `deductions`
  MODIFY `deduid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `deptid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `deptevents`
--
ALTER TABLE `deptevents`
  MODIFY `eventid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `empcategory`
--
ALTER TABLE `empcategory`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `empid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `employeetype`
--
ALTER TABLE `employeetype`
  MODIFY `typeid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `emppicture`
--
ALTER TABLE `emppicture`
  MODIFY `pidd` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `fpage`
--
ALTER TABLE `fpage`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `fpage_category`
--
ALTER TABLE `fpage_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `holid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `hourly`
--
ALTER TABLE `hourly`
  MODIFY `hourid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `iptable`
--
ALTER TABLE `iptable`
  MODIFY `ipid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jobtitle`
--
ALTER TABLE `jobtitle`
  MODIFY `jobid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `locks`
--
ALTER TABLE `locks`
  MODIFY `lockid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `Imid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `payrollid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `projectid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `salaryid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sickday`
--
ALTER TABLE `sickday`
  MODIFY `sickid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `timesheet`
--
ALTER TABLE `timesheet`
  MODIFY `timeid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
