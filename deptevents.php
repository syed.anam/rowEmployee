<?php
include 'inc/connect.php';
include 'inc/template.php';
?>

<style>
    .table1{background-color: #E4E6E8}
    /*       table tr span{ color: red; font-weight: bold;}*/
    small{color:red;}
    .buttonr{width: 5; height: 10; border-radius: 50%; background-color: #aaaaaa}
</style>

<!-- -------------container-------------->     
<div id="page-wrapper" style="padding:20px;">
    <div class="container-fluid">
        <h1>Department Event</h1>
        <hr/>    
        <!--===========  form=========-->
        <div class="row">



            <button type="button" class="btn btn-sm btn-success glyphicon glyphicon-plus" data-toggle="modal" data-target="#add_user"></button>
            <a href="depteventsview.php" class="btn btn-sm btn-default glyphicon glyphicon-print"></a>

            <!--=========================search=======================-->
            <!--            <form class="pull-right" action="depteventssearch.php" method="post" style="max-width: 200px;">
            
            
                            <div class="input-group"> 
                                <input type="text" name="add" id="distict" class="form-control" placeholder="enter your key"/>
                                <div class="input-group-btn">
                                    <button type="submit" name="save" class="btn btn-primary glyphicon glyphicon-search"></button>
                                </div>
            
                            </div>
                        </form>-->
            <!--====================================search===============-->


            <div class="modal fade" id="add_user" role="dialog">

                <div class="modal-dialog modal-lg">

                    <div class="modal-content">

                        <div class="modal-header text-success text-center" style="background-color:#01421A; color:white"><h2>Department Event</h2></div>
                        <div class="modal-body">
                            <form id="save" action="" method="post">
                                <table class="table table-hover table1">                                       
                                    <tr>
                                        <th>Department Name</th>
                                        <td><select id="deptid_name" name="deptid" onchange="names('deptid_name')" onkeyup="names('deptid_name')" class="form-control">
                                                <option value="">select any</option>

                                                <?php
                                                $sq4 = "select * from department";
                                                $query2 = mysqli_query($connnect, $sq4);
                                                while ($sdata = mysqli_fetch_array($query2)) {
                                                    ?>    


                                                    <option value="<?php echo $sdata['deptid'] ?>"><?php echo $sdata['deptname']; ?></option>

                                                <?php } ?>   
                                            </select>
                                            <span id="deptid_name_error" style="color:red;">*</span>
                                        </td>
                                    </tr>



                                    <tr>
                                        <th>Date of Event</th>
                                        <td>
                                            <input type="text" class="form-control" id="eventdate_name" name="eventdate" onchange="names('eventdate_name')" onkeyup="names('eventdate_name')">
                                            <span id="eventdate_name_error" style="color:red;"></span>
                                        </td>

                                    </tr>


                                    <tr>
                                        <th>Event Time</th>
                                        <td>
                                            <input type="text" class="form-control" id="eventime_name" name="eventime" onchange="names('eventime_name')" onkeyup="names('eventime_name')">
                                            <span id="eventime_name_error" style="color:red;"></span>
                                        </td>

                                    </tr>

                                    <tr>
                                        <th>Event Body</th>
                                        <td>
                                            <textarea rows="5" cols="20" class="form-control" id="evenbody_name" name="evenbody" onchange="names('evenbody_name')" onkeyup="names('evenbody_name')"></textarea>
                                            <span id="evenbody_name_error" style="color:red;"></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Posted By</th>
                                        <td>
                                            <input type="text" class="form-control" id="postedby_name" name="postedby" onchange="names('postedby_name')" onkeyup="names('postedby_name')">
                                            <span id="postedby_name_error" style="color:red;"></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Expiry Date</th>
                                        <td>
                                            <input type="text" class="form-control" id="expirydate_name" name="expirydate" onchange="names('expirydate_name')" onkeyup="names('expirydate_name')">
                                            <span id="expirydate_name_error" style="color:red;"></span>
                                        </td>
                                    </tr>

                                </table> 

                        </div>
                        <div class="modal-footer" style="background-color:#01421A; color:white">
                            <button type="submit" class="btn btn-sm btn-success" name="save" value="Submit">Submit</button>
                            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!--======================form end=============-->


    <!--==============table==============-->
    <div class="row" style="padding:15px;">
        <center>
            <!--this heading_of_table class in the sb-admin.css file=======-->
            <table class="table table-bordered text-center example" id="data_table_id">
                <thead>
                    <tr class="heading_of_table">
                        <th class="text-center">SL</th>
                        <th class="text-center">Department Name</th>
                        <th class="text-center">Event Date</th>
                        <th class="text-center">Event Time</th>

                        <?php
                        $access_user = $_SESSION['access_l'];
                        if ($access_user == 1) {
                            ?>                  
                            <th class="text-center">Active/Finished</th>
                            <th class="text-center">Delete</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">View</th>
                        <?php } elseif ($access_user == 2) { ?>

                            <th class="text-center">View</th>
                        <?php } ?>                

                    </tr>
                </thead>
                <tbody>
                    <?php
                    include 'inc/connect.php';
                    $psql = "SELECT
                    `department`.`deptname`
                    , `deptevents`.`eventid`
                    , `deptevents`.`deptid`
                    , `deptevents`.`eventdate`
                    , `deptevents`.`eventime`
                    , `deptevents`.`evenbody`
                    , `deptevents`.`postedby`
                    , `deptevents`.`dateposted`
                    , `deptevents`.`expirydate`
                    , `deptevents`.`active`
                FROM
                    `employee`.`department`
                    INNER JOIN `employee`.`deptevents` 
                        ON (`department`.`deptid` = `deptevents`.`deptid`)";
                    //$psql="select * from deptevents";
                    $pquery = mysqli_query($connnect, $psql);
                    $i = 1;
                    while ($data = mysqli_fetch_array($pquery)) {
                        $active = $data['active'];
                        ?>    



                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data['deptname'] ?></td>
                            <td><?php echo $data['eventdate']; ?></td>
                            <td><?php echo $data['eventime']; ?></td>

                            <?php
                            $access_user = $_SESSION['access_l'];
                            if ($access_user == 1) {
                                ?>
                                <td>
                                    <?php if (($active) == '0') { ?>
                                        <a href="deptevents_active_deactive.php?active=<?php echo $data['eventid']; ?>" class="btn btn-sm btn-warning glyphicon glyphicon-off"></a>
                                    <?php }if (($active) == '1') { ?>
                                        <a href="deptevents_active_deactive.php?active=<?php echo $data['eventid']; ?>" class="btn btn-sm btn-primary glyphicon glyphicon-ok-sign"></a>
                                    <?php }; ?>

                                </td>


                                <td><button data-target="#delete_nc" data-toggle="modal" class="btn btn-sm btn-danger glyphicon glyphicon-trash" id="<?php echo $data['eventid'] ?>"></button></td>
                                <td><button data-target="#update_nc" data-toggle="modal" class="btn btn-sm btn-info glyphicon glyphicon-edit" id="<?php echo $data['eventid'] ?>"></button></td>
                                <td><a href="deptevents_emp_view.php?eventid=<?php echo $data['eventid']; ?>" class="btn btn-sm btn-success glyphicon glyphicon-eject"></a></td>
                            <?php } elseif ($access_user == 2) { ?>

                                <td><a href="deptevents_emp_view.php?eventid=<?php echo $data['eventid']; ?>" class="btn btn-sm btn-success glyphicon glyphicon-eject"></a></td>

                            <?php } ?> 
                        </tr>

                    <?php } ?> 
                </tbody>
            </table>
        </center>
    </div>

    <!--==============table end==============-->




    <!-- -------------container-------------->    

</div>
<!-- -------------page wrapper-------------->  



<!--===========================delete modal================-->
<div id="delete_nc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 style="color:red;">Warning</h3>
            </div>
            <div class="modal-body">
                <input type="hidden" id="pre_id" value=""/>
                <p>Are you Sure to Want to Delete your Data?</p>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-danger" id="delete">Yes</button>
                <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>


</div>  


<!--===========================delete modal================-->


<!--================================update=================-->



<div id="update_nc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <strong><h3>Update your Department Event</h3></strong>
            </div>
            <div class="modal-body">
                <div id="up"></div>

            </div>
            <div class="modal-footer">
<!--                    <td><input class="btn btn-primary" type="submit" name="save" value="Submit"/></td>
                <td><button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button></td>-->
            </div>
        </div>

    </div>


</div>


<!--================================update=================-->


<script>
    $(document).ready(function () {
        var base_url = "http://localhost/employee/";


        $('#save').submit(function (e) {
            e.preventDefault();

            var deptid = $.trim($('#deptid_name').val());
            var eventdate = $.trim($('#eventdate_name').val());
            var eventime = $.trim($('#eventime_name').val());
            var evenbody = $.trim($('#evenbody_name').val());
            var postedby = $.trim($('#postedby_name').val());
            var expirydate = $.trim($('#expirydate_name').val());

            if (deptid === '' || eventdate === '' || eventime === '' || evenbody === '' || postedby === '' || expirydate === '') {
                if (deptid == '') {
                    $('#deptid_name_error').html('Please Enter Department Name');
                    $('#deptid_name').css("border-color", "red");
                }


                if (eventdate == '') {
                    $('#eventdate_name_error').html('Please Enter Your event date');
                    $('#eventdate_name').css("border-color", "red");
                }

                if (eventime == '') {
                    $('#eventime_name_error').html('Please Enter Your event time');
                    $('#eventime_name').css("border-color", "red");
                }

                if (evenbody == '') {
                    $('#evenbody_name_error').html('Please Enter Your event details');
                    $('#evenbody_name').css("border-color", "red");
                }

                if (postedby == '') {
                    $('#postedby_name_error').html('Please Enter Your event posted name');
                    $('#postedby_name').css("border-color", "red");
                }


                if (expirydate == '') {
                    $('#expirydate_name_error').html('Please Enter Your event End Date');
                    $('#expirydate_name').css("border-color", "red");
                }


            } else {
                $.ajax({
                    url: base_url + 'deptevents_insert.php',
                    method: 'post',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function () {
                        window.location.assign('deptevents.php');
                    }
                })
            }
        });
    });


    function names(id) {
        var val = $.trim($('#' + id).val());
        if (val === '') {
            $('#' + id + '_error').html('');
            $('#' + id).css('border-color', 'red');
        } else {
            $('#' + id + '_error').html('');
            $('#' + id).css('border-color', 'green');
        }
    }



//   ======================delete====================
    $('.example').delegate('button', 'click', function (event) {
        var id = event.target.id;
        $('#pre_id').val(id);

    })
    $('#delete').click(function () {
        var base_url = "http://localhost/employee/";
        var id = $('#pre_id').val();
        $.ajax({
            url: base_url + 'depteventsdelete.php',
            method: 'post',
            data: {id: id},
            success: function () {
                window.location.assign('deptevents.php');

            }
        })

    });
//   ======================delete====================

//===========================update======================


    $('.example').delegate('button', 'click', function (event) {
        var base_url = "http://localhost/employee/";
        var id = event.target.id;
        $('#pre_id').val(id);
        $.ajax({
            url: base_url + 'deptevents_update_form.php',
            method: 'post',
            data: {id: id},
            success: function (data) {
                $("#up").html(data);
                //alert(data);

            }
        })

    })

    $("#update_c").submit(function (event) {
        event.preventDefault();
        var base_url = "http://localhost/employee/";
        $.ajax({
            url: base_url + 'depteventsupdate.php',
            method: 'post',
            data: new FormData(this),
            processData: false,
            processType: false,
            success: function () {
                //  window.location.assign('index.php');


            }

        })

    })





//==========================update============



</script>