<?php
include 'inc/template.php';
include 'inc/connect.php';
//include 'inc/img_upload.php';
?>
<?php
if (isset($_POST['save'])) {
    include 'inc/img_upload.php';

    $emname = $_POST['empname'];
    $empitype = $_POST['eptype'];
    $filename = $_POST['finame'];
    $fisize = $_POST['fsize'];
    $fitype = $_POST['ftype'];
    $image = $path;

    $sql = "insert into emppicture(empid,emp_pic_type,filename,filesize,filetype,picture)values('$emname','$empitype','$filename','$fisize','$fitype','$image')";
    $query = mysqli_query($connnect, $sql);
//    if($query){
//        echo 'your data is successfuly inserted';
//    }  else {
//        echo 'your data is not insert';    
//    }
}
?>


<!-- -------------container-------------->     
<div id="page-wrapper">
    <div class="container-fluid">   
        <!---------------start form--------------------->
        <div class="row" style="margin-top:30px;">

            <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#add_user">Add</button>
            <a href="#" onclick="window.print()" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <div class="modal fade" id="add_user" role="dialog">

                <div class="modal-dialog modal-lg">

                    <div class="modal-content">

                        <div class="modal-header text-success text-center" style="background-color:#01421A; color:white"><h2>Employee Picture Table</h2></div>
                        <div class="modal-body">

                            <form action="" method="post" enctype="multipart/form-data">
                                <table class="table table-hover table1">
                                <!--        <tr>
                                            <th>Picture Id</th>
                                            <td><input type="text" name="" class="form-control" maxlength="10"/><span>*</span></td>
                                        </tr> -->
                                    <tr>
                                        <th>Employee Name</th>
                                        <td>
                                            <select name="empname" class="form-control" >
                                                <option value="">[Select From Dropdown]</option>
                                                <?php
                                                $sqlll = "select * from employee";
                                                $queryyyy = mysqli_query($connnect, $sqlll);
                                                while ($sdata = mysqli_fetch_array($queryyyy)) {
                                                    ?>    
                                                    <option value="<?php echo $sdata['empid'] ?>"><?php echo $sdata['firstname']; ?> <?php echo $sdata['lastname']; ?></option>
                                                <?php } ?>   
                                            </select>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <th>Employee Picture Type</th>
                                        <td><input type="text" name="eptype" class="form-control"/><span>*</span></td>
                                    </tr>
                                    <tr>
                                        <th>File Name</th>
                                        <td><input type="text" name="finame" class="form-control"/><span>*</span></td>
                                    </tr>
                                    <tr>
                                        <th>File Size</th>
                                        <td><input type="text" name="fsize" class="form-control"/><span>*</span></td>
                                    </tr>
                                    <tr>
                                        <th>File Type</th>
                                        <td><input type="text" name="ftype" class="form-control"/><span>*</span></td>
                                    </tr>
                                    <tr>
                                        <th>Picture</th>
                                        <td><input type="file" name="imgFile" class="form-control" /></td>       
                                    </tr>
                                    <span style="color:red; font-weight: bold">*</span><small >Require must be fill</small>
                                </table> 

                        </div>
                        <div class="modal-footer" style="background-color:#01421A;">
                            <input type="submit" class="btn btn-sm btn-warning" name="save" value="Submit">
                            <input type="button" class="btn btn-sm btn-danger" data-dismiss="modal" value="Cancel">
                        </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!------------------------End form------------------------>

    <!-----------------------------start table---------------->
    <div class="row">
        <center>
            <h3>Employee Picture Details</h3>
            <!--this heading_of_table class in the sb-admin.css file=======-->
            <table class="table table-bordered text-center example" id="data_table_id">
                <thead>
                    <tr class="heading_of_table">
                        <th class="text-center">SL</th>
                        <th class="text-center">Employee Name</th>
                        <th class="text-center">Employee Picture Type</th>                
                        <th class="text-center">File Name</th>
                        <th class="text-center">File Size</th>
                        <th class="text-center">File Type</th>
                        <th class="text-center">Picture</th>
                        <?php
                        $access_user = $_SESSION['access_l'];
                        if ($access_user == 1) {
                            ?>    
                            <th class="text-center">Delete</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">View</th>
                        <?php } elseif ($access_user == 2) { ?>
                            <th class="text-center">View</th>                
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $base_url = "http://localhost/employee_management/";
                    $typese = "select * from emppicture";
                    $query = mysqli_query($connnect, $typese);
                    $i = 1;
                    while ($data = mysqli_fetch_array($query)) {
                        ?>
                        <tr>
                            <td><?php echo $i++ ?></td> 
                            <td><?php echo $data['empid'] ?></td>
                            <td><?php echo $data['emp_pic_type'] ?></td>
                            <td><?php echo $data['filename'] ?></td>
                            <td><?php echo $data['filesize'] ?></td>
                            <td><?php echo $data['filetype'] ?></td>
                            <td><img src="<?php echo $base_url . $data['picture']; ?>" width="80" height="80"/></td>
                            <?php
                            $access_user = $_SESSION['access_l'];
                            if ($access_user == 1) {
                                ?>
                                <td><a href="empicture_del.php?lid=<?php echo $data['pidd'] ?>" class="btn btn-sm btn-danger glyphicon glyphicon-trash"></a></td>
                                <td><a href="empicture_update.php?lid=<?php echo $data['pidd'] ?>" class="btn btn-sm btn-warning glyphicon glyphicon-edit"></a></td>
                                <td><a href="empicture_view.php?lid=<?php echo $data['pidd'] ?>" class="btn btn-sm btn-success glyphicon glyphicon-eject"></a></td>
                            <?php } elseif ($access_user == 2) { ?>
                                <td><a href="empicture_view.php?lid=<?php echo $data['pidd'] ?>" class="btn btn-sm btn-success glyphicon glyphicon-eject"></a></td>
                                <?php } ?>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </center>
    </div>
    <!----------------------------End table------------------->
</div>
<!-- -------------container-------------->    




</body>

</html>
