<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>eMs</title>
		
        
        <!-- bootstrap -->
        <link rel="stylesheet" href="fcss/bootstrap.min.css" />
	<link rel="stylesheet" href="fcss/bootstrap-theme.min.css" />

        <!-- favicon -->
        <?php 
                  include_once './inc/connect.php';
                  
                  $query = mysqli_query($connnect, "select * from fpage where cat_id='5' order by page_id desc limit 1");
//                            
                  foreach ($query as $data){
                ?>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $data['page_picture'];?>">
        <?php }?>
        <!--<link rel="shortcut icon" type="image/x-icon" href="fimage/favicon1.ico">-->
        <!--<link rel="icon" href="fimage/favicon.ico" type="image/x-icon">-->
        <link rel="stylesheet" href="fcss/style.css">
        <!------------------------mendetory for animation--------------->
        <link rel="stylesheet" href="fcss/animate.css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">

    </head>

    <body>

        <!-- Header Starts -->
        <div class="navbar-wrapper" id="home">
            <div class="container">

                <div class="navbar navbar-default navbar-fixed-top" role="navigation" id="top-nav">
                    <div class="container">
                        <div class="navbar-header">
                            <!-- Logo Starts -->
                            <?php 
                                                            include_once './inc/connect.php';
                                                            
                                                            $query = mysqli_query($connnect, "select * from fpage where cat_id='1' order by page_id desc limit 1");
//                            
                                                            foreach ($query as $data){
                                                        ?>
                            <a class="navbar-brand" href="#home"><img src="<?php echo $data['page_picture'] ?>" alt="logo" style="max-height:68px;max-width: 330px; "></a>
                            <!-- #Logo Ends -->
                                <?php  };?>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                        </div>


                        <!-- Nav Starts -->
                        <div class="navbar-collapse  collapse">
                            <ul class="nav navbar-nav navbar-right scroll">
                                <li ><a href="#home">Home</a></li>
                                <li ><a href="#about">About</a></li>
                                <li ><a href="#Service">Service</a></li>
                                <li ><a href="#Management">Management</a></li>
                                <li ><a href="#contact">Contact</a></li>
				<li ><a href="log.php">Login</a></li>								
                            </ul>
                        </div>
                        <!-- #Nav Ends -->

                    </div>
                </div>

            </div>
        </div>
        <!-- #Header Starts -->

        <!-------------------------bannar----------------------->
        <div class="container-fluid">
            <div class="row banner_home">
                <img src="fbanner/7.jpg"/>
            </div>
        </div>
        <!-------------------------bannar----------------------->

        <!-----------------------------ABout------------------>
        <div class="container-fluid main_content" id="about">
            <div class="row">
                <div class="container">
                    <h2 class="text-center wowload fadeInUp">Welcome to our company</h2>
                    <div class="col-sm-6 sub_col_content wowload fadeInLeft">
                        <h3>Classic</h3>
                        <p>Search the world's information, including webpages,
                            images, videos and more. Google has many special 
                            features to help you find exactly what you're looking
                            Search the world's information, including webpages, 
                            images, videos and more. Google has many special 
                            features to help you find exactly what you're looking
                            Search the world's information, including webpages, 
                            images, videos and more. Google has many special 
                            features to help you find exactly what you're looking
                        </p>
                    </div>


                    <div class="col-sm-6 sub_col_content wowload fadeInRight">
                        <h3>This our own way</h3>
                        <p>Search the world's information, including webpages,
                            images, videos and more. Google has many special 
                            features to help you find exactly what you're looking
                            Search the world's information, including webpages, 
                            images, videos and more. Google has many special 
                            features to help you find exactly what you're looking
                            Search the world's information, including webpages, 
                            images, videos and more. Google has many special 
                            features to help you find exactly what you're looking
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!-----------------------------ABout------------------>


        <!-------------------------------Icons------------------>
        <div class="container icons_header" id="Service">
            <div class="row">
                <center>
                    <h3 class="wowload fadeInDown" >New Service</h3>
                    <div class="col-sm-3 icons_content animated slideInLeft"><img src="ficon/applications.png"/></div>
                    <div class="col-sm-3 icons_content wowload fadeInDown"><img src="ficon/global.png"/></div>
                    <div class="col-sm-3 icons_content animated slideInRight"><img src="ficon/newhelp.png"/></div>
                   
                </center>
            </div>
        </div>

        <!-------------------------------Icons------------------>


        <!------------------------------Our clients------------------>
        <div class="container-fluid notice_brand">
            <div class="row">
                <center><h2 class="wowload fadeInUp">Notice and Brand</h2></center>
                <div class="container sub_brand">
                    <!------------------brand------------------>
                    <div class="col-sm-6 wowload fadeInLeft">
                        <div class="row">
                            <div class="col-sm-6 brand_logo"><img src="fbrand/1.jpg"/></div>
                            <div class="col-sm-6 brand_logo"><img src="fbrand/2.jpg"/></div>
                        </div>

                        <div class="row wowload fadeInLeft">
                            <div class="col-sm-6 brand_logo"><img src="fbrand/3.jpg"/></div>
                            <div class="col-sm-6 brand_logo"><img src="fbrand/4.jpg"/></div>
                        </div>
                    </div>
                    <!------------------brand------------------>

                    <!------------------------notice-------------------->            

                    <div class="col-sm-6">
                        <div class="row notice wowload fadeInRight">
                            <p>
                                images, videos and more. Google has many special 
                                features to help you find exactly what you're looking
                                Search the world's information, including webpages, 
                                images, videos and more. Google has many special 
                                features to help you find exactly what you're looking
                                Search the world's information, including webpages, 
                                images, videos and more. Google has many special 
                                features to help you find exactly what you're looking
                            </p>
                        </div>

                    </div>
                    <!------------------------notice-------------------->            

                </div>

            </div>


        </div>

        <!------------------------------Our clients------------------>

        <!--------------------------management------------------------->

        <div class="container-fluid management" id="Management">
            <div class="row" style="padding: 3%">
                <center>
                    <h2 class="wowload fadeInLeft" style="margin-bottom: 30px">Management Team</h2>
                    <p class="wowload fadeInRight" style="margin-bottom: 30px">Our best management team in this hole company for ever</p>
                    <div class="col-md-3 management_group animated slideInRight"><img src="fimage/1.jpg"/></div>
                    <div class="col-md-3 management_group animated slideInLeft"><img src="fimage/2.jpg"/></div>
                    <div class="col-md-3 management_group animated slideInRight"><img src="fimage/3.jpg"/></div>
                    <div class="col-md-3 management_group animated slideInLeft"><img src="fimage/1.jpg"/></div>
                </center>
            </div>
        </div>


        <!--------------------------management------------------------->

        <!------------------------------------------contact us------------------------------------->
        <div class="container-fluid contact_us" id="contact">
            <center>
                <h1 class="animated slideInUp">Contact with us for your any important</h1>
                <div class="row" style="margin-top: 50px;">
                    <div class="col-sm-6 col-sm-offset-3 col-xs-12 wowload fadeInLeft">
                        <form>
                        <input placeholder="Name" type="text">
                        <input placeholder="Company" type="text">
                        <textarea rows="5" placeholder="Message"></textarea>
                        <button class="btn btn-primary"><i class="fa fa-paper-plane"></i>Send</button>
                        </form>
                    </div>
                </div>
            </center>
        </div>

        <!------------------------------------------contact us------------------------------------->

        <!--------------------------------------footer------------------------->
        <div class="container-fluid footer_own">
            <center>
                <div class="row">
                    <div class="col-sm-6 sub_footer">
                        <span>Shapan@gmail.com</span>
                    </div>

                    <div class="col-sm-6 sub_footer bb">
                        <a href="#"><i class="fa fa-lg fa-facebook sub_footer"></i></a>
                        <a href="#"><i class="fa fa-lg fa-twitter sub_footer"></i></a>
                        <a href="#"><i class="fa fa-lg fa-youtube sub_footer"></i></a>
                        <a href="#"><i class="fa fa-lg fa-google-plus sub_footer"></i></a>

                    </div>
                </div>
            </center>
        </div>

        <!--------------------------------------footer------------------------->



        <a class="gototop" href="#home"><i class="fa fa-lg fa-arrow-circle-up"></i></a>

        <!-- jquery -->
        <script src="fjs/jquery.js"></script>
        <!--------------------------------mandetory for amination--------------------->
        <script src="fjs/wow.min.js"></script>
                <!--------------------------------mandetory for amination--------------------->
        <script src="fjs/script.js"></script>
                        <!--------------------------------mandetory for amination--------------------->

        <!-- boostrap -->
        <script src="fjs/bootstrap.js" type="text/javascript" ></script>

    </body>
</html>