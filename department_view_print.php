<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Employee management | Department</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>

    </head>


    <body>
        <!-- -------------container--------------> 

        <div id="page-wrapper" style="padding:25px 25px;">
            <a href="department.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>            
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <a href="department_view_pdf.php" class="btn btn-sm btn-default"><img src="icon/pdf.png" width="17px" height="17px"/></a>
<button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>

            <div class="container-fluid parea">


                <!--===========  form=========-->
                <center>
                    <img src="icon/logoprint.png" style="max-width: 300px; max-height: 150px"/>


                    <table class="table table-hover text-center table-responsive excelTable" border="1">
                        <tr>
                            <td colspan="6"><center><h3>Department View</h3></center></td>
                        </tr>
                        <tr>
                            <th class="text-center">SL</th>
                            <th class="text-center">Employee Name</th>
                            <th class="text-center">Department Name</th>
                            <th class="text-center">Location</th>
                            <th class="text-center">Department description</th>
                            <th class="text-center">Manager work description</th>
                        </tr>

                        <?php
                        $deparmentv = "SELECT
    `employee`.`firstname`
    , `employee`.`lastname`
    , `department`.`deptid`
    , `department`.`empid`
    , `department`.`deptname`
    , `department`.`location`
    , `department`.`deptdesc`
    , `department`.`mandaworkdesc`
    , `department`.`messaging`
FROM
    `employee`.`employee`
    INNER JOIN `employee`.`department` 
        ON (`employee`.`empid` = `department`.`empid`)";

                        $query = mysqli_query($connnect, $deparmentv);
                        $i = 1;
                        while ($data = mysqli_fetch_array($query)) {
                            ?>
                            <tr>
                                <td><?php echo $i++ ?></td>
                                <td><?php echo $data['firstname']; ?> <?php echo $data['lastname']; ?></td>
                                <td><?php echo $data['deptname']; ?></td>
                                <td><?php echo $data['location']; ?></td>
                                <td><?php echo $data['deptdesc']; ?></td>
                                <td><?php echo $data['mandaworkdesc']; ?></td>
                            </tr>

                        <?php } ?>

                    </table>
                </center>
            </div>

            <!--==============table end==============-->

        </div>
        <!-- -------------container-------------->
    </body>
</html>
