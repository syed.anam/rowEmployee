<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Job Title view</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>


    </head>


    <body>
        <!-- -------------container--------------> 

        <div id="page-wrapper" style="padding:25px 25px;">
            <a href="jobtitle.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>            
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <a href="jobtitle_view_pdf.php" class="btn btn-sm btn-default"><img src="icon/pdf.png" width="17" height="17"/></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>

            <div class="container-fluid parea">


                <!--===========  table=========-->
                <center>
                    <img src="icon/logoprint.png" class="img-responsive" style="max-width: 400px; max-height: 200px"/>


                    <table class="table table-hover text-center table-responsive excelTable" border="1">
                        <tr>
                            <td colspan="3"><center><h3>Job Title Sheet Detail</h3></center></td>
                        </tr>
                        <tr>
                            <th class="text-center">SL</th>
                            <th class="text-center">Job Title</th>
                            <th class="text-center">Job Description</th> 

                        </tr>


                        <?php
                        $jobtitlev = "SELECT * FROM jobtitle";
                        $query3 = mysqli_query($connnect, $jobtitlev);

                        $i = 1;

                        while ($data = mysqli_fetch_array($query3)) {
                            ?>


                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $data['jobtitle'] ?></td>
                                <td><?php echo $data['jobdesc'] ?></td>

                            </tr>


                        <?php } ?>
                    </table>
                </center>
            </div>

            <!--==============table end==============-->

        </div>
        <!-- -------------container-------------->
    </body>
</html>
