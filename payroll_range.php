<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Employee management | Payroll view</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery-ui.js"></script>

    </head>


    <body>
        <div id="page-wrapper" style="padding:25px 25px;">
            <div class="row" style="padding:25px 25px;">
            <a href="payroll.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>            
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>
            <form action="payroll_range.php" method="post" class="pull-right" style="max-width: 500px;">

                <table>
                    <td><input type="text" class="form-control" name="startnetpay" placeholder="Enter your start amount"></td>
                    <td><input type="text" class="form-control"  name="endnetpay" placeholder="Enter your end amount"></td>
                    <td><button type="submit" name="search" class="btn btn-primary glyphicon glyphicon-search"></button></td>
                </table>
            </form>
            </div>


                <!--===========  table=========-->
            <div class="container-fluid parea">


                <!--===========  table=========-->
                <center>
                    <img src="icon/logoprint.png" class="img-responsive" style="max-width: 400px; max-height: 200px"/>



                    <table class="table table-hover text-center table-responsive excelTable" border="1">
                        <tr>
                            <td colspan="8"><center><h3>Payroll Sheet Detail</h3></center></td>
                        </tr>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Hours worked</th>
                                    <th class="text-center">Gross pay</th>
                                    <th class="text-center">Net Pay</th>

                                </tr>
                                <?php
                                include 'inc/connect.php';
                                global $post_1, $post_2, $pquery;
                                if (isset($_POST["search"])) {
                                    $s1 = $_POST['startnetpay'];
                                    $s2 = $_POST['endnetpay'];


                                    $ssql = "SELECT
                                                `employee`.`firstname`
                                                , `employee`.`lastname`
                                                , `payroll`.`payrollid`
                                                , `payroll`.`empid`
                                                , `payroll`.`date`
                                                , `payroll`.`startday`
                                                , `payroll`.`endday`
                                                , `payroll`.`hoursworked`
                                                , `payroll`.`grosspay`
                                                , `payroll`.`deductions`
                                                , `payroll`.`netpay`
                                            FROM
                                                `employee`.`employee`
                                                INNER JOIN `employee`.`payroll` 
                                                    ON (`employee`.`empid` = `payroll`.`empid`) WHERE `payroll`.`netpay` BETWEEN '$s1' AND '$s2'";

                                                                                $pquery = mysqli_query($connnect, $ssql);
                                    $i = 1;
                                    while ($data = mysqli_fetch_array($pquery)) {
                                        ?>    



                                        <tr>
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo $data['firstname']; ?> <?php echo $data['lastname']; ?></td>
                                            <td><?php echo $data['date']; ?></td>
                                            <td><?php echo $data['hoursworked']; ?></td>
                                            <td><?php echo $data['grosspay']; ?></td>
                                            <td><?php echo $data['netpay']; ?></td>

                                        </tr>

                                    <?php }
                                }
                                ?>        
                            </table>
                            </center>
                        </div>

                        <!--==============table end==============-->

                        </div>
        

                        <!-- -------------container-------------->
                        </body>
                        </html>
