<?php
include'inc/connect.php';
require_once 'dompdf/dompdf_config.inc.php';

$codigoHTML='

   <center>
<img src="icon/logoprint.png" style="max-width: 400px; max-height: 200px"/>
    <h3>Department Event sheet detail</h3>

<table width="100%" border="1" style="padding:30px;">
    <tr>
        <td style="background-color:#EFEFEF">SL</th>
        <td style="background-color:#EFEFEF">Department Name</th>
        <td style="background-color:#EFEFEF">Event Date</th> 
        <td style="background-color:#EFEFEF">Event Time</th>
        <td style="background-color:#EFEFEF">Event Details</th>
        <td style="background-color:#EFEFEF">Posted By</th> 
        <td style="background-color:#EFEFEF">Expiry Date</th> 
    </tr>';




$depteventspdf="SELECT
                    `department`.`deptname`
                    , `deptevents`.`eventid`
                    , `deptevents`.`deptid`
                    , `deptevents`.`eventdate`
                    , `deptevents`.`eventime`
                    , `deptevents`.`evenbody`
                    , `deptevents`.`postedby`
                    , `deptevents`.`dateposted`
                    , `deptevents`.`expirydate`
                    , `deptevents`.`active`
                FROM
                    `employee`.`department`
                    INNER JOIN `employee`.`deptevents` 
                        ON (`department`.`deptid` = `deptevents`.`deptid`)";
$query=  mysqli_query($connnect,$depteventspdf);

$i=1;

while($data=mysqli_fetch_array($query)){   
 $codigoHTML.='
        <tr>
            <td>'.$i++.'</td>
            <td>'.$data['deptname'].'</td>
            <td>'.$data['eventdate'].'</td>
            <td>'.$data['eventime'].'</td>
            <td>'.$data['evenbody'].'</td>
            <td>'.$data['postedby'].'</td>
            <td>'.$data['expirydate'].'</td>
        </tr>';


 }
 $codigoHTML.='
</table>
</center>';


$codigoHTML= utf8_decode($codigoHTML);
$dompdf=new DOMPDF();
$dompdf->load_html($codigoHTML);
ini_set("memory_limit", "128M");
$dompdf->render();
$dompdf->stream("deptevents_pdf_view.pdf");

?>
