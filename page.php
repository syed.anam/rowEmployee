<?php
include 'inc/template.php';
include 'inc/connect.php';



if (isset($_POST['save'])) {   
//======file upload======  
    $file_name      =($_FILES['file']['name']);
    $destination    = "fpage_images/".$file_name;
    $filename       = $_FILES['file']['tmp_name'];
    $upload         = move_uploaded_file($filename, $destination);
    
    
    $page_cat_name = $_POST['page_cat_name'];
    $page_title = $_POST['page_title'];
    $page_sub_title = $_POST['page_sub_title'];
    $page_des = $_POST['page_des'];
    date_default_timezone_get('Asia/Dhaka');
    $page_date = date('Y-m-d');
    $page_time = date('g:i A') ;
    $path = $destination;

      if($upload) {
    $sql = "insert into fpage(cat_id,page_title,page_sub_title,page_des,page_date,page_time,page_picture)"
            . "values('{$page_cat_name}','{$page_title}','{$page_sub_title}','{$page_des}','{$page_date}',
'{$page_time}','{$path}')";
$query = mysqli_query($connnect, $sql);
}
}


?>


<!-- -------------container-------------->     
<div id="page-wrapper">
    <div class="container-fluid">   
        <!---------------start form--------------------->
        <div class="row" style="margin-top:30px;">

            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#add_user">Add</button>
            <a href="#" onclick="window.print()" class="btn btn-info glyphicon glyphicon-print"></a>
            <div class="modal fade" id="add_user" role="dialog">

                <div class="modal-dialog modal-lg">

                    <div class="modal-content">

                        <div class="modal-header text-success text-center" style="background-color:#01421A; color:white"><h2>Pages</h2></div>
                        <div class="modal-body">
                          <?php
                                                           $query1 = mysqli_query($connnect, "select * from fpage_category");
                                                  ?>
                          <form action="" method="post" enctype="multipart/form-data">
                                <table class="table table-hover table1"> 
                                    <tr>
                                        <th>Page Category Name</th>
                                        <td>
                                          <select name="page_cat_name"  class="form-control">
                                            <option value="">Select Category Name</option>
                                            <?php foreach ($query1 as $query_data) {?>
                                            <option value="<?php echo $query_data['cat_id']; ?>">
                                              <?php echo $query_data['cat_name']; ?>
                                            </option>                                            
                                            <?php } ?>
                                          </select><span>*</span>
                                        </td>
                                    </tr>
                                    <tr>
                                      <th>Page Title</th>
                                      <td><input type="text" name="page_title" class="form-control"/></td>
                                    </tr>
                                    <tr>
                                      <th>Page Sub Title</th>
                                      <td><input type="text" name="page_sub_title" class="form-control"/></td>
                                    </tr>
                                    <tr>
                                      <th>Page Description</th>
                                      <td><input type="text" name="page_des" class="form-control"/></td>
                                    </tr>
                                    <tr>
                                      <th>Page Picture</th>
                                      <td><input type="file" name="file" class="form-control"/></td>
                                    </tr>
                                    <span style="color:red; font-weight: bold">*</span><small >Require must be fill</small>
                                </table> 

                        </div>
                        <div class="modal-footer" style="background-color:#01421A;">
                            <input type="submit" class="btn btn-warning" name="save" value="Submit">
                            <input type="button" class="btn btn-danger" data-dismiss="modal" value="Cancel">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!------------------------End form------------------------>

    <!-----------------------------start table---------------->
    <div class="row">
        <center>
            <h3>Pages Details</h3>
            <!--this heading_of_table class in the sb-admin.css file=======-->
           
            <table class="table table-bordered text-center example" id="data_table_id">
                <thead>
                    <tr class="heading_of_table">
                        <th class="text-center">SL</th>                        
                        <th class="text-center">Category Id</th>                        
                        <th class="text-center">Page Title</th>                        
                        <th class="text-center">Page picture</th>                        
<?php
$access_user = $_SESSION['access_l'];
if ($access_user == 1) {
    ?>    
                            <th class="text-center">Delete</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">View</th>
<?php } elseif ($access_user == 2) { ?>
                            <th class="text-center">View</th>                
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
<?php
//            $ipid=$_GET['ipid'];
$sql = "select * from fpage";
$query = mysqli_query($connnect, $sql);
$i = 1;
while ($data = mysqli_fetch_array($query)) {
    ?>
                        <tr>
                            <td><?php echo $i++ ?></td>                            
                            <td><?php echo $data['cat_id'] ?></td>
                            <td><?php echo $data['page_title'] ?></td>
                            <td><img src="<?php echo $data['page_picture'] ?>" width="60" height="60" /></td>
    <?php
    $access_user = $_SESSION['access_l'];
    if ($access_user == 1) {
        ?> 
                                <td><a href="page_del.php?id=<?php echo $data['cat_id'] ?>" class="btn btn-danger glyphicon glyphicon-trash"></a></td>
                                <td><a href="page_update.php?id=<?php echo $data['cat_id'] ?>" class="btn btn-warning glyphicon glyphicon-edit"></a></td>
                                <td><a href="#<?php // echo $data['cat_id'] ?>" class="btn btn-success glyphicon glyphicon-eject"></a></td>
    <?php } elseif ($access_user == 2) { ?>
                                <td><a href="#<?php // echo $data['cat_id'] ?>" class="btn btn-success glyphicon glyphicon-eject"></a></td>
                            <?php } ?>
                        </tr>
                            <?php } ?>

                </tbody>
            </table>    
            
            
        </center>
    </div>
    <!----------------------------End table------------------->
</div>
<!-- -------------container-------------->    

<?php include 'inc/footer.php';?>