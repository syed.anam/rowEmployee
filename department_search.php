<?php
include 'inc/connect.php';
include 'inc/template.php';
?>
  <!-- -------------container-------------->    
         <div id="page-wrapper" style="padding:25px 25px;">
<div class="container-fluid">
    <h1>Department search</h1>
    <hr/>    
    <div class="row" style="margin-top:30px; margin-bottom: 10px;">
        <a href="department.php" class="btn btn-danger glyphicon glyphicon-backward pull-left"></a>
        <!--===========================search================-->
        <form action="department_search.php" method="post" class="pull-right" style="max-width: 200px;">
    
                <div class="input-group">
                    <input type="text" name="add" id="distict" class="form-control" placeholder="enter your key"/>
                <div class="input-group-btn">
                    <button type="submit" name="save" class="btn btn-primary glyphicon glyphicon-search"></button>
                  </div>
                
                 </div>
</form>
        <!--===========================search================-->
     
</div>


<!--==============table==============-->
<div class="row">
    
    
<center>
<table class="table table-hover text-center" border="1">
            <tr>
                <th class="text-center">SL</th>
                <th class="text-center">Employee Name</th>
                <th class="text-center">Department Name</th>
                <th class="text-center">Location</th>
                <th class="text-center">Department description</th>
                <th class="text-center">Manager work description</th>
                <th class="text-center">Delete</th>
                <th class="text-center">Edit</th>
                <th class="text-center">View</th>
            </tr>


                <?php
                if(isset($_POST['save'])){
                $search=$_POST['add'];
                $departmentv="SELECT
                    `employee`.`firstname`
                    , `employee`.`lastname`
                    , `department`.`deptid`
                    , `department`.`empid`
                    , `department`.`deptname`
                    , `department`.`location`
                    , `department`.`deptdesc`
                    , `department`.`mandaworkdesc`
                    , `department`.`messaging`
                FROM
                    `employee`.`employee`
                    LEFT JOIN `employee`.`department` 
                        ON (`employee`.`empid` = `department`.`empid`) 
                        where `department`.`deptname`='$search' or `department`.`deptid`='$search'";
                $query=  mysqli_query($connnect,$departmentv);
                $i=1;
                while($data=mysqli_fetch_array($query)){
                ?>
            <tr>
                <td><?php echo $i++;?></td>
                <td><?php echo $data['firstname'];?> <?php echo $data['lastname'];?></td>
                <td><?php echo $data['deptname'];?></td>
                <td><?php echo $data['location'];?></td>
                <td><?php echo $data['deptdesc'];?></td>
                <td><?php echo $data['mandaworkdesc'];?></td>
                <td><button data-target="#delete_nc" data-toggle="modal" class="btn btn-danger glyphicon glyphicon-trash" id="<?php echo $data['deptid']?>"></button></td>
                <td><button data-target="#update_nc" data-toggle="modal" class="btn btn-info glyphicon glyphicon-edit" id="<?php echo $data['deptid']?>"></button></td>
                <td><a href="department_emp_print.php?deptid=<?php echo $data['deptid'];?>" class="btn btn-success glyphicon glyphicon-eject"></a></td>
            </tr>


<?php }}?>
</table>
</center>
</div>

<!--==============table end==============-->

        </div>
        <!-- -------------container-------------->    
        
        </div>
       <!-- -------------page wrapper-------------->  

       
       <!--===========================delete modal================-->
     <div id="delete_nc" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 style="color:red;">Warning</h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="pre_id" value=""/>
                    <p>Are you Sure to Want to Delete your Data?</p>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger" id="delete">Yes</button>
                    <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">No</button>
                </div>
            </div>
            
        </div>
        
        
    </div>  
    

<!--===========================delete modal================-->


<!--================================update=================-->



<div id="update_nc" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <strong><h3>Update your Payroll</h3></strong>
                </div>
                <div class="modal-body">
                    <div id="up"></div>
                    
                </div>
                <div class="modal-footer">
<!--                    <td><input class="btn btn-primary" type="submit" name="save" value="Submit"/></td>
                    <td><button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button></td>-->
                </div>
            </div>
           
        </div>
        
        
    </div>
    

<!--================================update=================-->       
       

       
       <script>
        //   ======================delete====================
            $('.example').delegate('button','click',function(event){
                var id=event.target.id;
                $('#pre_id').val(id);

            })
            $('#delete').click(function(){
                var base_url="http://localhost/employee/";
                var id=$('#pre_id').val();
                $.ajax({
                        url:base_url+'departmentdelete.php',
                        method:'post',
                        data:{deptid:id},
                        success:function(){
                        window.location.assign('department.php');

                }      
                        })

            });
            //   ======================delete====================

            //===========================update======================


            $('.example').delegate('button','click',function(event){
                var base_url="http://localhost/employee/";
                    var id=event.target.id;
                $('#pre_id').val(id);
                $.ajax({
                    url:base_url+'department_update_form.php',
                        method:'post',
                        data:{id:id},
                        success:function(data){
                          $("#up").html(data);
                       //alert(data);

                }   
                })

            })

            $("#update_c").submit(function(event){
             event.preventDefault();
             var base_url="http://localhost/employee/";
             $.ajax({
                 url:base_url+'departmentupdate.php',
                 method:'post',
                 data: new FormData(this),
                 processData:false,
                 processType:false,
                 success:function(){
                  //  window.location.assign('index.php');


                 }

             })

            })





            //==========================update============

            $(document).ready(function(){
              var base_url="http://localhost/employee/";
              $("#distict").autocomplete({
                  source:base_url+'dept_search.php',
                  minLength:1


              });

          });     


       </script>