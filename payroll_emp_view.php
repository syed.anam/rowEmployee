<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Payroll</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>


    </head>


    <body>
        <div class="container" style="padding: 50px">
            <a href="payroll.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>


            <div class="row parea" style="padding: 30px 20px 0px 20px">
                <center>


                    <table class="table table-bordered table-responsive excelTable">
                        <tr>
                            <td colspan="2"><center><h3>Payroll Information</h3></center></td>
                        </tr>
                        <?php
                        $payrollid = $_GET['payrollid'];
                        $payrollview = "SELECT
                                            `employee`.`firstname`
                                            , `employee`.`lastname`
                                            , `payroll`.`payrollid`
                                            , `payroll`.`empid`
                                            , `payroll`.`date`
                                            , `payroll`.`startday`
                                            , `payroll`.`endday`
                                            , `payroll`.`hoursworked`
                                            , `payroll`.`grosspay`
                                            , `payroll`.`deductions`
                                            , `payroll`.`netpay`
                                        FROM
                                            `employee`.`employee`
                                            INNER JOIN `employee`.`payroll` 
                                                ON (`employee`.`empid` = `payroll`.`empid`)
                                                where `payroll`.`payrollid`='$payrollid'";
                        $query = mysqli_query($connnect, $payrollview);
                        while ($data = mysqli_fetch_array($query)) {
                            ?> 
                            <tr>
                                <th>Employee Name </th>
                                <td><?php echo $data['firstname'] ?> <?php echo $data['lastname'] ?></td>
                            </tr>
                            <tr>
                                <th>Date </th>
                                <td><?php echo $data['date']; ?></td>
                            </tr>
                            <tr>
                                <th>Start Day :</th>
                                <td><?php echo $data['startday']; ?></td>
                            </tr>
                            <tr>
                                <th>End Day </th>
                                <td><?php echo $data['endday']; ?></td>
                            </tr>
                            <tr>
                                <th>Hours Worked </th>
                                <td><?php echo $data['hoursworked']; ?></td>
                            </tr>
                            <tr>
                                <th>Gross Pay </th>
                                <td><?php echo $data['grosspay']; ?></td>
                            </tr>
                            <tr>
                                <th>Deductions </th>
                                <td><?php echo $data['deductions']; ?></td>
                            </tr>
                            <tr>
                                <th>Net Pay </th>
                                <td><?php echo $data['netpay']; ?></td>
                            </tr>       
                        <?php } ?> 
                    </table>               
                </center>
            </div>
        </div>
    </body>
</html>

