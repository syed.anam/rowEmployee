<?php
include'inc/connect.php';
require_once 'dompdf/dompdf_config.inc.php';

$codigoHTML='

   <center>
<img src="icon/logoprint.png" style="max-width: 400px; max-height: 200px"/>
    <h3>Payroll sheet detail</h3>

<table width="100%" border="1" style="padding:30px;">
    <tr>
        <td style="background-color:#EFEFEF">SL</th>
        <td style="background-color:#EFEFEF">Employee Name</th>
        <td style="background-color:#EFEFEF">Date</th> 
        <td style="background-color:#EFEFEF">Start Day</th>
        <td style="background-color:#EFEFEF">End Day</th>
        <td style="background-color:#EFEFEF">Gross Pay</th>
        <td style="background-color:#EFEFEF">Deduction</th>
        <td style="background-color:#EFEFEF">Net Pay</th>
    </tr>';




$payrollpdf="SELECT
    `employee`.`firstname`
    , `employee`.`lastname`
    , `payroll`.`date`
    , `payroll`.`startday`
    , `payroll`.`endday`
    , `payroll`.`hoursworked`
    , `payroll`.`grosspay`
    , `payroll`.`deductions`
    , `payroll`.`netpay`

FROM
    `employee`.`employee`
    INNER JOIN `employee`.`payroll` 
        ON (`employee`.`empid` = `payroll`.`empid`)";
$query=  mysqli_query($connnect,$payrollpdf);

$i=1;

while($data=mysqli_fetch_array($query)){   
 $codigoHTML.='
        <tr>
            <td>'.$i++.'</td>
            <td>'.$data['firstname'].' '.$data['lastname'].'</td>
            <td>'.$data['date'].'</td>
            <td>'.$data['startday'].'</td>
            <td>'.$data['endday'].'</td>
            <td>'.$data['grosspay'].'</td>
            <td>'.$data['deductions'].'</td>
            <td>'.$data['netpay'].'</td>
        </tr>';


 }
 $codigoHTML.='
</table>
</center>';


$codigoHTML= utf8_decode($codigoHTML);
$dompdf=new DOMPDF();
$dompdf->load_html($codigoHTML);
ini_set("memory_limit", "128M");
$dompdf->render();
$dompdf->stream("payroll_pdf_view.pdf");

?>
