<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Hourly view</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>


    </head>


    <body>
        <!-- -------------container--------------> 

        <div id="page-wrapper" style="padding:25px 25px;">
            <a href="hourly.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>            
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <a href="hourly_view_pdf.php" class="btn btn-sm btn-default"><img src="icon/pdf.png" width="17" height="17"/></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>

            <div class="container-fluid parea">


                <!--===========  table=========-->
                <center>
                    <img src="icon/logoprint.png" class="img-responsive" style="max-width: 400px; max-height: 200px"/>


                    <table class="table table-hover text-center table-responsive excelTable" border="1">
                        <tr>
                            <td colspan="4"><center><h3>Hourly Sheet Detail</h3></center></td>
                        </tr>
                        <tr>
                            <th class="text-center">SL</th>
                            <th class="text-center">Employee Name</th>
                            <th class="text-center">Hourly Rate</th> 
                            <th class="text-center">Note</th>
                        </tr>


                        <?php
                        $hourlyv = "SELECT
                                        `employee`.`firstname`
                                            , `employee`.`lastname`
                                            , `hourly`.`hourid`
                                            , `hourly`.`empid`
                                            , `hourly`.`hourlyrate`
                                            , `hourly`.`note`
                                        FROM
                                            `employee`.`employee`
                                            INNER JOIN `employee`.`hourly` 
                                                ON (`employee`.`empid` = `hourly`.`empid`)";
                        $query3 = mysqli_query($connnect, $hourlyv);

                        $i = 1;

                        while ($data = mysqli_fetch_array($query3)) {
                            ?>


                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $data['firstname'] ?> <?php echo $data['lastname'] ?></td>
                                <td><?php echo $data['hourlyrate'] ?></td>
                                <td><?php echo $data['note'] ?></td>

                            </tr>


                        <?php } ?>
                    </table>
                </center>
            </div>

            <!--==============table end==============-->

        </div>
        <!-- -------------container-------------->
    </body>
</html>
