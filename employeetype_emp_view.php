<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Employee management | Employee Type view</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>


    </head>


    <body>
        <div class="container" style="padding: 50px">
            <a href="employeetype.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>


            <di<div class="row parea" style="padding: 30px 20px 0px 20px">

                    <table class="table table-bordered table-responsive excelTable">
                        <tr>
                            <td colspan="2"><center><h3>Employee Type Information</h3></center></td>
                        </tr>
                        <?php
                        $typeid = $_GET['typeid'];
                        $employeetypeview = "SELECT * from employeetype where typeid ='$typeid'";
                        $query = mysqli_query($connnect, $employeetypeview);
                        while ($data = mysqli_fetch_array($query)) {
                            ?> 
                            <tr>
                                <th>Employee Type</th>
                                <td><?php echo $data['typename'] ?></td>
                            </tr>
                            <tr>
                                <th>Employee Description</th>
                                <td><?php echo $data['typedesc']; ?></td>
                            </tr>

                            <tr>
                                <th>Note</th>
                                <td><?php echo $data['miscnote']; ?></td>
                            </tr>

                        <?php } ?> 
                    </table>               
                    </center>
                </div>
        </div>

    </body>
</html>


