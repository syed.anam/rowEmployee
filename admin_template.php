<!-- Sidebar Menu Items -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
     <ul class="nav navbar-nav side-nav">
     <li class="active"><a href="home.php">Home</a></li>
       <li>
        <a href="#" data-toggle="collapse" data-target="#employee">Employee</a>
      <ul id="employee" class="collapse">
       <li><a href="employee.php">Employee</a></li>
       <li><a href="jobtitle.php">Jobtitle</a></li>
       <li><a href="empicture.php">Employee Picture</a></li>
       <li><a href="empcategory.php">Employee Category</a></li>
       <li><a href="employeetype.php">Employee Type</a></li>
       <li><a href="iptable.php">Ip Table</a></li>
       <li><a href="massage.php">Massage</a></li>
       <li><a href="locks.php">Locks</a></li>
       <li><a href="#">Others</a></li>
       </ul>
    </li>
    
    
<!---        <li><a href="#" data-toggle="collapse" data-target="#payroll">Payroll</a>
        <ul id="payroll" class="collapse">
        <li><a href="payroll.php">Payroll</a></li>
        <li><a href="salary.php">Salary</a></li>
        <li><a href="hourly.php">Hourly</a></li>
        <li><a href="deductions.php">Deductions</a></li>
        <li><a href="bonus.php">Bonus</a></li>
        </ul>        
        </li>---> 
        <li>
        <a href="#" data-toggle="collapse" data-target="#leave">Leave</a>
        <ul id="leave" class="collapse">
        <li><a href="holidays.php">Holiday</a></li>
        <li><a href="sickday.php">Sickday</a></li>
        </ul>
        </li>        
        <li>
        <a href="#" data-toggle="collapse" data-target="#department">Department</a>
        
        <ul id="department" class="collapse">
        <li><a href="department.php">Department</a></li>
        <li><a href="project.php">Project</a></li>
        <li><a href="deptevents.php">Department Events</a></li>
        </ul>
        </li>        
        
        <li><a href="#" data-toggle="collapse" data-target="#attendance">Attendance</a>
        <ul id="attendance" class="collapse">
        <li><a href="time.php">Time Sheet</a></li>
        </ul>
        </li>
        <li><a href="#" data-toggle="collapse" data-target="#page"><i class="fa fa-clock-o pull-right"></i>page</a>
            <ul id="page" class="collapse">
                <li><a href="page_cat.php">Page Category</a></li>
                <li><a href="page.php">Pages</a></li>
            </ul>
        </li>       
        <li><a href="policy.php">Policy</a></li>
        <li><a href="about.php">About</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
