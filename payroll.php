<?php
include 'inc/connect.php';
include 'inc/template.php';
?>

<style>


    .formcontainer{
        max-width: 780px;                
        margin: 10px;

    }

    .reali{
        max-width: 170px;
        float: left;
        margin: 10px;
    }

    .formdropdown{
        max-width:550px;
        margin: 10px;
    }



    .table1{background-color: #E4E6E8}
    /*       table tr span{ color: red; font-weight: bold;}*/
    small{color:red;}
    .buttonr{
        width: 5px;
        height: 10px;
        border-radius: 50%;
        background-color: #aaaaaa
    }
</style>

<!-- -------------container-------------->     
<div id="page-wrapper" style="padding:20px;">
    <div class="container-fluid">
        <h1>Payroll</h1>
        <hr/>    
        <!--===========  form=========-->
        <div class="row">



            <button type="button" class="btn btn-sm btn-success glyphicon glyphicon-plus" data-toggle="modal" data-target="#add_user"></button>
            <button class="btn btn-sm btn-primary glyphicon glyphicon-time" type="button" onclick="$('#target').toggle(1000);"></button>
            <a href="payroll_range.php" class="btn btn-sm btn-default glyphicon glyphicon-random"></a>
            <a href="payrollview.php" class="btn btn-sm btn-default glyphicon glyphicon-print"></a>

            <!--=========================search=======================-->
            <!--            <form class="pull-right" action="payrollsearch.php" method="post" style="max-width: 200px;">
            
            
                            <div class="input-group"> 
                                <input type="text" name="add" id="distict" class="form-control" placeholder="enter your key"/>
                                <div class="input-group-btn">
                                    <button type="submit" name="save" class="btn btn-primary glyphicon glyphicon-search"></button>
                                </div>
            
                            </div>
                        </form>-->
            <!--====================================search===============-->


            <div class="modal fade" id="add_user" role="dialog">

                <div class="modal-dialog modal-md">

                    <div class="modal-content">

                        <div class="modal-header text-success text-center" style="background-color:#01421A; color:white"><h2>Payroll</h2></div>
                        <div class="modal-body">
                            <form id="save" action="" method="post"> 
                                <div class="formcontainer">
                                    <div class="row">
                                        <div class="formdropdown">
                                            <label>Department Name</label>

                                            <select name="cetagotry" class="form-control department">
                                                <option value="">Please Select any department</option>
                                                <?php
                                                $sqle = "select * from department order by deptname";
                                                $querye = mysqli_query($connnect, $sqle);
                                                while ($row = mysqli_fetch_array($querye)) {
                                                    ?>
                                                    <option value="<?php echo $row['deptid'] ?>"><?php echo $row['deptname'] ?></option>
                                                <?php } ?>
                                            </select>

                                        </div>




                                        <div class="formdropdown">
                                            <label>Employee Name</label>

                                            <select name="emid" id="emid_name" onchange="names('emid_name')" onkeyup="names('emid_name')" class="form-control emid_name">
                                                <option value="">Please select any employee</option>
                                                <option value=""></option>
                                            </select>
                                            <span id="emid_name_error" style="color:red;"></span>

                                        </div>
                                        
                                        
                                     
                                    </div>

                                    <!--=============================prayroll_data_select page work start from here========================================-->
                                    <div class="deducid row">
                                        <div class="reali">
                                            <label><b>Hours worked</b></label>
                                            <input type="text" class="form-control" name="hoursworked" value="<?php echo $row['hoursworked'] ?>"/>
                                        </div>

                                        <div class="reali">
                                            <label><b>Hourly rate</b></label>
                                            <input type="text" class="form-control" name="hourlyrate" value="<?php echo $row['hourlyrate'] ?>"/>
                                        </div>

                                        <div class="reali">
                                            <label><b>Total Deduction</b></label>
                                            <input type="text" class="form-control" name="deductions" value="<?php echo $row['amount'] ?>"/>
                                        </div>
                                    </div>
                                    <!--=============================prayroll_data_select page work start from here========================================-->

                                </div> 

                        </div>
                        <div class="modal-footer" style="background-color:#01421A; color:white">
                            <button type="submit" class="btn btn-sm btn-success" name="save" value="Submit">Submit</button>
                            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!--======================form end=============-->
    <!--===========================date search box=====================-->
    <div class="row">


        <div class="container" id="target" style=" padding-left: 200px;display: none;margin-bottom: 50px">

            <form id="showhide" action="payroll_datebetween.php" method="post">

                <div style="max-width:200px; margin-left: 20px;  float: left">
                    <label>Start date</label>
                    <input type="text" class="form-control datepicker" id="post_at" name="startdate" placeholder="enter your startdate" />

                </div>

                <div style="max-width:200px; margin-left: 20px; float: left">
                    <label>End date</label>
                    <input type="text" class="form-control datepicker" id="post_at_to_date" name="enddate" placeholder="enter your enddate">
                </div>

                <div style="max-width:200px; margin-top: 24px; margin-left: 20px; float: left">
                    <button type="submit" name="search" class="btn btn-primary glyphicon glyphicon-search"></button>
                </div>
            </form>

        </div>



    </div>

    <!--===========================date search box=====================-->


    <!--==============table==============-->
    <div class="row" style="padding:15px;">
        <center>

            <!--this heading_of_table class in the sb-admin.css file=======-->
            <table class="table table-bordered text-center example" id="data_table_id">
                <thead>
                    <tr class="heading_of_table">
                        <th class="text-center">SL</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Hours worked</th>
                        <th class="text-center">Net Pay</th>
                        <?php
                        $access_user = $_SESSION['access_l'];
                        if ($access_user == 1) {
                            ?>                  

                            <th class="text-center">Delete</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">View</th>
                        <?php } elseif ($access_user == 2) { ?>

                            <th class="text-center">View</th>
                        <?php } ?>                

                    </tr>
                </thead>
                <tbody>
                    <?php
                    include 'inc/connect.php';
                    $psql = "SELECT
                                `employee`.`firstname`
                                , `employee`.`lastname`
                                , `payroll`.`payrollid`
                                , `payroll`.`empid`
                                , `payroll`.`date`
                                , `payroll`.`startday`
                                , `payroll`.`endday`
                                , `payroll`.`hoursworked`
                                , `payroll`.`grosspay`
                                , `payroll`.`deductions`
                                , `payroll`.`netpay`
                            FROM
                                `employee`.`employee`
                                INNER JOIN `employee`.`payroll` 
                                    ON (`employee`.`empid` = `payroll`.`empid`)";
                    //$psql="select * from payroll";
                    $pquery = mysqli_query($connnect, $psql);
                    $i = 1;
                    while ($data = mysqli_fetch_array($pquery)) {
                        ?>    



                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data['firstname'] ?> <?php echo $data['lastname'] ?></td>
                            <td><?php echo $data['date']; ?></td>
                            <td><?php echo $data['hoursworked']; ?></td>
                            <td><?php echo $data['netpay']; ?></td>

                            <?php
                            $access_user = $_SESSION['access_l'];
                            if ($access_user == 1) {
                                ?>   

                                <td><button data-target="#delete_nc" data-toggle="modal" class="btn btn-sm btn-danger glyphicon glyphicon-trash" id="<?php echo $data['payrollid'] ?>"></button></td>
                                <td><button data-target="#update_nc" data-toggle="modal" class="btn btn-sm btn-info glyphicon glyphicon-edit" id="<?php echo $data['payrollid'] ?>"></button></td>
                                <td><a href="payroll_emp_view.php?payrollid=<?php echo $data['payrollid']; ?>" class="btn btn-sm btn-success glyphicon glyphicon-eject"></a></td>
                            <?php } elseif ($access_user == 2) { ?>
                                <td><a href="payroll_emp_view.php?payrollid=<?php echo $data['payrollid']; ?>" class="btn btn-sm btn-success glyphicon glyphicon-eject"></a></td>

                            <?php } ?> 
                        </tr>

                    <?php } ?> 
                </tbody>
            </table>
        </center>
    </div>

    <!--==============table end==============-->




    <!-- -------------container-------------->    

</div>
<!-- -------------page wrapper-------------->  



<!--===========================delete modal================-->
<div id="delete_nc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 style="color:red;">Warning</h3>
            </div>
            <div class="modal-body">
                <input type="hidden" id="pre_id" value=""/>
                <p>Are you Sure to Want to Delete your Data?</p>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-danger" id="delete">Yes</button>
                <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>


</div>  


<!--===========================delete modal================-->


<!--================================update=================-->



<div id="update_nc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <strong><h3>Update your Payroll</h3></strong>
            </div>
            <div class="modal-body">
                <div id="up"></div>

            </div>
            <div class="modal-footer">
<!--                    <td><input class="btn btn-primary" type="submit" name="save" value="Submit"/></td>
                <td><button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button></td>-->
            </div>
        </div>

    </div>


</div>


<!--================================update=================-->






<script>
    $(document).ready(function () {
        var base_url = "http://localhost/employee/";


        $('#save').submit(function (e) {
            e.preventDefault();

            var emid = $.trim($('#emid_name').val());


            if (emid === '') {
                if (emid === '') {
                    $('#emid_name_error').html('Please Enter Employee Name');
                    $('#emid_name').css("border-color", "red");
                }

            } else {
                $.ajax({
                    url: base_url + 'payrollinsert.php',
                    method: 'post',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function () {
                        window.location.assign('payroll.php');
                    }
                });
            }
        });
    });



    function names(id) {
        var val = $.trim($('#' + id).val());
        if (val === '') {
            $('#' + id + '_error').html('');
            $('#' + id).css('border-color', 'red');
        } else {
            $('#' + id + '_error').html('');
            $('#' + id).css('border-color', 'green');
        }
    }

    //===============Empid and dep id change============


    $(".department").change(function ()
    {
        var id = $(this).val();
        var dataString = 'depID=' + id;

        $.ajax
                ({
                    type: "POST",
                    url: "time_dept_ID.php",
                    data: dataString,
                    cache: false,
                    success: function (html)
                    {
                        $(".emid_name").html(html);
                    }
                });
    });


    $(".emid_name").change(function () {
        var id = $(this).val();
        //var dataString = 'p_id='+ id;


        $.ajax
                ({
                    type: "POST",
                    url: "payroll_data_select.php",
                    data: {empid: id},
                    //cache: false,
                    success: function (data)
                    {
                        $(".deducid").html(data);
                    }
                });
    });





//   ======================delete====================
    $('.example').delegate('button', 'click', function (event) {
        var id = event.target.id;
        $('#pre_id').val(id);

    });
    $('#delete').click(function () {
        var base_url = "http://localhost/employee/";
        var id = $('#pre_id').val();
        $.ajax({
            url: base_url + 'payroll_delete.php',
            method: 'post',
            data: {id: id},
            success: function () {
                window.location.assign('payroll.php');

            }
        });

    });
//   ======================delete====================

//===========================update======================


    $('.example').delegate('button', 'click', function (event) {
        var base_url = "http://localhost/employee/";
        var id = event.target.id;
        $('#pre_id').val(id);
        $.ajax({
            url: base_url + 'payroll_update_form.php',
            method: 'post',
            data: {id: id},
            success: function (data) {
                $("#up").html(data);
                //alert(data);

            }
        });

    });

    $("#update_c").submit(function (event) {
        event.preventDefault();
        var base_url = "http://localhost/employee/";
        $.ajax({
            url: base_url + 'payrollupdate.php',
            method: 'post',
            data: new FormData(this),
            processData: false,
            processType: false,
            success: function () {
                //  window.location.assign('index.php');


            }

        });

    });





//==========================update============


</script>

<?php include 'inc/footer.php';?>