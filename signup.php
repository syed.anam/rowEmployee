<?php include 'inc/connect.php';?>

<?php
if (isset($_POST['save'])) {
    include 'inc/img_upload.php';
    $depid = $_POST['depid'];
    $jobid = $_POST['jodid'];
    $typeid = $_POST['typeid'];
    $catid = $_POST['catid'];
    $laname = $_POST['lastname'];
    $fname = $_POST['firstname'];
    $minute = $_POST['minute'];
    $ssn = $_POST['ssn'];
    $dob = $_POST['dob'];
    $gender = $_POST['gender'];
    $race = $_POST['race'];
    $marid = $_POST['marital'];
    $maddress = $_POST['maddress'];
    $paddress = $_POST['paddress'];
    $city = $_POST['city'];
    $state = $_POST['division'];
    $zipcode = $_POST['zipcode'];
    $country = $_POST['country'];
    $email = $_POST['email'];
    $webpage = $_POST['webpage'];
    $homephon = $_POST['homepage'];
    $cellphon = $_POST['cellphone'];
    $offphon = $_POST['officephone'];
    $reghour = $_POST['reghour'];
    $login = $_POST['login'];
    $pass = $_POST['pass'];
    $admin = $_POST['admin'];
    $sadmin = $_POST['sadmin'];
    $numlogin = $_POST['numlogin'];
    $llogindate = $_POST['llogin'];
    $loginip = $_POST['loginip'];
    $datesignup = $_POST['dsignup'];
    $ipsignup = $_POST['ipsignup'];
    $dateupdate = $_POST['dupdate'];
    $ipupdate = $_POST['iupdated'];
    $lproject = $_POST['lproject'];
    $active = $_POST['active'];
    $image = $path;
//    echo 
    $sqlem = "INSERT INTO employee (deptid,jobid,typeid,catid,firstname,lastname,minit,"
            . "ssn,dob,gender,race,marital,address1,address2,city,state,zipcode,"
            . "country,email,webpage,homephone,officephone,cellphone,regularhours,"
            . "login,emp_password,admin,superadmin,numlogins,lastlogindate,loginip,"
            . "datesingup,ipsingup,dateupdated,ipupdated,lastproject,active,"
            . "picture)values('$depid','$jobid','$typeid','$catid','$fname','$laname',"
            . "'$minute','$ssn','$dob','$gender','$race','$marid','$maddress','$paddress',"
            . "'$city','$state','$zipcode','$country','$email','$webpage','$homephon',"
            . "'$offphon','$cellphon','$reghour','$login','$pass','$admin','$sadmin',"
            . "'$numlogin','$llogindate','$loginip','$datesignup','$ipsignup',"
            . "'$dateupdate','$ipupdate','$lproject','$active','$image')";
//    exit();
    $query = mysqli_query($connnect,$sqlem);
    if ($query) {
//        header("location:index.php");
        echo '<meta http-equiv="refresh" content="0; url=index.php">';
    } else {
        echo "Your data is not valid";
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Employee management</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.min.js"></script>
        <script src="js/validationFile.js"></script>
        <script type="text/javascript" language="javascript" >

            $(function () {
                $(".d").datepicker();
            });

        </script>
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><b>Employee management</b></a>
                </div>
                <!-- Top Menu Items -->



                <!-- Sidebar Menu Items -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">     

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
        
        
  <!-- -------------container-------------->  

<div id="page-wrapper" style="padding:25px 25px;">
<div class="container-fluid">
    <h1>Singup</h1>
    <hr/>


<!--==================registration form==============-->
<div class="row" style="margin-top:30px;">

<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#add_user">Sign Up</button>

<div class="modal fade" id="add_user" role="dialog">

<div class="modal-dialog modal-lg ">

<div class="modal-content">

<div class="modal-header text-success text-center" style="background-color:#01421A; color:white"><h2>Employee registraion Form</h2></div>
        <div class="modal-body">
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="table table-responsive">
                        <table class="table table-hover table1">
                            <tr>
                                <th>Depertment Name</th>
                                <td><input type="text" name="depid" class="form-control" maxlength="20"/><span>*</span></td>
                                <th>Job Name</th>
                                <td><input type="text" name="jodid" class="form-control" maxlength="20"/><span>*</span></td>
                            </tr>    
                            <tr>
                                <th>Type Id</th>
                                <td><input type="text" name="typeid" class="form-control" maxlength="20"/><span>*</span></td>
                                <th>Category Id</th>
                                <td><input type="text" name="catid" class="form-control" maxlength="20"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Last Name</th>
                                <td><input type="text" name="lastname" class="form-control" maxlength="20"/><span>*</span></td>
                                <th>First Name</th>
                                <td><input type="text" name="firstname" class="form-control" maxlength="20"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Minute</th>
                                <td><input type="text" name="minute" class="form-control" maxlength="50"/><span>*</span></td>
                                <th>SSN</th>
                                <td><input type="text" name="ssn" class="form-control" maxlength="50"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Date of Birth</th>
                                <td><input type="text" name="dob" class="d form-control"/><span>*</span></td>
                                <th>Gender</th>
                                <td>
                                    <select class="form-control" name="gender">
                                        <option value="">[Please select]</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Intersex">Intersex</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Marital status</th>
                                <td>
                                    <select class="form-control" name="marital">
                                        <option value="">[Please select]</option>
                                        <option value="Single">Single</option>
                                        <option value="Maried">Maried</option>
                                    </select>
                                </td>
                                <th>Race</th>
                                <td><input type="text" name="race" class="form-control"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Mailing Address</th>
                                <td><textarea rows="2" cols="2" name="maddress" class="form-control" maxlength="150"></textarea><span>*</span></td>
                                <th>Permanent Address</th>
                                <td><textarea rows="2" cols="2" name="paddress" class="form-control" maxlength="150"></textarea><span>*</span></td>    
                            </tr>
                            <tr>

                            </tr>
                            <tr>
                                <th>City</th>
                                <td><input type="text" name="city" class="form-control"/><span>*</span>
                                <th>Division</th>
                                <td>
                                    <select class="form-control" name="division">
                                        <option value="">[Please select]</option>
                                        <option value="Dhaka">Dhaka</option>
                                        <option value="Rajshahi">Rajshahi</option>
                                        <option value="Sylhet">Sylhet</option>
                                        <option value="Barishal">Barishal</option>
                                        <option value="Rangpur">Rangpur</option>
                                        <option value="Khulna">Khulna</option>
                                        <option value="Chittagong">Chittagong</option>
                                        <option value="Moymonshing">Moymonshing</option>
                                    </select>
                                </td>    
                                </td>
                            </tr>
                            <tr>
                                <th>Zipcode</th>
                                <td><input type="text" name="zipcode" class="form-control" maxlength="50"/></td>
                                <th>Country</th>
                                <td>
                                    <select class="form-control" name="country">
                                        <option value="">[Please select]</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="India">India</option>
                                        <option value="Pakistan">Pakistan</option>
                                        <option value="Japan">Japan</option>
                                        <option value="China">China</option>
                                        <option value="U.S.A">U.S.A</option>
                                        <option value="U.K">U.K</option>
                                        <option value="Canada">Canada</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td><input type="text" name="email" class="form-control"/><span>*</span></td>
                                <th>Webpage</th>
                                <td><input type="text" name="webpage" class="form-control"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Home Phone</th>
                                <td><input type="text" name="homepage" class="form-control"/><span>*</span></td>
                                <th>Cell Phone</th>
                                <td><input type="text" name="cellphone" class="form-control"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Office Phone</th>
                                <td><input type="text" name="officephone" class="form-control"/><span>*</span></td>
                                <th>Regular Hour</th>
                                <td><input type="text" name="reghour" class="form-control"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Login</th>
                                <td><input type="text" name="login" class="form-control"/><span>*</span></td>
                                <th>Password</th>
                                <td><input type="password" name="pass" class="form-control"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Admin</th>
                                <td><input type="text" name="admin" class="form-control"/><span>*</span></td>
                                <th>Super admin</th>
                                <td><input type="text" name="sadmin" class="form-control"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>NumLogin</th>
                                <td><input type="text" name="numlogin" class="form-control"/><span>*</span></td>
                                <th>Last Login Date</th>
                                <td><input type="text" name="llogin" class="d form-control"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Login Ip</th>
                                <td><input type="text" name="loginip" class="form-control"/><span>*</span></td>
                                <th>Date Sign up</th>
                                <td><input type="text" name="dsignup" class="d form-control"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Ip sign up</th>
                                <td><input type="text" name="ipsignup" class="form-control"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Date Updated</th>
                                <td><input type="text" name="dupdate" class="d form-control"/><span>*</span></td>
                                <th>Ip Updated</th>
                                <td><input type="text" name="iupdated" class="form-control"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Last Project</th>
                                <td><input type="text" name="lproject" class="form-control"/><span>*</span></td>
                                <th>Active</th>
                                <td><input type="text" name="active" class="form-control"/><span>*</span></td>
                            </tr>
                            <tr>
                                <th>Image upload</th>
                                <td><input type="file" name="imgFile" class="form-control" /><span>*</span></td>
                            </tr>
                            <span style="color:red; font-weight: bold">*</span><small >Require must be fill</small>
                        </table> 
                        </div>
                </div>
                <div class="modal-footer" style="background-color:#01421A;">
                    <input type="submit" class="btn btn-success" name="save" value="Yes">
                    <input type="button" class="btn btn-danger" data-dismiss="modal" value="No">
                </div>
                </form>
       </div>
  </div>
   </div>
    </div>
    <!--==============modal end==============-->

<!--==============registration form end==============-->

<!--==============table==============-->
</div>
<!--==============table end==============-->
        </div>
<!-- -------------container-------------->  
    </div>