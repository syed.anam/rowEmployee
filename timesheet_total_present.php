<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Employee management | time sheet attendance</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery-ui.js"></script>

    </head>


    <body>
        <div id="page-wrapper" style="padding:25px 25px;">
            <div class="row" style="padding:25px 25px;">
                <a href="time.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>            
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>
            <form action="timesheet_total_present.php" method="post" class="pull-right" style="max-width: 500px;">

                <table>
                    <td><input type="text" class="form-control datepicker" name="startdate" placeholder="enter your start date"></td>
                    <td><input type="text" class="form-control datepicker"  name="endndate" placeholder="enter your endn date"></td>
                    <td><button type="submit" name="search" class="btn btn-primary glyphicon glyphicon-search"></button></td>
                </table>
            </form>
            </div>


                <!--===========  table=========-->
            <div class="container-fluid parea">


                <!--===========  table=========-->
                <center>
                    <img src="icon/logoprint.png" class="img-responsive" style="max-width: 400px; max-height: 200px"/>



                    <table class="table table-hover text-center table-responsive excelTable" border="1">
                        <tr>
                            <td colspan="8"><center><h3>Total attendance Detail</h3></center></td>
                        </tr>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Present</th>
                                    <th class="text-center">Absent</th>
                                    <th class="text-center">Total</th>

                                </tr>
                                <?php
                                include 'inc/connect.php';
                                global $post_1, $post_2, $pquery;
                                if (isset($_POST["search"])) {
                                    $s1 = $_POST['startdate'];
                                    $s2 = $_POST['endndate'];


                                    $ssql = "SELECT
                                            `employee`.`firstname`
                                            , `employee`.`lastname`
                                            , `timesheet`.`timeid`
                                            , `timesheet`.`empid`
                                            , `timesheet`.`checked`
                                            ,SUM(IF(`timesheet`.`checked` = 'P', 1,0)) AS `Present`
                                            ,SUM(IF(`timesheet`.`checked` = 'A', 1,0)) AS `Absent`
                                            ,COUNT(`timesheet`.`checked`) AS `total`
                                            , `timesheet`.`t_date`
                                        FROM
                                            `employee`.`employee`
                                            INNER JOIN `employee`.`timesheet` 
                                                ON (`employee`.`empid` = `timesheet`.`empid`) WHERE `timesheet`.`t_date` BETWEEN '$s1' AND '$s2'
                                                GROUP BY `timesheet`.`empid`
                                                ORDER BY Present DESC";

                                                                                $pquery = mysqli_query($connnect, $ssql);
                                    $i = 1;
                                    while ($data = mysqli_fetch_array($pquery)) {
                                        ?>    



                                        <tr>
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo $data['firstname']; ?> <?php echo $data['lastname']; ?></td>
                                            <td><?php echo $data['t_date']; ?></td>
                                            <td><?php echo $data['Present']; ?></td>
                                            <td><?php echo $data['Absent']; ?></td>
                                            <td><?php echo $data['total']; ?></td>

                                        </tr>

                                    <?php }
                                }
                                ?>        
                            </table>
                            </center>
                        </div>

                        <!--==============table end==============-->

                        </div>
        
                <script type="text/javascript" language="javascript" >

            $(function () {
                $(".datepicker").datepicker();
            });

        </script>

                        <!-- -------------container-------------->
                        </body>
                        </html>
