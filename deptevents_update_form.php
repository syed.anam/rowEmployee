<?php
include 'inc/connect.php';
$u_eventid = $_POST['id'];
//$sql3="Select * from timesheet where timeid='$u_timeid'";
$sql3 = "SELECT
                    `department`.`deptname`
                    , `deptevents`.`eventid`
                    , `deptevents`.`deptid`
                    , `deptevents`.`eventdate`
                    , `deptevents`.`eventime`
                    , `deptevents`.`evenbody`
                    , `deptevents`.`postedby`
                    , `deptevents`.`dateposted`
                    , `deptevents`.`expirydate`
                    , `deptevents`.`active`
                FROM
                    `employee`.`department`
                    INNER JOIN `employee`.`deptevents` 
                        ON (`department`.`deptid` = `deptevents`.`deptid`)where `deptevents`.`eventid`='$u_eventid'";

$query5 = mysqli_query($connnect, $sql3);

while ($data = mysqli_fetch_array($query5)) {
    ?>


    <form  action="depteventsupdate.php" method="post">
        <table class="table table-hover table1">
            <input type="hidden" name="pre_id" value="<?php echo $data[1] ?>"  id="pre_id"/>                    

            <tr>
                <td>
                    <label><b>Department Name</b></label>
                    <input type="text" class="form-control" value="<?php echo $data['deptname'] ?>">
                    <span style="color:red;"></span>
                </td>
            </tr>

            <tr>
                <td>
                    <label><b>Event Date</b></label>
                    <input type="text" class="form-control" name="ueventdate"  value="<?php echo $data['eventdate'] ?>">
                    <span style="color:red;"></span>
                </td>
            </tr>            


            <tr>
                <td>
                    <label><b>Event Time</b></label>
                    <input type="text" class="form-control" name="ueventime"  value="<?php echo $data['eventime'] ?>">
                    <span style="color:red;"></span>
                </td>
            </tr>                     

            <tr>
                <td>
                    <label><b>Event Body</b></label>
                    <textarea cols="20" rows="5" class="form-control" name="uevenbody"><?php echo $data['evenbody'] ?></textarea>  
                    <span style="color:red;"></span>
                </td>
            </tr>            


            <tr>
                <td>
                    <label><b>Posted By</b></label>
                    <input type="text" class="form-control" name="upostedby"  value="<?php echo $data['postedby'] ?>">
                    <span style="color:red;"></span>
                </td>
            </tr>

            <tr>
                <td>
                    <label><b>Expiry Date</b></label>
                    <input type="text" class="form-control" name="uexpirydate"  value="<?php echo $data['expirydate'] ?>">
                    <span style="color:red;"></span>
                </td>
            </tr>

        </table> 
        <button type="button" class="pull-right btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
        <input class="pull-right btn btn-sm btn-primary" id="update_c" type="submit" name="save" value="Submit" style=""/>
    </form>
<?php } ?>

