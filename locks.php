<?php
include 'inc/template.php';
include 'inc/connect.php';
?>

<?php
if (isset($_POST['save'])) {
    $empname = $_POST['emid'];
    $lockdate = $_POST['pdate'];
    $reasonlock = $_POST['rlock'];
    $active = $_POST['act'];
    $datainsert = "insert into locks(empid, datelock, reasonlock, active)values('$empname','$lockdate','$reasonlock','$active')";
    $query = mysqli_query($connnect, $datainsert);
    if ($query) {
        echo 'your data is successfuly inserted';
    } else {
        echo 'your data is not insert';
    }
}
?>


<!-- -------------container-------------->     
<div id="page-wrapper">
    <div class="container-fluid">
        <h1>locks</h1>
        <hr/>    

        <!--------------------start form--------------------->
        <div class="row" style="padding: 20px">

            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#add_user">Add</button>
            <a href="#" onclick="window.print()" class="btn btn-info glyphicon glyphicon-print"></a>
            <!--=========================search=======================-->
            <!--<form action="lock_search.php" method="post" class="pull-right" style="max-width: 200px;">
                
                 
                            <div class="input-group"> 
                                <input type="text" name="add" class="form-control" placeholder="enter your key"/>
                                <div class="input-group-btn">
                                    <button type="submit" name="save" class="btn btn-primary glyphicon glyphicon-search"></button>
                                </div>
                
                            </div>
                        </form>-->
            <!--====================================search===============-->
            <div class="modal fade" id="add_user" role="dialog">

                <div class="modal-dialog modal-lg">

                    <div class="modal-content">

                        <div class="modal-header text-success text-center" style="background-color:#01421A; color:white"><h2>Locks</h2></div>
                        <div class="modal-body">

                            <form action="" method="post">
                                <table class="table table-hover table1">     
                                    <tr>
                                        <th>Employee Name</th>
                                        <td><select name="emid" class="form-control" >
                                                <option value="">select any</option>

                                                <?php
                                                $sqlll = "select * from employee";
                                                $queryyyy = mysqli_query($connnect, $sqlll);
                                                while ($sdata = mysqli_fetch_array($queryyyy)) {
                                                    ?>   

                                                    <option value="<?php echo $sdata['empid'] ?>"><?php echo $sdata['login']; ?></option>

                                                <?php } ?>   
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Lock Date</th>      
                                    <script type="text/javascript" language="javascript" >
                                        $(function () {
                                            $("#datepicker").datepicker();
                                        });
                                    </script>
                                    <td><input type="text" id="datepicker" name="pdate" class="form-control"/><span>*</span></td>
                                    </tr>
                                    <tr>
                                        <th>Reason lock</th>
                                        <td><input type="text" name="rlock" class="form-control" maxlength="10"/><span>*</span></td>
                                    </tr>
                                    <tr>
                                        <th>Active</th>
                                        <td><input type="text" name="act" class="form-control" maxlength="50"/></td>     
                                    </tr>        
                                    <span style="color:red; font-weight: bold">*</span><small >Require must be fill</small>
                                </table>     
                        </div>
                        <div class="modal-footer" style="background-color:#01421A;">
                            <input type="submit" class="btn btn-warning" name="save" value="Submit">
                            <input type="button" class="btn btn-danger" data-dismiss="modal" value="Cancel">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!------------------------End form------------------------>

    <!-----------------------------start table---------------->
    <div class="row" style="padding: 20px">
        <center>
            <!--this heading_of_table class in the sb-admin.css file=======-->
            <table class="table table-bordered text-center example" id="data_table_id">
                <thead>
                    <tr class="heading_of_table">
                        <th class="text-center">SL</th>
                        <th class="text-center">Employee Name</th>
                        <th class="text-center">Lock Date</th>
                        <th class="text-center">Reason Lock</th>
                        <th class="text-center">Active</th>
                        <?php
                        $access_user = $_SESSION['access_l'];
                        if ($access_user == 1) {
                            ?>    
                            <th class="text-center">Delete</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">View</th>
                        <?php } elseif ($access_user == 2) { ?>
                            <th class="text-center">View</th>                
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
//            $lockid=$_GET['lockid'];
                    $lockdet = "select * from locks";
                    $query = mysqli_query($connnect, $lockdet);
                    $i = 1;
                    while ($data = mysqli_fetch_array($query)) {
                        ?>
                        <tr>
                            <td><?php echo $i++ ?></td> 
                            <td><?php echo $data['empid'] ?></td>
                            <td><?php echo $data['datelock'] ?></td>
                            <td><?php echo $data['reasonlock'] ?></td>
                            <td><?php echo $data['active'] ?></td>
                            <?php
                            $access_user = $_SESSION['access_l'];
                            if ($access_user == 1) {
                                ?>
                                <td><a href="lock_delete.php?lockid=<?php echo $data['lockid'] ?>" class="btn btn-danger glyphicon glyphicon-trash"></a></td>
                                <td><a href="lock_update.php?lockid=<?php echo $data['lockid'] ?>" class="btn btn-warning glyphicon glyphicon-edit"></a></td>
                                <td><a href="lock_view.php?lockid=<?php echo $data['lockid'] ?>" class="btn btn-success glyphicon glyphicon-eject"></a></td>
                            <?php } elseif ($access_user == 2) { ?>
                                <td><a href="lock_view.php?lockid=<?php echo $data['lockid'] ?>" class="btn btn-success glyphicon glyphicon-eject"></a></td>
                                <?php } ?>

                        </tr>
                    <?php } ?>

                </tbody>
            </table>
        </center>
    </div>
    <!----------------------------End table------------------->

    <!-- -------------container-------------->    

</div>
<!-- -------------page wrapper-------------->  




<?php include 'inc/footer.php'; ?>