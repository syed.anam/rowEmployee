<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Employee management | Department</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>

    </head>


    <body>
        <!-- -------------container--------------> 

        <div class="container" style="padding: 50px">
            <a href="department.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>            
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
<!--            <a href="#" class="btn btn-sm btn-default"><img src="icon/pdf.png" width="17px" height="17px"/></a>-->
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>


            <div class="row parea" style="padding: 30px 20px 0px 20px">


                <!--===========  form=========-->
                <center>
                    <img src="icon/logoprint.png" style="max-width: 300px; max-height: 150px"/>



                    <table class="table table-bordered table-responsive excelTable">
                        <tr>
                            <td colspan="2"><center><h3>Department Information</h3></center></td>
                        </tr>
                        <?php
                        $deptid = $_GET['deptid'];
                        $departmentview = "SELECT * FROM department WHERE deptid='$deptid'";

                        $query = mysqli_query($connnect, $departmentview);
                        while ($data = mysqli_fetch_array($query)) {
                            ?>
                            <tr>
                                <th>Department Name </th>
                                <td><?php echo $data['deptname']; ?></td>
                            </tr>
                            <tr>
                                <th>Location</th>
                                <td><?php echo $data['location']; ?></td>
                            </tr>
                            <tr>
                                <th>Department Description</th>
                                <td><?php echo $data['deptdesc']; ?></td>
                            </tr>
                            <tr>
                                <th>Employee Work Description</th>
                                <td><?php echo $data['mandaworkdesc']; ?></td>
                            </tr>
                            <tr>
                                <th>Massage</th>
                                <td><?php echo $data['messaging']; ?></td>
                            </tr>



                        <?php } ?> 
                    </table>



                </center>

            </div>
        </div>
        <!-- -------------container-------------->
    </body>
</html>
