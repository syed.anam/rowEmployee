<?php
include 'inc/connect.php';
include 'inc/template.php';
?>

<style>
    .table1{background-color: #E4E6E8}
    /*       table tr span{ color: red; font-weight: bold;}*/
    small{color:red;}
    .buttonr{width: 5px; height: 10px; border-radius: 50%; background-color: #aaaaaa}
</style>

<!-- -------------container-------------->     
<div id="page-wrapper" style="padding:20px;">
    <div class="container-fluid">
        <h1>Project</h1>
        <hr/>    
        <!--===========  form=========-->
        <div class="row">



            <button type="button" class="btn btn-sm btn-success glyphicon glyphicon-plus" data-toggle="modal" data-target="#add_user"></button>
            <!--    <button class="btn btn-primary glyphicon glyphicon-time" type="button" onclick="$('#target').toggle(1000);">search</button>-->
            <a href="project_view_print.php" class="btn btn-sm btn-default glyphicon glyphicon-print"></a>

            <!--=========================search=======================-->
<!--            <form class="pull-right" action="project_search.php" method="post" style="max-width: 200px;">


                <div class="input-group"> 
                    <input type="text" name="add" id="distict" class="form-control" placeholder="enter your key"/>
                    <div class="input-group-btn">
                        <button type="submit" name="save" class="btn btn-primary glyphicon glyphicon-search"></button>
                    </div>

                </div>
            </form>-->
            <!--====================================search===============-->


            <div class="modal fade" id="add_user" role="dialog">

                <div class="modal-dialog modal-lg">

                    <div class="modal-content">

                        <div class="modal-header text-success text-center" style="background-color:#01421A; color:white"><h2>Project</h2></div>
                        <div class="modal-body">
                            <form id="save" action="" method="post">
                                <table class="table table-hover table1">                                       
                                    <tr>
                                        <th>Department Name</th>
                                        <td><select id="depid_name" name="depid" onchange="names('depid_name')" onkeyup="names('depid_name')" class="form-control">
                                                <option value="">select any</option>

                                                <?php
                                                $sq4 = "select * from department";
                                                $query2 = mysqli_query($connnect, $sq4);
                                                while ($sdata = mysqli_fetch_array($query2)) {
                                                    ?>    


                                                    <option value="<?php echo $sdata['deptid'] ?>"><?php echo $sdata['deptname']; ?></option>

                                                <?php } ?>   
                                            </select>
                                            <span id="depid_name_error" style="color:red;">*</span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Project Name</th>
                                        <td><input type="text" id="projecttitle_name" name="projecttitle" onchange="names('projecttitle_name')" onkeyup="names('projecttitle_name')" class="form-control" />
                                            <span id="projecttitle_name_error" style="color:red;">*</span>
                                        </td>
                                    </tr>


                                    <tr>
                                        <th>projectdesc</th>
                                        <td><input type="text" id="projectdesc_name" name="projectdesc" onchange="names('projectdesc_name')" onkeyup="names('projectdesc_name')" class="form-control" />
                                            <span id="projectdesc_name_error" style="color:red;">*</span>
                                        </td>
                                    </tr>


                                    <tr>
                                        <th>Hours work</th>
                                        <td><input type="text" id="hoursworked_name" name="hoursworked" onchange="names('hoursworked_name')" onkeyup="names('hoursworked_name')" class="form-control" />
                                            <span id="hoursworked_name_error" style="color:red;">*</span>
                                        </td>
                                    </tr>


                                    <tr>
                                        <th>Active</th>
                                        <td><input type="text" id="active_name" name="active" onchange="names('active_name')" onkeyup="names('active_name')" class="form-control" />
                                            <span id="active_name_error" style="color:red;">*</span>
                                        </td>
                                    </tr>

                                    <span style="color:red; font-weight: bold">*</span><small >Require must be fill</small>
                                </table>

                        </div>
                        <div class="modal-footer" style="background-color:#01421A; color:white">
                            <button type="submit" class="btn btn-success" name="save" value="Submit">Submit</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!--======================form end=============-->
    <!--===========================date search box=====================-->
    <!--      <div class="row">
              
                  
        <div class="container" id="target" style=" padding-left: 200px;display: none;margin-bottom: 50px">
            
            <form id="showhide" action="payroll_datebetween.php" method="post">
        
                <div style="max-width:200px; margin-left: 20px;  float: left">
                <label>Start date</label>
                <input type="text" class="form-control datepicker" id="post_at" name="startdate" placeholder="enter your startdate" />
                    
                </div>
    
                <div style="max-width:200px; margin-left: 20px; float: left">
                <label>End date</label>
                <input type="text" class="form-control datepicker" id="post_at_to_date" name="enddate" placeholder="enter your enddate">
                </div>
                
                <div style="max-width:200px; margin-top: 24px; margin-left: 20px; float: left">
                    <button type="submit" name="search" class="btn btn-primary glyphicon glyphicon-search"></button>
                </div>
    </form>
           
        </div>
        
            
              
          </div>-->

    <!--===========================date search box=====================-->


    <!--==============table==============-->
    <div class="row" style="padding:15px;">
        <center>

            <!--this heading_of_table class in the sb-admin.css file=======-->
            <table class="table table-bordered text-center example" id="data_table_id">
                <thead>
                    <tr class="heading_of_table">
                        <th class="text-center">SL</th>
                        <th class="text-center">Department Name</th>
                        <th class="text-center">Project title</th>
                        <th class="text-center">Project description</th>
                        <th class="text-center">Hours Worked</th>
                        <th class="text-center">Active</th>
                        <?php
                        $access_user = $_SESSION['access_l'];
                        if ($access_user == 1) {
                            ?>    
                            <th class="text-center">Delete</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">View</th>
                        <?php } elseif ($access_user == 2) { ?>
                            <th class="text-center">View</th>                
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $projectv = "SELECT
    `department`.`deptname`
    , `project`.`projectid`
    , `project`.`deptid`
    , `project`.`projecttitle`
    , `project`.`projectdesc`
    , `project`.`hoursworked`
    , `project`.`active`
FROM
    `employee`.`department`
    INNER JOIN `employee`.`project` 
        ON (`department`.`deptid` = `project`.`deptid`) ORDER BY projectid DESC";
                    $query = mysqli_query($connnect, $projectv);

                    $i = 1;

                    while ($data = mysqli_fetch_array($query)) {
                        ?>



                        <tr>
                            <td><?php echo $i++ ?></td>
                            <td><?php echo $data['deptname']; ?></td>
                            <td><?php echo $data['projecttitle']; ?></td>
                            <td><?php echo $data['projectdesc']; ?></td>
                            <td><?php echo $data['hoursworked']; ?></td>
                            <td><?php echo $data['active']; ?></td>
                            <?php
                            $access_user = $_SESSION['access_l'];
                            if ($access_user == 1) {
                                ?>  
                                <td><button data-target="#delete_nc" data-toggle="modal" class="btn btn-danger glyphicon glyphicon-trash" id="<?php echo $data['projectid'] ?>"></button></td>
                                <td><button data-target="#update_nc" data-toggle="modal" class="btn btn-info glyphicon glyphicon-edit" id="<?php echo $data['projectid'] ?>"></button></td>
                                <td><a href="project_emp_view.php?projectid=<?php echo $data['projectid']; ?>" class="btn btn-success glyphicon glyphicon-eject"></a></td>
                            <?php } elseif ($access_user == 2) { ?>
                                <td><a href="project_emp_view.php?projectid=<?php echo $data['projectid']; ?>" class="btn btn-success glyphicon glyphicon-eject"></a></td>
                            <?php } ?>
                        </tr>

                    <?php } ?> 
                </tbody>
            </table>
        </center>
    </div>

    <!--==============table end==============-->




    <!-- -------------container-------------->    

</div>
<!-- -------------page wrapper-------------->  



<!--===========================delete modal================-->
<div id="delete_nc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 style="color:red;">Warning</h3>
            </div>
            <div class="modal-body">
                <input type="hidden" id="pre_id" value=""/>
                <p>Are you Sure to Want to Delete your Data?</p>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-danger" id="delete">Yes</button>
                <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>


</div>  


<!--===========================delete modal================-->


<!--================================update=================-->



<div id="update_nc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <strong><h3>Update your Project</h3></strong>
            </div>
            <div class="modal-body">
                <div id="up"></div>

            </div>
            <div class="modal-footer">
<!--                    <td><input class="btn btn-primary" type="submit" name="save" value="Submit"/></td>
                <td><button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button></td>-->
            </div>
        </div>

    </div>


</div>


<!--================================update=================-->






<script>
    $(document).ready(function () {
        var base_url = "http://localhost/employee/";


        $('#save').submit(function (e) {
            e.preventDefault();

            var depid = $.trim($('#depid_name').val());
            //var payroll_date=$.trim($('#pdate_name').val());
            //var startday=$.trim($('#startday_name').val());
            var projecttitle = $.trim($('#projecttitle_name').val());
            var projectdesc = $.trim($('#projectdesc_name').val());
            var hoursworked = $.trim($('#hoursworked_name').val());
            var active = $.trim($('#active_name').val());
            //var messaging=$.trim($('#messaging_name').val());


            if (depid === '' || projecttitle === '' || projectdesc === '' || hoursworked === '' || active === '') {
//                if(emid==='' || deptname==='' || location==='' || deptdesc==='' || mandaworkdesc=''){
                if (depid === '') {
                    $('#depid_name_error').html('Please Enter Department Name');
                    $('#depid_name').css("border-color", "red");
                }


                if (projecttitle === '') {
                    $('#projecttitle_name_error').html('Please Enter Your Project name');
                    $('#projecttitle_name').css("border-color", "red");
                }


                if (projectdesc === '') {
                    $('#projectdesc_name_error').html('Please Enter Your Project description');
                    $('#projectdesc_name').css("border-color", "red");
                }


                if (hoursworked === '') {
                    $('#hoursworked_name_error').html('Please Enter Your Works hour');
                    $('#hoursworked_name').css("border-color", "red");
                }

                if (active === '') {
                    $('#active_name_error').html('Please Enter Your Active');
                    $('#active_name').css("border-color", "red");
                }


            } else {
                $.ajax({
                    url: base_url + 'project_insert.php',
                    method: 'post',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function () {
                        window.location.assign('project.php');
                    }
                });
            }
        });
    });




//   ======================delete====================
    $('.example').delegate('button', 'click', function (event) {
        var id = event.target.id;
        $('#pre_id').val(id);

    });
    $('#delete').click(function () {
        var base_url = "http://localhost/employee/";
        var id = $('#pre_id').val();
        $.ajax({
            url: base_url + 'projectdelete.php',
            method: 'post',
            data: {projectid: id},
            success: function () {
                window.location.assign('project.php');

            }
        })

    });
//   ======================delete====================

//===========================update======================


    $('.example').delegate('button', 'click', function (event) {
        var base_url = "http://localhost/employee/";
        var id = event.target.id;
        $('#pre_id').val(id);
        $.ajax({
            url: base_url + 'project_update_form.php',
            method: 'post',
            data: {id: id},
            success: function (data) {
                $("#up").html(data);
                //alert(data);

            }
        });

    });

    $("#update_c").submit(function (event) {
        event.preventDefault();
        var base_url = "http://localhost/employee/";
        $.ajax({
            url: base_url + 'projectupdate.php',
            method: 'post',
            data: new FormData(this),
            processData: false,
            processType: false,
            success: function () {
                //  window.location.assign('index.php');


            }

        });

    });





//==========================update============



    function names(id) {
        var val = $.trim($('#' + id).val());
        if (val === '') {
            $('#' + id + '_error').html('');
            $('#' + id).css('border-color', 'red');
        } else {
            $('#' + id + '_error').html('');
            $('#' + id).css('border-color', 'green');
        }
    }




</script>

<?php include 'inc/footer.php'; ?>