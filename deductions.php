<?php
include 'inc/connect.php';
include 'inc/template.php';
?>

<style>
    .table1{background-color: #E4E6E8}
    /*       table tr span{ color: red; font-weight: bold;}*/
    small{color:red;}
    .buttonr{width: 5; height: 10; border-radius: 50%; background-color: #aaaaaa}
</style>

<!-- -------------container-------------->     
<div id="page-wrapper" style="padding:20px;">
    <div class="container-fluid">
        <h1>Deduction</h1>
        <hr/>    
        <!--===========  form=========-->
        <div class="row">



            <button type="button" class="btn btn-sm btn-success glyphicon glyphicon-plus" data-toggle="modal" data-target="#add_user"></button>
            <a href="deductionsview.php" class="btn btn-sm btn-default glyphicon glyphicon-print"></a>

            <!--=========================search=======================-->
            <!--            <form class="pull-right" action="deductionssearch.php" method="post" style="max-width: 200px;">
            
            
                            <div class="input-group"> 
                                <input type="text" name="add" id="distict" class="form-control" placeholder="enter your key"/>
                                <div class="input-group-btn">
                                    <button type="submit" name="save" class="btn btn-primary glyphicon glyphicon-search"></button>
                                </div>
            
                            </div>
                        </form>-->
            <!--====================================search===============-->


            <div class="modal fade" id="add_user" role="dialog">

                <div class="modal-dialog modal-md">

                    <div class="modal-content">

                        <div class="modal-header text-success text-center" style="background-color:#01421A; color:white"><h2>Deduction</h2></div>
                        <div class="modal-body">
                            <form id="save" action="" method="post">
                                <table class="table table-hover table1">                                       
                                    <tr>
                                        <th>Department Name</th>
                                        <td>
                                            <select name="cetagotry" onchange="getId(this.value);" class="form-control department">
                                                <option value="">Please Select any department</option>
                                                <?php
                                                $sqle = "select * from department order by deptname";
                                                $querye = mysqli_query($connnect, $sqle);
                                                while ($row = mysqli_fetch_array($querye)) {
                                                    ?>
                                                    <option value="<?php echo $row['deptid'] ?>"><?php echo $row['deptname'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                    </tr>




                                    <tr>
                                        <th>Employee Name</th>
                                        <td>
                                            <select name="emid_name" id="emid_name" onchange="names('emid_name')" onkeyup="names('emid_name')" class="form-control emid_name">
                                                <option value="">Please select any employee</option>
                                                <option value=""></option>
                                            </select>
                                            <span id="emid_name_error" style="color:red;"></span>
                                        </td>
                                    </tr>


                                    <tr>
                                        <th>Deduction Type</th>
                                        <td>
                                            <input type="text" class="form-control" id="deduc_type" name="deductype" onchange="names('deduc_type')" onkeyup="names('deduc_type')">
                                            <span id="deduc_type_error" style="color:red;"></span>
                                        </td>

                                    </tr>


                                    <tr>
                                        <th>Amount</th>
                                        <td>
                                            <input type="text" class="form-control" id="amount_name" name="amount" onchange="names('amount_name')" onkeyup="names('amount_name')">
                                            <span id="amount_name_error" style="color:red;"></span>
                                        </td>

                                    </tr>

                                    <tr>
                                        <th>Note</th>
                                        <td>
                                            <input type="text" class="form-control" id="note_name" name="note" onchange="names('note_name')" onkeyup="names('note_name')">
                                            <span id="note_name_error" style="color:red;"></span>
                                        </td>

                                    </tr>







                                </table> 

                        </div>
                        <div class="modal-footer" style="background-color:#01421A; color:white">
                            <button type="submit" class="btn btn-sm btn-success" name="save" value="Submit">Submit</button>
                            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!--======================form end=============-->


    <!--==============table==============-->
    <div class="row" style="padding:15px;">
        <center>
            <!--this heading_of_table class in the sb-admin.css file=======-->
            <table class="table table-bordered text-center example" id="data_table_id">
                <thead>
                    <tr class="heading_of_table">
                        <th class="text-center">SL</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Deduction Type</th>
                        <th class="text-center">Amount</th>

                        <?php
                        $access_user = $_SESSION['access_l'];
                        if ($access_user == 1) {
                            ?>                  

                            <th class="text-center">Delete</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">View</th>
                        <?php } elseif ($access_user == 2) { ?>

                            <th class="text-center">View</th>
                        <?php } ?>                

                    </tr>
                </thead>
                <tbody>
                    <?php
                    include 'inc/connect.php';
                    $psql = "SELECT
                    `employee`.`firstname`
                    , `employee`.`lastname`
                    , `deductions`.`deduid`
                    , `deductions`.`empid`
                    , `deductions`.`deductype`
                    , `deductions`.`amount`
                    , `deductions`.`note`
                FROM
                    `employee`.`employee`
                    INNER JOIN `employee`.`deductions` 
                        ON (`employee`.`empid` = `deductions`.`empid`)";
                    //$psql="select * from deductions";
                    $pquery = mysqli_query($connnect, $psql);
                    $i = 1;
                    while ($data = mysqli_fetch_array($pquery)) {
                        ?>    



                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data['firstname'] ?> <?php echo $data['lastname'] ?></td>
                            <td><?php echo $data['deductype']; ?></td>
                            <td><?php echo $data['amount']; ?></td>

                            <?php
                            $access_user = $_SESSION['access_l'];
                            if ($access_user == 1) {
                                ?>   

                                <td><button data-target="#delete_nc" data-toggle="modal" class="btn btn-sm btn-danger glyphicon glyphicon-trash" id="<?php echo $data['deduid'] ?>"></button></td>
                                <td><button data-target="#update_nc" data-toggle="modal" class="btn btn-sm btn-info glyphicon glyphicon-edit" id="<?php echo $data['deduid'] ?>"></button></td>
                                <td><a href="deductions_emp_view.php?deduid=<?php echo $data['deduid']; ?>" class="btn btn-sm btn-success glyphicon glyphicon-eject"></a></td>
                            <?php } elseif ($access_user == 2) { ?>
                                <td><a href="deductions_emp_view.php?deduid=<?php echo $data['deduid']; ?>" class="btn btn-sm btn-success glyphicon glyphicon-eject"></a></td>

                            <?php } ?> 
                        </tr>

                    <?php } ?>
                </tbody>
            </table>
        </center>
    </div>

    <!--==============table end==============-->




    <!-- -------------container-------------->    

</div>
<!-- -------------page wrapper-------------->  



<!--===========================delete modal================-->
<div id="delete_nc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 style="color:red;">Warning</h3>
            </div>
            <div class="modal-body">
                <input type="hidden" id="pre_id" value=""/>
                <p>Are you Sure to Want to Delete your Data?</p>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-danger" id="delete">Yes</button>
                <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>


</div>  


<!--===========================delete modal================-->


<!--================================update=================-->



<div id="update_nc" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <strong><h3>Update your deductions</h3></strong>
            </div>
            <div class="modal-body">
                <div id="up"></div>

            </div>
            <div class="modal-footer">
<!--                    <td><input class="btn btn-primary" type="submit" name="save" value="Submit"/></td>
                <td><button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button></td>-->
            </div>
        </div>

    </div>


</div>


<!--================================update=================-->


<script>
    $(document).ready(function () {
        var base_url = "http://localhost/employee/";


        $('#save').submit(function (e) {
            e.preventDefault();

            var emid = $.trim($('#emid_name').val());
            var deductype = $.trim($('#deduc_type').val());
            var amount = $.trim($('#amount_name').val());

            if (emid === '' || deductype === '' || amount === '') {
                if (emid == '') {
                    $('#emid_name_error').html('Please Enter Employee Name');
                    $('#emid_name').css("border-color", "red");
                }


                if (deductype == '') {
                    $('#deduc_type_error').html('Please Enter Your Deduction Type');
                    $('#deduc_type').css("border-color", "red");
                }

                if (amount == '') {
                    $('#amount_name_error').html('Please Enter Payment');
                    $('#amount_name').css("border-color", "red");
                }


            } else {
                $.ajax({
                    url: base_url + 'deductions_insert.php',
                    method: 'post',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function () {
                        window.location.assign('deductions.php');
                    }
                })
            }
        });
    });


    function names(id) {
        var val = $.trim($('#' + id).val());
        if (val === '') {
            $('#' + id + '_error').html('');
            $('#' + id).css('border-color', 'red');
        } else {
            $('#' + id + '_error').html('');
            $('#' + id).css('border-color', 'green');
        }
    }

//    ==============================employee id find=======================
    $(".department").change(function ()
    {
        var id = $(this).val();
        var dataString = 'depID=' + id;

        $.ajax
                ({
                    type: "POST",
                    url: "time_dept_ID.php",
                    data: dataString,
                    cache: false,
                    success: function (html)
                    {
                        $(".emid_name").html(html);
                    }
                });
    });




//    =============================employee id find=============



//   ======================delete====================
    $('.example').delegate('button', 'click', function (event) {
        var id = event.target.id;
        $('#pre_id').val(id);

    })
    $('#delete').click(function () {
        var base_url = "http://localhost/employee/";
        var id = $('#pre_id').val();
        $.ajax({
            url: base_url + 'deductionsdelete.php',
            method: 'post',
            data: {id: id},
            success: function () {
                window.location.assign('deductions.php');

            }
        })

    });
//   ======================delete====================

//===========================update======================


    $('.example').delegate('button', 'click', function (event) {
        var base_url = "http://localhost/employee/";
        var id = event.target.id;
        $('#pre_id').val(id);
        $.ajax({
            url: base_url + 'deductions_update_form.php',
            method: 'post',
            data: {id: id},
            success: function (data) {
                $("#up").html(data);
                //alert(data);

            }
        })

    })

    $("#update_c").submit(function (event) {
        event.preventDefault();
        var base_url = "http://localhost/employee/";
        $.ajax({
            url: base_url + 'deductionsupdate.php',
            method: 'post',
            data: new FormData(this),
            processData: false,
            processType: false,
            success: function () {
                //  window.location.assign('index.php');


            }

        })

    })





//==========================update============

</script>

<?php include 'inc/footer.php';?>