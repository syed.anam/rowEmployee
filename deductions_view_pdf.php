<?php
include'inc/connect.php';
require_once 'dompdf/dompdf_config.inc.php';

$codigoHTML='

   <center>
<img src="icon/logo1.png"/>
    <h3>Deduction sheet detail</h3>

<table width="100%" border="1" style="padding:30px;">
    <tr>
        <td style="background-color:#EFEFEF">SL</th>
        <td style="background-color:#EFEFEF">Employee Name</th>
        <td style="background-color:#EFEFEF">Deduction type</th> 
        <td style="background-color:#EFEFEF">Payment</th>
        <td style="background-color:#EFEFEF">Note</th>
    </tr>';




$deductionspdf="SELECT
                `employee`.`firstname`
                    , `employee`.`lastname`
                    , `deductions`.`deduid`
                    , `deductions`.`empid`
                    , `deductions`.`deductype`
                    , `deductions`.`amount`
                    , `deductions`.`note`
                FROM
                    `employee`.`employee`
                    INNER JOIN `employee`.`deductions` 
                        ON (`employee`.`empid` = `deductions`.`empid`)";
$query=  mysqli_query($connnect,$deductionspdf);

$i=1;

while($data=mysqli_fetch_array($query)){   
 $codigoHTML.='
        <tr>
            <td>'.$i++.'</td>
            <td>'.$data['firstname'].' '.$data['lastname'].'</td>
            <td>'.$data['deductype'].'</td>
            <td>'.$data['amount'].'</td>
            <td>'.$data['note'].'</td>
        </tr>';


 }
 $codigoHTML.='
</table>
</center>';


$codigoHTML= utf8_decode($codigoHTML);
$dompdf=new DOMPDF();
$dompdf->load_html($codigoHTML);
ini_set("memory_limit", "128M");
$dompdf->render();
$dompdf->stream("deductions_pdf_view.pdf");

?>
