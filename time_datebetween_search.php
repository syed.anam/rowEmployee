<?php
include 'inc/connect.php';
include 'inc/template.php';
?>
<!----- \========================================form==================================---------->
<div id="page-wrapper" style="padding:20px;">
    <h1>Time Sheet search</h1>
    <hr/>
    <!--==========================================date search====================================-->
    <div class="row" style="padding:0px 15px 5px;">
        <a href="time.php" class="btn btn-danger glyphicon glyphicon-backward"></a>
        <button class="btn btn-success glyphicon glyphicon-time" type="button" onclick="$('#target').toggle(1000);">search</button>
        <div class="container" id="target" style=" padding-left: 200px;display: none;margin-bottom: 10px">
            <form id="showhide" action="time_datebetween_search.php" method="post">
                <div style="max-width:200px; margin-left: 20px;  float: left">
                    <label>Start date</label>
                    <input type="text" class="form-control datepicker" id="post_at" name="startdate" placeholder="enter your startdate" />
                </div>
                <div style="max-width:200px; margin-left: 20px; float: left">
                    <label>End date</label>
                    <input type="text" class="form-control datepicker" id="post_at_to_date" name="enddate" placeholder="enter your enddate">
                </div>
                <div style="max-width:200px; margin-top: 24px; margin-left: 20px; float: left">
                    <button type="submit" name="search" class="btn btn-primary glyphicon glyphicon-search"></button>
                </div>
            </form>
        </div>
    </div>  
    <!--==========================================date search====================================-->
    <!----- \========================================Table==================================---------->
    <div class="row" style="padding:15px;">
        <table class="table table-bordered text-center example">
            <tr>
                <th class="text-center">SL</th>
                <th class="text-center">Name</th>
                <th class="text-center">Date</th>
                <th class="text-center">Check in</th>
                <th class="text-center">Check out</th>
                <th class="text-center">Rounded Time</th>
            </tr>
            <?php
            include 'inc/connect.php';
            global $post_1, $post_2, $pquery;
            if (isset($_POST["search"])) {
                $s1 = $_POST['startdate'];
                $s2 = $_POST['enddate'];
                $ssql = "SELECT
    `employee`.`firstname`
    , `employee`.`lastname`
    , `project`.`projecttitle`
    , `timesheet`.`timeid`
    , `timesheet`.`empid`
    , `timesheet`.`projectid`
    , `timesheet`.`checkin`
    , `timesheet`.`checkout`
    , `timesheet`.`rawtime`
    , `timesheet`.`roundtime`
    , `timesheet`.`workdesc`
    , `timesheet`.`ipcheckin`
    , `timesheet`.`ipcheckout`
    , `timesheet`.`checked`
    , `timesheet`.`t_date`
FROM
    `employee`.`project`
    INNER JOIN `employee`.`timesheet` 
        ON (`project`.`projectid` = `timesheet`.`projectid`)
    INNER JOIN `employee`.`employee` 
        ON (`employee`.`empid` = `timesheet`.`empid`)
        WHERE `timesheet`.`t_date` BETWEEN '$s1' AND '$s2'";
                $pquery = mysqli_query($connnect, $ssql);
                $i = 1;
                while ($data = mysqli_fetch_array($pquery)) {
                    ?>                          
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['firstname'] ?> <?php echo $data['lastname'] ?></td>
                        <td><?php echo $data['t_date']; ?></td>
                        <td><?php echo $data['checkin']; ?></td>
                        <td><?php echo $data['checkout']; ?></td>
                        <td><?php echo $data['roundtime']; ?></td>                               
                    </tr>
                <?php }
            } ?>        
        </table>
    </div>
    <!----- \========================================Table==================================---------->
</div>
</body>
</html>