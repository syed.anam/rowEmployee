<?php
include 'inc/connect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Employee management | Sick Day view</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/jquery.table2excel.js" type="text/JavaScript" language="javascript"></script>
        <script src="print/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
        <script src="js/print_excel_id.js" type="text/JavaScript" language="javascript"></script>


    </head>


    <body>
        <div class="container" style="padding: 50px">
            <a href="sickday.php" class="btn btn-sm btn-danger glyphicon glyphicon-backward"></a>
            <a href="javascript:void(0);" id="print_button1" class="btn btn-sm btn-info glyphicon glyphicon-print"></a>
            <button class="btn btn-sm btn-default" id="excelTable"><img src="icon/Excel128.jpg" width="17px" height="17px"/></button>



            <div class="row parea" style="padding: 30px 20px 0px 20px">
                <center>

                    <table class="table table-bordered table-responsive excelTable">
                        <tr>
                            <td colspan="2"><center><h3>Sick Day Information</h3></center></td>
                        </tr>
                        <?php
                        $sickid = $_GET['sickid'];
                        $sickdayview = "SELECT
                                            `employee`.`firstname`
                                            , `employee`.`lastname`
                                            , `sickday`.`sickid`
                                            , `sickday`.`empid`
                                            , `sickday`.`datesick`
                                            , `sickday`.`payment`
                                        FROM
                                            `employee`.`employee`
                                            INNER JOIN `employee`.`sickday` 
                                                ON (`employee`.`empid` = `sickday`.`empid`)
                                                where `sickday`.`sickid`='$sickid'";
                        $query = mysqli_query($connnect, $sickdayview);
                        while ($data = mysqli_fetch_array($query)) {
                            ?> 
                            <tr>
                                <th>Employee Name </th>
                                <td><?php echo $data['firstname'] ?> <?php echo $data['lastname'] ?></td>
                            </tr>
                            <tr>
                                <th>Date of Sick</th>
                                <td><?php echo $data['datesick']; ?></td>
                            </tr>
                            <tr>
                                <th>Payment :</th>
                                <td><?php echo $data['payment']; ?></td>
                            </tr>


                        <?php } ?> 
                    </table>               
                </center>
            </div>
        </div>
    </body>
</html>


